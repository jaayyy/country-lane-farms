<?php

$databases = array (
  'default' => array (
    'default' => array (
      'database' => 'countr70_prod',
      'username' => 'countr70_root',
      'password' => 'vancouver1',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);
$base_url = 'http://www.countrylanefarms.com';  // NO trailing slash!
$cookie_domain = 'www.countrylanefarms.com';

//  Turn off Caching and Aggregation of CSS and JS
$conf['error_level'] = '1';
$conf['preprocess_css'] = '0';
$conf['preprocess_js'] = '0';
$conf['cache'] = '0';
$conf['page_compression'] = '1';
$conf['block_cache'] = '1';

?>