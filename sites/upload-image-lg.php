<?php
ini_set("display_errors", 1);

require(dirname(__FILE__) . "/all/libraries/simpleajaxupload/uploader.php");

// Directory where we store uploaded images
// Remember to set correct permissions or it won't work
$upload_dir = dirname(__FILE__).'/default/files/products/large';

$uploader = new FileUpload('uploadfile');

// Handle the upload
$result = $uploader->handleUpload($upload_dir);

if (!$result) {
  exit(json_encode(array('success' => false, 'msg' => $uploader->getErrorMsg())));
}

echo json_encode(array('success' => true));
