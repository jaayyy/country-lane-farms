<?php
  $type  = $content['type'];
  $bg_image_url = isset($content['background']->uri) ? $content['background']->uri : false;
  $color = $content['color'];
  $description = $content['description']['value'];
?>

<?php if($type) :?> 
  <?php
    if($bg_image_url) {
        print '<section class="statement" style="background-image: url('.file_create_url($bg_image_url).')">';	
    }else {
        print '<section class="statement" style="background-color: #'.$color.'">';
    }
  ?>
    <div class="content"><?php print $description;?></div>
  </section>
<?php else: ?>
  <section class="line">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
            <?php
				if($bg_image_url) {
					print '<div class="action-call red" style="background-image: url('.file_create_url($bg_image_url).')">';	
				}else {
					print '<div class="action-call red">';
				}
			?>
              	<?php print $description;?>
            </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>