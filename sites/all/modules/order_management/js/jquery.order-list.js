(function ($) {
    function CalculateTotal() {
        $("#order-total-bucket").val("0.00");
        
        $(".item-total").each(function() {
            var current = parseFloat($("#order-total-bucket").val());
            var addValue = parseFloat($(this).val());
            var newTotal = current + addValue;
            
            $(".order-total").html(newTotal.toFixed(2));
            $("#order-total-bucket").val(newTotal);
        });
    }
    $(document).ready(function() {
        $(".quantity-input").on("change", function() {
            var id = $(this).attr("pid");
            var bulkMinimum = parseInt($(this).attr("bulk-min"));
            var unitCost = $(this).attr("unit-cost");
            var unitCostBulk = $(this).attr("unit-cost-bulk");
            var priceLB = $(this).attr("price-lb");
            var priceLBBulk = $(this).attr("price-lb-bulk");
            var quantity = parseInt($(this).val());

            var cost = 0;
            var costLB = 0;
            var priceModel = "";
            var total = 0;

            if (bulkMinimum > 1 && quantity >= bulkMinimum) {
                cost = unitCostBulk;
                costLB = priceLBBulk;
                priceModel = "bulk";
                $("#total-" + id).removeClass("black-text").addClass("red-text");
                $(this).addClass("red-text");
            } else {
                cost = unitCost;
                costLB = priceLB;
                priceModel = "regular";
                $("#total-" + id).addClass("black-text").removeClass("red-text");
                $(this).removeClass("red-text");                
            }

            total = quantity * cost;
            
            $("#total-" + id).html('$' + total.toFixed(2));
            $("#total-holder-" + id).val(total.toFixed(2));
            $("#cost-" + id).val(cost);
            $("#pricemodel-" + id).val(priceModel);
            $("#lb-" + id).val(costLB);
            
            CalculateTotal();
        });
    });
})(jQuery);