(function ($) { 
    $(document).ready(function() {
       $("#master-checked").on("change", function() {
            if ($(this).prop("checked")) {
                $(".email-recipient").prop("checked", true);
            } else {
               $(".email-recipient").prop("checked", false); 
            }
        }); 
    });
})(jQuery);