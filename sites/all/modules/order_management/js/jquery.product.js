function escapeTags( str ) {
  return String( str )
           .replace( /&/g, '&amp;' )
           .replace( /"/g, '&quot;' )
           .replace( /'/g, '&#39;' )
           .replace( /</g, '&lt;' )
           .replace( />/g, '&gt;' );
}

window.onload = function() {

  var btn = document.getElementById('largeimageupload'),
      progressBar = document.getElementById('progressbarlg'),
      progressOuter = document.getElementById('progressouter'),
      msgBox = document.getElementById('msg-box');

  var uploader = new ss.SimpleUpload({
        button: btn,
        url: '/upload-large',
        name: 'uploadfile',
        hoverClass: 'hover',
        focusClass: 'focus',
        responseType: 'json',
        startXHR: function() {
            progressOuter.style.display = 'block'; // make progress bar visible
            this.setProgressBar( progressBar );
        },
        onSubmit: function() {
            msgBox.innerHTML = ''; // empty the message box
          },
        onComplete: function( filename, response ) {
            btn.innerHTML = 'Choose Another File';
            progressOuter.style.display = 'none'; // hide progress bar when upload is completed

            if ( !response ) {
                msgBox.innerHTML = 'Unable to upload file';
                return;
            }

            if ( response.success === true ) {
            	jQuery('#largeimage_inputname').val(filename);
                msgBox.innerHTML = '<strong>' + escapeTags( filename ) + '</strong>' + ' successfully uploaded.';
            	jQuery('#largeimageupload').css('background-image','url(/sites/default/files/products/large/'+filename+')');

            } else {
                if ( response.msg )  {
                    msgBox.innerHTML = escapeTags( response.msg );

                } else {
                    msgBox.innerHTML = 'An error occurred and the upload failed.';
                }
            }
            
          },
        onError: function() {
            //alert('oppo');
            progressOuter.style.display = 'none';
            msgBox.innerHTML = 'Unable to upload file';
          }
	});
  
  
  var thumbnailbtn = document.getElementById('thumbnailupload'),
  thumbnailprogressBar = document.getElementById('thumbnailprogressbarlg'),
  thumbnailprogressOuter = document.getElementById('thumbnailprogressouter'),
  thumbnailmsgBox = document.getElementById('thumbnail-msg-box');

var tnuploader = new ss.SimpleUpload({
    button: thumbnailbtn,
    url: '/upload-thumbnail',
    name: 'uploadfile',
    hoverClass: 'hover',
    focusClass: 'focus',
    responseType: 'json',
    startXHR: function() {
    	thumbnailprogressOuter.style.display = 'block'; // make progress bar visible
        this.setProgressBar( thumbnailprogressBar );
    },
    onSubmit: function() {
        msgBox.innerHTML = ''; // empty the message box
      },
    onComplete: function( filename, response ) {
        btn.innerHTML = 'Choose Another File';
        thumbnailprogressOuter.style.display = 'none'; // hide progress bar when upload is completed

        if ( !response ) {
        	thumbnailmsgBox.innerHTML = 'Unable to upload file';
            return;
        }

        if ( response.success === true ) {
        	jQuery('#thumbnail_inputname').val(filename);
        	thumbnailmsgBox.innerHTML = '<strong>' + escapeTags( filename ) + '</strong>' + ' successfully uploaded.';
        	jQuery('#thumbnailupload').css('background-image','url(/sites/default/files/products/small/'+filename+')');
        	
        } else {
            if ( response.msg )  {
            	thumbnailmsgBox.innerHTML = escapeTags( response.msg );

            } else {
            	thumbnailmsgBox.innerHTML = 'An error occurred and the upload failed.';
            }
        }
        
      },
    onError: function() {
        //alert('oppo');
    	thumbnailprogressOuter.style.display = 'none';
        thumbnailmsgBox.innerHTML = 'Unable to upload file';
      }
});
};
