<?php

function om_get_products_by_category_nav($CategoryID, $DeliveryID = 0) {
  $Return = array(
                  "StatusCode"  =>  0
                  );
  if (intval($CategoryID) > 0) {
    if (intval($DeliveryID) > 0) {
        $DeliverySQL = "LEFT JOIN om_VIEW_productavailability AS pa ON pa.productid = p.id AND pa.deliveryid = $DeliveryID";
        $DeliverySelectSQL = ", if(pa.productid IS NULL, 0, 1) AS productavailable";
    } else {
        $DeliverySQL = "";
        $DeliverySelectSQL = "";
    }
    $Query = "SELECT p.*
                FROM om_products AS p
                WHERE p.status = 'actv'
                AND p.visible=1
                AND p.categoryid = $CategoryID
                ORDER BY p.pid ASC";
               // echo "<PRE>$Query</PRE>"; 
     $Results = db_query($Query);
  
      $Rows = $Results->fetchAll();
      
      if (count($Rows) > 0) {
          $Return["StatusCode"] = 1;
          $Return["Data"] = $Rows;
      }
   
  } else {
    $Return["ErrorMessage"] = "Category ID is required!";
  }
    return $Return;
}
?>