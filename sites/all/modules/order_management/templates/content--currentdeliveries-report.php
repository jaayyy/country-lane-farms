<?php
$Report = om_get_current_deliveries_report();

if ($Report["StatusCode"] == 1) {
   $Report = $Report["Data"];
} else {
    $Report = false;
}

if ($Report != false) {
?>
<table class="table table-striped">
    <tr>
        <th>
            Delivery Date
        </th>
        <th>
            Location
        </th>
        <th>
            Delivery Type
        </th>
        <th>
           Total Orders
        </th>
        <th>
            Estimated Revenue
        </th>
        <th>
            Open Orders
        </th>
        <th>
            Sales Report
        </th>
    </tr>
    <tbody>
        <?php
 
        foreach($Report as $r) {
        ?>
    <tr>
        <td>
            <?php echo $r->deliverydate; ?>
        </td>
        <td>
            <?php echo $r->location; ?>
        </td>
        <td>
            <?php echo $r->deliverytype; ?>
        </td>
        <td>
            <?php echo $r->orders; ?>
        </td>
        <td>
           <?php echo om_money($r->revenue); ?>
        </td>
        <td>
            <a href="/admin/order_management/orderqueue/?d=<?php echo $r->detailid; ?>" class="btn btn-primary">Open Orders</a>
        </td>
        <td>
            <a href="/admin/order_management/locationproductsales/?d=<?php echo $r->detailid; ?>" class="btn btn-primary">Sales Report</a>
        </td>
    </tr>
    <?php
        }
    ?>
    </tbody>
</table>
<?php
}
?>