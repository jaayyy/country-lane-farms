<?php
drupal_add_js($base_url."/".drupal_get_path('module', 'order_management')."/js/jquery.weigh-in.js");
$OrderID = intval($_REQUEST["o"]);
$Details = om_get_order($OrderID);
$DelivereyID = intval($_REQUEST["d"]);

if ($Details["StatusCode"] == 1) {
    $Details = $Details["Data"];
} else {
    $Details = false;
}

    $DetailsID = intval($OrderDetails->deliveryid);
    $DeliveryID = om_get_deliveryid($DetailsID);
    if ($DeliveryID["StatusCode"] == 1) {
        $DeliveryID = intval($DeliveryID["Data"]);
        }

$OrderItems = om_order_items($OrderID);

$Items = array();
$ItemIDs = array();

if ($OrderItems["StatusCode"] == 1) {
        $OrderItems = $OrderItems["Data"];
        foreach ($OrderItems as $i) {
            $Items[$i->productid] = array("id" => $i->id, "quantity"=>$i->quantity, "total"=>$i->total, "weight" => $i->weight, "weighedtotal" => $i->weighedtotal);
            $ItemIDs[] = $i->productid;
        }
    } else {
        $OrderItems = false;
    }
    
    $ProductData = om_get_product_by_ids($ItemIDs);
    
    if ($ProductData["StatusCode"] == 1) {
        $ProductData = $ProductData["Data"];
    }
?>
<form action="/admin/order_management/omprocessing" method="post" id="checkout-form">
    <input type="hidden" name="o" value="<?php echo $OrderID; ?>" />
    <input type="hidden" name="d" value="<?php echo $DelivereyID; ?>" />
<h1>Weigh In </h1>
<div class="row">
    <div class="col-md-6">
        <h3>Delivery Location: <?php echo $Details->location; ?></h3>
    </div>
    <div class="col-md-6 right">
        <h3>Customer: <?php echo $Details->customer; ?></h3>
            <div class="delivery-date-time">Phone Number:  <?php echo $Details->phone; ?></div>
            <div class="delivery-date-time">Email:  <?php echo $Details->email; ?></div>        
    </div>
</div>
<div class="row">
    <div class="col-md-6 order-receipt-date-time">
        <h4>Date: <?php echo date("D M j/y", $Details->starttime); ?></h4>
    </div>
    <div class="col-md-6 right order-receipt-date-time">
        <h4 class="receipt-top">Estimated Order Total: <span id="est-order-total"><?php echo om_money($Details->total); ?></span></h4>       
    </div>    
</div>

    <input type="hidden" name="a" value="weigh-in" />
<div class="order-receipt-wrapper">
    <div class="row">
        <div class="col-md-6 spacing-bottom">
            <h3 class="receipt-top">Ordered Products</h3>
            <strong>* Add Exact Weights</strong>
        </div>

   <table class="table table-striped">
    <tr>
        <th>
            Products
        </th>
        <th>
            Price
        </th>
        <th>
            Quantity
        </th>
        <th>
            Minimum Bulk Quantity
        </th>
        <th>
            Weight
        </th>
        <th>
            Total
        </th>
    </tr>
    <tbody>
    <?php
    $FixedTotal = 0;
    foreach($ProductData as $p) {

        $BulkMin = $ProductData[$p->pid]->bulkminquantity;

        if ($BulkMin > 1 && intval($Items[$p->pid]["quantity"]) >= $BulkMin) {
            $Price = floatval($p->bulkpricelb);
            $DiscountFlag = "(Bulk Discount)";
            $PriceModel = "bulk";
            ?>
            
            <?php
        } else {
            $Price = floatval($p->pricelb);
            $DiscountFlag = "";
            $PriceModel = "regular";
        }
        
    ?>
    <input type="hidden" name="p[<?php echo $p->pid; ?>]" value="<?php echo $i->bulkminquantity; ?>:<?php echo $p->pricelb; ?>:<?php echo $p->bulkpricelb; ?>" />
    <tr>
        <td  style="text-align: left;">
             <?php echo $p->name; ?>
        </td>
        <td  style="text-align: left;">
             <?php
             
             if ($Price > 0) {
             ?>            
             Price per lb. <?php echo om_money($Price); ?> <?php echo $DiscountFlag; ?> 
             <?php
             } else {
            ?>
            Fixed price per item <?php echo om_money($p->estimatedpriceperitem); ?>
          <?php
             }
          ?>
        </td>
        <td align="center" nowrap>
            <div class="col-xs-4">
                <?php
                $Quantity = intval($Items[$p->pid]["quantity"]);
                ?>                
                <input type="text" value="<?php echo $Quantity; ?>" name="quantity[<?php echo $Items[$p->pid]["id"]; ?>]" pid="<?php echo $p->pid; ?>" class="quantity-input" id="quantity-<?php echo $p->pid; ?>" <?php echo $ProductAvailable["Disable"]; ?>/>
                <input type="hidden" value="" bulk-min="<?php echo $p->bulkminquantity; ?>" price-lb="<?php echo $p->pricelb; ?>" price-lb-bulk="<?php echo $p->bulkpricelb; ?>" unit-cost-bulk="<?php echo $p->estimatedbulkpriceperitem; ?>" unit-cost="<?php echo number_format($p->estimatedpriceperitem, 2, ".", ","); ?>" pid="<?php echo $p->pid; ?>" id="info-<?php echo $p->pid; ?>"/>                                                                                                                                                                                                                                                                                                                                                                                                         
                <input type="hidden" value="<?php echo $p->estimatedpriceperitem; ?>" name="cost[<?php echo $Items[$p->pid]["id"]; ?>]"  id="cost-<?php echo $p->pid; ?>" />
                <input type="hidden" value="<?php echo $Price; ?>" name="lb[<?php echo $Items[$p->pid]["id"]; ?>]"  id="lb-<?php echo $p->pid; ?>" />
                <input type="hidden" value="<?php echo $PriceModel; ?>" name="pricemodel[<?php echo $Items[$p->pid]["id"]; ?>]"  id="pricemodel-<?php echo $p->pid; ?>" />
                <input type="hidden" value="<?php echo $Items[$p->pid]["weighedtotal"]; ?>"  id="total-holder-<?php echo $p->pid; ?>" class="item-total-hidden" />
            </div>
        </td>
        <td align="center" nowrap>
            <?php
            if ($BulkMin > 1) {
            echo $BulkMin;
            } else {
            echo "0";
            }    
            ?>
        </td>        
        <td align="center" nowrap>
            <?php
            if ($Price > 0) {
            ?>
            <div class="col-xs-5">
                <input type="text" name="w[<?php echo $Items[$p->pid]["id"]; ?>]" pid="<?php echo $p->pid; ?>" value="<?php echo floatval($Items[$p->pid]["weight"]); ?>" class="actual-weight form-control"  style="display:inline;" /><span style="display:inline;"> lbs</span>
                <input type="hidden" class="item-price-lb" />
            </div>
            <?php
            } else {
            ?>
            N/A
            <?php
            }
            ?>
        </td>
        <td>
            <?php
            if ($p->pricelb == 0) {
            ?>
            <?php echo om_money($Items[$p->pid]["total"]);
            $FixedTotal += $Items[$p->pid]["total"];
            ?>
            <?php
            } else {
                if ($Items[$p->pid]["weighedtotal"] == 0) {
                    $CSS = "red-text ";
                } else {
                    $FixedTotal += $Items[$p->pid]["weighedtotal"];
                    $CSS = "";
                }
            ?>
            <span class="<?php echo $CSS; ?>item-total" id="total-<?php echo $p->pid; ?>"><?php echo om_money($Items[$p->pid]["weighedtotal"]); ?></span>
            <?php
            }
            ?>
        </td>
    </tr>
    <?php
    }
    ?>
    <tr>
        <td colspan="5" align="right">
           <strong> Weighed Total:</strong>
        </td>
        <td>
            <strong>$<span id="order-total"><?php echo number_format($FixedTotal, 2, ".", ","); ?></span>  </strong>  
        </td>
    </tr>
    </tbody>
   </table>
   <input type="hidden" name="ft" value="<?php echo $FixedTotal; ?>" class="item-total-hidden"/>
   </div>
</div><br/>
<div class="form-buttons">
    <a href="javascript:window.history.back();" class="btn btn-warning">Cancel</a>
    <input type="submit" value="Save" class="btn btn-primary" />
</div>
<input type="hidden" id="order-total-bucket" value="<?php echo $FixedTotal; ?>" />
</form>