<?php

$Products = om_get_products();

if ($Products["StatusCode"] == 1) {
    $Products = $Products["Data"];
} else {
    $Products = false;
}
?>
<div class="top-buttons">
    <a href="/admin/order_management/product" class="btn btn-success">Create New Product</a> 
</div>
<table class="table table-striped">
    <tbody>
        <tr>
            <th>
                Product ID
            </th>
            <th>
                Name
            </th>
            <th>
                Summary
            </th>
            <th>
                Category
            </th>
            <th>
                Created
            </th>
            <th>
                Last Update
            </th>
            <th>
                Action
            </th>
        </tr>
        <?php
         foreach($Products as $p) {
            if ($p->available == 0) {
                $StatusClass = "background-color: #fcf8e3 !important;";
            } else if ($p->visible == 0) {
                $StatusClass = "background-color: #f2dede !important;";
            } else {
                $StatusClass = "";
            }
        ?>
        <tr style="<?php echo $StatusClass; ?>">
            <td style="<?php echo $StatusClass; ?>">
                <?php echo $p->pid; ?>
            </td>
            <td style="text-align: left;<?php echo $StatusClass; ?>">
                <?php
                echo $p->name;
                ?>
            </td>
            <td style="text-align: left;<?php echo $StatusClass; ?>">
                <?php echo $p->summary; ?>
            </td>
            <td style="<?php echo $StatusClass; ?>">
                <?php echo $p->category; ?>
            </td>
            <td style="<?php echo $StatusClass; ?>">
                <?php echo date("Y-m-d H:i:s", $p->created); ?>
            </td>
            <td style="<?php echo $StatusClass; ?>">
                <?php echo date("Y-m-d H:i:s", $p->lastupdated); ?>
            </td>
            <td style="<?php echo $StatusClass; ?>">
                <a href="/admin/order_management/product/?p=<?php echo $p->id; ?>" class="btn btn-primary btn-sm">Edit</a>
            </td>
        </tr>       
        <?php
        }
        ?>
    </tbody>
</table>
<style type="text/css">
    .legend-thing li {
        list-style: none;
        margin: 3px;
        line-height: 25px;
    }
    
    .legend-thing li span.legend{
        display: inline-block;
        width: 30px;
        height:30px;
        border: 1px solid #ccc;
        border-radius: 4px;
        margin-right: 5px;
        
    }
    
    .legend-thing li span.legend.not-available{
        background-color: #fcf8e3;
    }
    
    .legend-thing li span.legend.not-visible{
        background-color: #f2dede;
    }
    
    .legend-thing li.title {
        font-weight: bold;
        
    }
    
</style>
       
<ul class="legend-thing">
    <li class="title">
        Legend
    </li>
    <li>
        <span class="legend not-available"></span> Not Available
        
    </li>
    <li>
        <span class="legend not-visible"></span> Not Visible
    </li>
</ul>