<?php
$OrderID = intval($_REQUEST["o"]);
$Details = om_get_delivery_details_by_order($OrderID);
$DeliveryID = intval($_REQUEST["d"]);

if ($Details["StatusCode"] == 1) {
    $Details = $Details["Data"];
} else {
    $Details = false;
}
$DetailsID = $Details->deliveryid;
$OrderItems = om_order_items($OrderID);

if ($OrderItems["StatusCode"] == 1) {
    $OrderItems = $OrderItems["Data"];
    $ItemIDs = array();
    foreach($OrderItems as $i) {
        $ItemIDs[] = $i->productid;
    }
    
    $ProductData = om_get_product_by_ids($ItemIDs);
    
    if ($ProductData["StatusCode"] == 1) {
        $ProductData = $ProductData["Data"];
    }
    
} else {
    $OrderItems = false;
}
?>
<form action="/admin/order_management/omprocessing" method="post" id="checkout-form">
    <input type="hidden" name="o" value="<?php echo $OrderID; ?>" />
    <input type="hidden" name="d" value="<?php echo $DeliveryID; ?>" />
<h1>Final Order Summary </h1>
<div class="row">
    <div class="col-md-6">
        <h3>Delivery Location: <?php echo $Details->location; ?></h3>
    </div>
    <div class="col-md-6 right">
        <h3>Customer: <?php echo $Details->customer; ?></h3>        
    </div>
</div>
<div class="row">
    <div class="col-md-6 order-receipt-date-time">
        <h4>Date: <?php echo date("D M j/y", $Details->starttime); ?></h4>
    </div>
    <div class="col-md-6 right order-receipt-date-time">
        <h3><?php echo date("h:ia"); ?>  </h3>    
    </div>    
</div>

    <input type="hidden" name="a" value="weigh-in" />
<div class="order-receipt-wrapper">
    <div class="row">
        <div class="col-md-6 spacing-bottom left-col">
            <h3 class="receipt-top">Order Receipt</h3>
            <p>
                
            </p>
        </div>
        <div class="col-md-6 right-col right">
           <h4 class="receipt-top">Order Total: <span id="order-total"><?php echo om_money($Details->totalweighed); ?></span></h4> 
        </div>

   <table class="table table-striped">
    <tr>
        <th  style="text-align: left;">
            Products
        </th>
        <th  style="text-align: left;">
            Price
        </th>
        <th  style="text-align: left;">
            Quantity
        </th>
        <th>
            Weight
        </th>
        <th>
            Total
        </th>
    </tr>
    <tbody>
    <?php
    $FixedTotal = 0;
    foreach($OrderItems as $i) {
        
        $BulkMin = $ProductData[$i->productid]->bulkminquantity;

        if ($BulkMin > 1 && $i->quantity >= $BulkMin) {
            $Price = $i->bulkpricelb;
            $DiscountFlag = "<br/><span class='red-text'>(Bulk Discount)</span>";
            ?>
            
            <?php
        } else {
            $Price = $i->pricelb;
            $DiscountFlag = "";
        }
        
    ?>
    <input type="hidden" name="p[<?php echo $i->id; ?>]" value="<?php echo $ProductData[$i->productid]->bulkminquantity; ?>:<?php echo $ProductData[$i->productid]->pricelb; ?>:<?php echo $ProductData[$i->productid]->bulkpricelb; ?>" />
    <tr>
        <td style="text-align: left;">
             <?php echo $i->name; ?>
        </td>
        <td style="text-align: left;">
             <?php
             if ($i->pricelb != 0) {
             ?>            
             Price per lb. <?php echo om_money($Price); ?> <?php echo $DiscountFlag; ?>
             <?php
             } else {
            ?>
            Fixed price per item <?php echo om_money($i->estimatedpriceperitem); ?>
            <?php
            $FixedTotal += $i->estimatedpriceperitem;
             }
             ?>           
        </td>
        <td align="center" nowrap>
            <div class="col-xs-4">
                <?php echo intval($i->quantity); ?>
            </div>
        </td>
        <td  style="text-align: right;" nowrap>
            <?php
            if ($i->pricelb != 0) {
            ?>
           
                <?php echo floatval($i->weight); ?> lbs
        
            <?php
            } else {
            ?>
            N/A
            <?php
            }
            ?>
        </td>
        <td  style="text-align: right;">
            <?php
            if ($i->pricelb == 0) {
            ?>
            <?php echo om_money($i->total); ?>
            <?php
            } else {
                if ($i->weighedtotal == 0) {
                    $CSS = "red-text ";
                } else {
                    $CSS = "";
                }
            ?>
            <span class="<?php echo $CSS; ?>item-total"><?php echo om_money($i->weighedtotal); ?></span>
            <?php
            }
            ?>
        </td>
    </tr>
    <?php
    }
    ?>
    </tbody>
   </table>
   <input type="hidden" name="ft" value="<?php echo $FixedTotal; ?>" />
   </div>
</div><br/>
<div class="form-buttons">
    <a href="/admin/order_management/checkout/?o=<?php echo $OrderID; ?>&d=<?php echo $DetailsID; ?>" class="btn btn-warning">Go Back</a>
    <a href="/admin/order_management/omprocessing/?a=order-complete&o=<?php echo $OrderID; ?>&d=<?php echo $DetailsID; ?>" class="btn btn-success">Send Email Receipt</a>
</div>
</form>