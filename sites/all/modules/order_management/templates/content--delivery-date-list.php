<?php
global $base_url;
drupal_add_js($base_url."/".drupal_get_path('module', 'order_management')."/js/jquery.delivery-date-list.js");
$Deliveries = om_get_delivery_dates();

if ($Deliveries["StatusCode"] == 1) {
    $Deliveries = $Deliveries["Data"];    
} else {
    $Deliveries = false;
}
?>
<div class="top-buttons">
    <a href="/admin/order_management/deliverydate" class="btn btn-success">Create New Delivery Date</a>
</div>
<?php
if ($Deliveries == false) {
    ?>
    <div class="alert alert-info" role="alert">
        There are no Deliveries Dates to display. :(
    </div>
    <?php
} else {
?>
<table class="table table-striped">
    <tbody>
        <tr>
            <th>
                Delivery ID
            </th>
            <th>
               Date
            </th>
            <th>
                Type
            </th>
            <th>
                Num Locations
            </th>
            <th>
                Num Orders
            </th>
            <th>
                Created
            </th>
            <th>
                Last Update
            </th>
            <th>
                Action
            </th>
        </tr>
        <?php
         foreach($Deliveries as $d) {
        ?>
        <tr id="delivery-date-row-<?php echo $d->id; ?>">
            <td  align="center">
                <?php echo $d->id; ?>
            </td>
            <td>
                <?php
                echo $d->deliverydate;
                ?>
            </td>
            <td>
                <?php echo $d->deliverytype; ?>
            </td>
            <td align="center">
                <?php echo $d->locations; ?>
            </td>
            <td  align="center">
                <?php echo $d->numberorders; ?>
            </td>
            <td>
                <?php echo $d->created; ?>
            </td>
            <td>
                <?php echo $d->lastupdated; ?>
            </td>
            <td>
                <a href="/admin/order_management/deliverydate/?d=<?php echo $d->id; ?>" class="btn btn-primary btn-sm">Edit</a>
                <a href="javascript:void(null);" class="btn btn-danger btn-sm delete-delivery-date" delivery-type="<?php echo $d->deliverytype ?>" delivery-date="<?php echo $d->deliverydate; ?>" delivery-id="<?php echo $d->id; ?>" num-orders="<?php echo $d->numberorders; ?>">Delete</a>
            </td>
        </tr>       
        <?php
        }
        ?>
    </tbody>
</table>
<!-- Modal -->
<div class="modal fade" id="delete-delivery-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirm Delivery Date Deletion</h4>
            </div>
            <div class="modal-body">
            <p>
                 Do you really want to delete this <span id="delivery-type"></span> delivery on <span id="delivery-date"></span>?
            </p>
            <p class="alert alert-warning" id="orders-warning">
                <strong>Please Note:</strong> This delivery has <span id="open-orders"></span> open orders associated with it.
            </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="confirm-delete-button" delivery-id="">Confirm Delete</button>
            </div>
        </div>
     </div>
</div>
<?php
}
?>