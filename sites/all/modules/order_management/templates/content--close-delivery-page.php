<?php
global $base_url;
drupal_add_js($base_url."/".drupal_get_path('theme', 'bootstrap')."/js/jquery.missed-orders.js");
$DeliveryItemID = intval($_REQUEST["d"]);
$Delivery = om_get_delivery_item_details($DeliveryItemID);

if ($Delivery["StatusCode"] == 1) {
    $Delivery = $Delivery["Data"];
} else {
    $Delivery = false;
}
$DeliveryID = $Delivery->deliveryid;
?>
<script type="text/javascript">
    var AJAX_URL = '<?php echo $base_url."/".drupal_get_path('theme', 'bootstrap')."/ajax"; ?>';
</script>
<form action="/admin/order_management/omprocessing/" id="close-delivery-form" method="post">
    <input type="hidden" name="a" value="close-delivery" />
    <input type="hidden" name="d" value="<?php echo $DeliveryItemID; ?>" />
<div class="row admin-wrapper">
    <div class="order-location-wrapper col-md-10">
        <ul class="order-location-header">
            <li>
                <div>Missed Orders</div>
                <span><?php echo intval($Delivery->open); ?></span>
            </li>
            <li>
                <div>Completed Orders</div>
                <span><?php echo intval($Delivery->completed); ?></span>        
            </li>
            <li>
                <div>Current Time</div>
                <span><?php echo date("g:i A"); ?></span>
            </li>
        </ul>
    </div>
    <div class="col-md-10">
        <div class="col-md-7">
            <div id="delivery-location-text">Delivery Location:  <?php echo $Delivery->location; ?></div>
            <div class="delivery-date-time">Date: <?php echo $Delivery->deliverydate; ?></div>
            <div class="delivery-date-time">Time:  <?php echo strtolower($Delivery->start); ?> - <?php echo strtolower($Delivery->end); ?></div>
        </div>
        <div class="col-md-5">
            <div class="button-wrapper">
                <input type="submit" value="Send Email &amp; Close Delivery" class="btn btn-warning btn-lg"/>
            </div>
        </div>
    </div>
    <div class="col-md-10">
        <h3>Missed Orders</h3>
        <?php
        
        $MissedOrders =  om_order_queue_get($DeliveryItemID, "missed");
        
        if ($MissedOrders["StatusCode"] == 1) {
            $MissedOrders = $MissedOrders["Data"];
        } else {
        }
        
        if ($MissedOrders != false) {
        ?>
        <table class="table table-striped order-queue">
            <tr>
                <th>
                    Email Recipients<br>
                    <input type="checkbox" id="master-checked" />
                </th>
                <th>
                    Customer Name
                </th>
                <th>
                    Order Placed
                </th>
                <th>
                    Total # of <br />
                    Items
                </th>
                <th>
                    Est Order <br />
                    Total
                </th>
                <th>
                    View
                </th>
                <th>
                   Review/Alter
                </th>
                <th>
                    Admin
                </th>
            </tr>
            <tbody>
                <?php
                foreach($MissedOrders as $o) {
                ?>
                <tr id="row-<?php echo $o->id; ?>">
                    <td align="center">
                        <input type="checkbox" name="e[<?php echo $o->id;?>]" class="email-recipient form-control"  value="1" id="" />
                    </td>
                    <td>
                        <?php echo $o->name; ?>
                    </td>
                    <td>
                        <?php echo $o->ordercreated; ?>
                    </td>
                    <td align="center">
                        <?php echo $o->numberitems; ?>
                    </td>
                    <td align="right">
                        <?php echo om_money($o->ordertotal); ?>
                    </td>
                    <td>
                        <a href="/admin/order_management/vieworder/?o=<?php echo $o->id; ?>&di=<?php echo $DeliveryItemID; ?>&d=<?php echo  $DeliveryID; ?>" class="btn btn-primary">View Order</a>
                    </td>
                    <td align="center">
                        <a href="/admin/order_management/editorder/?o=<?php echo $o->id; ?>&di=<?php echo $DeliveryItemID; ?>&d=<?php echo $DeliveryID; ?>" class="btn btn-primary">Edit Order</a>
                    </td>
                    <td align="center">
                        <a href="javascript:void(null);" customer="<?php echo $o->name; ?>" amount="<?php echo $o->ordertotal; ?>" order-id="<?php echo $o->id; ?>" class="btn btn-danger delete-order" >Delete</a>
                    </td>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        <?php
        } else {
        ?>
        <div>
            No open orders were found.
        </div>    
        <?php
        }
        ?>
    </div>
    
    <div class="col-md-10">
        <h3>Completed Orders</h3>
        <?php
        
        $CompletedOrders = om_order_queue_get($DeliveryItemID, "cmpt");
        
        if ($CompletedOrders["StatusCode"] == 1) {
            $CompletedOrders = $CompletedOrders["Data"];
        } else {
            $CompletedOrders = false;
        }
        
        if ($CompletedOrders != false) {
        ?>
        <table class="table table-striped order-queue">
            <tr>
                <th>
                    Customer Name
                </th>
                <th>
                    Order Placed
                </th>
                <th>
                    Order Completed
                </th>
                <th>
                    Total # of <br />
                    Items
                </th>
                <th>
                    Order Total
                </th>
                <th>
                   Admin
                </th>
            </tr>
            <tbody>
                <?php
                foreach($CompletedOrders as $o) {
                ?>
                <tr>
                    <td>
                        <?php echo $o->name; ?>
                    </td>
                    <td>
                        <?php echo $o->ordercreated; ?>
                    </td>
                    <td>
                        <?php echo $o->ordercompleted; ?>
                    </td>
                    <td align="center">
                        <?php echo $o->numberitems; ?>
                    </td>
                    <td align="right">
                        <?php echo om_money($o->ordertotal); ?>
                    </td>
                    <td align="center">
                        <a href="/admin/order_management/vieworder/?o=<?php echo $o->id; ?>&di=<?php echo $DeliveryItemID; ?>&d=<?php echo  $DeliveryID; ?>" class="btn btn-primary">View Order</a>
                    </td>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        <?php
        } else {
        ?>
        <div>
            No completed orders were found.
        </div>    
        <?php
        }
        ?>            
</div>
</form>