<?php
$DeliveryItemID = intval($_REQUEST["di"]);
$DeliveryID = $_REQUEST["d"];

$Delivery = om_get_delivery_item_details($DeliveryItemID);
if ($Delivery["StatusCode"] == 1) {
    $Delivery = $Delivery["Data"];
} else {
    $Delivery = false;
}
$OrderID = intval($_REQUEST["o"]);
$Categories = om_get_product_categories();
if ($Categories["StatusCode"] == 1) {
    $Categories = $Categories["Data"];
} else {
    $Categories = false;
}

$OrderDetails = om_get_delivery_info($OrderID);

if ($OrderDetails["StatusCode"] == 1) {
    $OrderDetails = $OrderDetails["Data"];
} else {
    $OrderDetails = false;
}

$OrderItems = om_order_items($OrderID);

$Items = array();

if ($OrderItems["StatusCode"] == 1) {
    $OrderItems = $OrderItems["Data"];
    
    foreach ($OrderItems as $i) {
        
        $PriceLB = $i->pricelb;
        $EstimatedPerItem = $i->unitcost;
        
        $Items[$i->productid] = array("quantity"=>$i->quantity, "weight"=>$i->weight, "total"=>$i->total, "weighedtotal"=>$i->weighedtotal, "pricelb"=>$PriceLB, "estimatedpriceperitem"=>$EstimatedPerItem, "pricemodel"=>$i->pricemodel);        
    
    }
} else {
    $OrderItems = false;
}

$LocationDropdown = om_delivery_location_dropdpown($OrderID);

if ($LocationDropdown["StatusCode"] == 1) {
    $LocationDropdown = $LocationDropdown["Data"];
} else {
    $LocationDropdown = false;
}
?>
<link rel="stylesheet" href="<?php echo $base_url."/".drupal_get_path('module', 'order_management')?>/css/style.css" type="text/css" media="screen" />
    <form action="/admin/order_management/omprocessing" method="post" id="complete-order-form">

    <div class="content col-md-10 col-md-offset-1"> 
        <div class="option-info section row ">
            <h1>View Order</h1>
            <div class="row well">
                <div class="left-col col-md-6">
                        <!--<label>
                            Delivery Location
                        </label>
                            <?php
                            /*foreach($LocationDropdown as $l) {
                                $Item = "<strong>".$l->location."</strong><br/>".date("D M jS", $l->starttime)."<br/>".date("g:ia",$l->starttime);
                                if ($l->locationid == $OrderDetails->locationid) {
                                    $SelectedLocationText = $Item;
                                    $CSS = " hidden ";
                                } else {
                                    $CSS = "";
                                }
                            }*/
                            ?>-->
                    <div id="delivery-location-text">Delivery Location:  <?php echo $Delivery->location; ?></div>
                    <div class="delivery-date-time">Date: <?php echo $Delivery->deliverydate; ?></div>
                    <div class="delivery-date-time">Time:  <?php echo date("g:ia", $Delivery->starttime); ?> - <?php echo date("g:ia", $Delivery->endtime); ?></div>
                    <div class="delivery-date-time">Last Name:  <?php echo $OrderDetails->lastname; ?></div>
                    <div class="delivery-date-time">First Name:  <?php echo $OrderDetails->firstname; ?></div>
                    <div class="delivery-date-time">Phone Number:  <?php echo $OrderDetails->phone; ?></div>
                    <div class="delivery-date-time">Email:  <?php echo $OrderDetails->email; ?></div>
                </div>
                <div class="right-col col-md-6 right">
                    <?php
                    if ($OrderDetails->status == "cmpt") {
                        $OrderTotal = $OrderDetails->totalweighed;
                    } else {
                        $OrderTotal = $OrderDetails->total;
                    }
                    ?>
                  <h4>Order Total: $<span id="order-total"><?php echo number_format($OrderTotal, 2, ".", ","); ?></span></h4>
                  <a href="javascript:window.history.back();" class="btn btn-warning">Back</a> 
                </div>
            </div>
        </div>
<?php
foreach($Categories as $c) {
    $Products = om_get_products_by_category($c->id, $DeliveryID);
    if ($Products["StatusCode"] == 1) {
         $Products = $Products["Data"];
 

    foreach($Products as $p) {
        if(isset($Items[$p->pid])) {
        ?>
        <div class="row order-item">
          <div class="pic col-md-2">
            <img src="/sites/default/files/products/small/<?php echo $p->thumbnail; ?>"  alt="" class="admin-pic">
          </div>
          <div class="col-md-4">
            <h4><?php echo $p->name; ?></h4>
            <p>
            <?php echo $p->summary; ?>
            </p>
            <?php
            if ($p->bulkminquantity > 1) {
            ?>
            <p class="red-text">Minimum Bulk Quantity: <?php echo $p->bulkminquantity; ?>+</p>
            <?php
            }
            ?>
          </div>
          <div class="price col-md-2">
            <?php
            if ($p->pricelb > 0) {
            ?>            
            <p>Price per lb. $<?php echo number_format($p->pricelb, 2, ".", ","); ?></p>
            <?php
            }
            ?>            
            <?php
            if ($p->estimatedpriceperitem > 0) {
            ?>              
            <p>Estimated Cost Per Item $<?php echo number_format($p->estimatedpriceperitem, 2, ".", ","); ?></p>
            <?php
            }
            ?>
            <?php
            if ($p->bulkpricelb > 0) {
            ?>              
            <p class="red-text">Bulk Price per lb. $<?php echo number_format($p->bulkpricelb, 2, ".", ","); ?></p>
            <?php
            }
            ?>
            <?php
            if ($p->estimatedbulkpriceperitem > 0) {
            ?>             
            <p class="red-text">Estimated Bulk Cost Per Item $<?php echo number_format($p->estimatedbulkpriceperitem, 2, ".", ","); ?></p>
            <?php
            }
            ?>            
          </div>
        <div class="quantity col-md-2">
            <h4>Quantity</h4>
            <?php
            if ($OrderDetails->status == "cmpt" && $Items[$p->pid]["pricelb"] > 0) {
                $WeightAdd = " (".$Items[$p->pid]["weight"]." lbs)";
            } else {
                $WeightAdd = "";
            }
            
            $Quantity = intval($Items[$p->pid]["quantity"]).$WeightAdd;
            ?>            
            <?php echo $Quantity; ?>
        </div>
          <div class="total col-md-2">
            <?php
                if ($OrderDetails->status == "cmpt") {
                    $TotalTitle = "Weighed Total";
                    $ItemTotal = $Items[$p->pid]["weighedtotal"];
                } else {
                    $TotalTitle = "Est. Total";
                    $ItemTotal = $Items[$p->pid]["total"];
                }
                
                if ($Items[$p->pid]["pricelb"] == 0) {
                    $TotalTitle = "Total";
                    $ItemTotal = $Items[$p->pid]["estimatedpriceperitem"] * $Items[$p->pid]["quantity"];                    
                }
            ?>
            <h4 style="text-align: right;"><?php echo $TotalTitle; ?></h4>
            <?php
            $Total = number_format($ItemTotal, 2, ".", ",");
            ?>
            <div style="text-align: right;">
            $<?php echo $Total; ?>
            </div>
          </div>
        </div>
        <?php
        }
    }
}
}
?>
    </div>
    </div>