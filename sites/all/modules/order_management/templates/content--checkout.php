<?php
drupal_add_js($base_url."/".drupal_get_path('module', 'order_management')."/js/jquery.weigh-in.js");
drupal_add_js($base_url."/".drupal_get_path('module', 'order_management')."/js/jquery.checkout.js");
drupal_add_js($base_url."/".drupal_get_path('module', 'order_management')."/js/jquery.vertical-align.js");

$OrderID = intval($_REQUEST["o"]);
$DetailsID = intval($_REQUEST["d"]);
$Details = om_get_order_summary($OrderID);

if ($Details["StatusCode"] == 1) {
    $Details = $Details["Data"];
} else {
    $Details = false;
}


$DeliveryID = om_get_deliveryid($DetailsID);

if ($DeliveryID["StatusCode"] == 1) {
    $DeliveryID = intval($DeliveryID["Data"]);
}

$OrderItems = om_order_items($OrderID);

$Items = array();
$ItemIDs = array();
$Displayed = array();
$FixedTotal = 0;
if ($OrderItems["StatusCode"] == 1) {
        $OrderItems = $OrderItems["Data"];

        foreach ($OrderItems as $i) {
            
            $Displayed[$i->productid] = true;
            if (floatval($i->pricelb) == 0) {
                $Total = $i->total;
                $FixedTotal += $i->total;
            } else {
                $Total = $i->weighedtotal;

            }
            $Items[$i->productid] = array("id" => $i->id, "quantity"=>$i->quantity, "total"=>$Total, "weight" => $i->weight, "weighedtotal" => $i->weighedtotal);
            $ItemIDs[] = $i->productid;
        }
    } else {
        $OrderItems = false;
    }

    $ProductData = om_get_product_by_ids($ItemIDs);
    
    if ($ProductData["StatusCode"] == 1) {
        $ProductData = $ProductData["Data"];
    }

$HiddenProducts = om_get_products_by_availability($DeliveryID, 0);

if ($HiddenProducts["StatusCode"] == 1) {
    $hp = array();
    foreach($HiddenProducts["Data"] as  $v) {
      $hp[$v->pid] = $v;
    }
    ksort($hp,SORT_NUMERIC);
    $HiddenProducts = $hp;
} else {
    $HiddenProducts = false;
}
    $Categories = om_get_product_categories();
    
    if ($Categories["StatusCode"] == 1) {
        $Categories = $Categories["Data"];
    } else {
        $Categories = false;
    }
?>
<link rel="stylesheet" href="<?php echo $base_url."/".drupal_get_path('module', 'order_management')?>/css/style.css" type="text/css" media="screen" />
<form action="/admin/order_management/omprocessing" method="post" id="checkout-form">
    <input type="hidden" name="o" value="<?php echo $OrderID; ?>" />
    <input type="hidden" name="d" value="<?php echo $DeliveryID; ?>" />
<h1>Checkout </h1>
<div class="row well">
<div class="row">
    <div class="col-md-6">
        <h3>Delivery Location: <?php echo $Details->name; ?></h3>
    </div>
    <div class="col-md-6 right">
        <h3 class="receipt-top">Est Order Total: <span id="order-total-est"><?php echo om_money($Details->ordertotal); ?></span></h3>      
    </div>
</div>
<div class="row">
    <div class="col-md-6 order-receipt-date-time">
        <h4>Date: <?php echo $Details->deliverydate; ?></h4>
        <h4>Time: <?php echo date("g:ia"); ?></h4>
        <h4>Last Name: <?php echo $Details->lastname; ?></h4>
        <h4>First Name: <?php echo $Details->firstname; ?></h4>
        <h4>Phone: <?php echo $Details->phone; ?></h4>
        <h4>Email: <?php echo $Details->email; ?></h4>
    </div>
    <div class="col-md-6 right order-receipt-date-time">
        <a href="javascript:window.history.back();" class="btn btn-warning">Cancel</a>
        <input type="submit" value="Check Out" class="btn btn-success" />
    </div>    
</div>
</div>
    <input type="hidden" name="a" value="checkout" />
<div class="order-receipt-wrapper">
    <div class="row">
        <div class="col-md-6 spacing-bottom">
            <h3 class="receipt-top">Ordered Products</h3>
        </div>
        <div class="col-md-6 right spacing-bottom">

        </div>

 <?php echo om_display_products($ProductData, $Items, true, array(), true); ?>
   <input type="hidden" name="ft" value="<?php echo $FixedTotal; ?>" />
   </div>
</div><br/>
<input type="hidden" id="order-total-bucket" value="<?php echo $FixedTotal; ?>" />
    <div class="row">
        <h3>Hidden Products</h3>
        <?php
        if($HiddenProducts !== false) {
            echo om_display_products($HiddenProducts, "", true, $ItemIDs);
            foreach($HiddenProducts as $p) {
                $ItemIDs[] = $p->pid;
            }
        } else {
        ?>
            <div class="alert alert-info col-md-6">
                No hidden products are currently available.
            </div>
        <?php
        }
          
   ?>
    </div>
</div><br/>
<?php
foreach($Categories as $c) {
?>
<div class="row">
    <?php
    $Available =  om_get_products_by_category($c->id, $DetailsID);
   
    if ($Available["StatusCode"] == 1) {
        $Available = $Available["Data"];

        foreach($Available as $k=>$p) {
            if (!in_array($p->pid, $ItemIDs)) {
            } else {
                unset($Available[$k]);
            }
        }
    }
    if(count($Available) > 0) {
    ?>
    <h3><?php echo $c->name; ?></h3>
    <?php  
        echo om_display_products($Available, "", true, $ItemIDs);
    }
    ?>
</div>
<?php
}
?>
<div class="form-buttons">
    <a href="javascript:window.history.back();" class="btn btn-warning">Cancel</a>
    <input type="submit" value="Check Out" class="btn btn-success" />
</div>
</form>
<?php
function om_display_products($ProductData, $Items = "", $FilterDuplicates = false, $ItemIDs = array(), $ShowTotal = false) {

    ob_start();
    ?>
  <table class="table table-striped">
    <tr>
        <th>
         PID
        </th>
        <th>
            Product
        </th>
        <th>
            Price
        </th>
        <th>
            Quantity
        </th>
        <th>
            Minimum Bulk Quantity
        </th>
        <th>
            Weight
        </th>
        <th>
            Total
        </th>
    </tr>
    <tbody>
    <?php
    $FixedTotal = 0;
   
    foreach($ProductData as $p) {
        if ((!in_array($p->pid, $ItemIDs) && $FilterDuplicates == true) || $FilterDuplicates == false) {
        $BulkMin = $ProductData[$p->pid]->bulkminquantity;

        if ($BulkMin > 1 && (isset($Items[$p->pid]["quantity"]) && intval($Items[$p->pid]["quantity"]) >= $BulkMin)) {
            $Price = floatval($p->bulkpricelb);
            $DiscountFlag = "(Bulk Discount)";
            $PriceModel = "bulk";
            ?>
            
            <?php
        } else {
            $Price = floatval($p->pricelb);
            $DiscountFlag = "";
            $PriceModel = "regular";
        }
        
        if ($Price != 0) {
            $Variable = "p";
            $PricePound = $p->pricelb;
            $PriceBulk = $p->bulkpricelb;
        } else {
            $PricePound = $p->estimatedpriceperitem;
            $PriceBulk = $p->estimatedbulkpriceperitem;
            $Variable = "b";
        }
    ?>
    <input type="hidden" name="<?php echo $Variable; ?>[<?php echo $p->pid; ?>]" value="<?php echo $p->bulkminquantity; ?>:<?php echo $PricePound; ?>:<?php echo $PriceBulk; ?>" />
    <tr>
     <td style="text-align: center;">
      <div class="vertical-align">
          <?php echo $p->pid; ?>
        </div>
     </td>
        <td  style="text-align: left;">
            <div class="vertical-align">
             <?php echo $p->name; ?>
            </div>
        </td>
        <td style="text-align: left;">
            <div class="vertical-align">
             <?php
             
             if ($Price != 0) {
             ?>            
             Price per lb. <?php echo om_money($Price); ?> <?php echo $DiscountFlag; ?> 
             <?php
             } else {
            ?>
            Fixed price per item <?php echo om_money($p->estimatedpriceperitem); ?>
          <?php
             }
          ?>
            </div>
        </td>
        <td align="center" nowrap>
            <div class="col-xs-4 ">
                <?php
                if (is_array($Items)) {
                    $Quantity = intval($Items[$p->pid]["quantity"]);
                    $Total = $Items[$p->pid]["total"];
                    $Weight = $Items[$p->pid]["weight"];
                    $WeighedTotal = $Items[$p->pid]["weighedtotal"];
                    $ID = $Items[$p->pid]["id"];
                } else {
                    $Quantity = 0;
                    $Total = 0;
                    $Weight =  0;
                    $WeighedTotal = 0;
                    $ID = 0;
                }
                ?>                
                <input type="text" value="<?php echo $Quantity; ?>" name="quantity[<?php echo $p->pid; ?>]" pid="<?php echo $p->pid; ?>" class="quantity-input" id="quantity-<?php echo $p->pid; ?>" <?php echo $ProductAvailable["Disable"]; ?>/>
                <input type="hidden" value="" bulk-min="<?php echo $p->bulkminquantity; ?>" price-lb="<?php echo $p->pricelb; ?>" price-lb-bulk="<?php echo $p->bulkpricelb; ?>" unit-cost-bulk="<?php echo $p->estimatedbulkpriceperitem; ?>" unit-cost="<?php echo number_format($p->estimatedpriceperitem, 2, ".", ","); ?>" pid="<?php echo $p->pid; ?>" id="info-<?php echo $p->pid; ?>"/>                                                                                                                                                                                                                                                                                                                                                                                                         
                <input type="hidden" value="<?php echo $p->estimatedpriceperitem; ?>" name="cost[<?php echo $p->pid; ?>]"  id="cost-<?php echo $p->pid; ?>" />
                <input type="hidden" value="<?php echo $Price; ?>" name="lb[<?php echo $p->pid; ?>]"  id="lb-<?php echo $p->pid; ?>" />
                <input type="hidden" value="<?php echo $PriceModel; ?>" name="pricemodel[<?php echo $p->pid; ?>]"  id="pricemodel-<?php echo $p->pid; ?>" />
                <input type="hidden" value="<?php echo $Total; ?>"  id="total-holder-<?php echo $p->pid; ?>" class="item-total-hidden" />
                <input type="hidden" value="<?php echo $ID; ?>"  name="i[<?php echo $p->pid; ?>]" id="itemid-<?php echo $p->pid; ?>" />
            </div>
        </td>
        <td align="center" nowrap>
            <div class="vertical-align">
            <?php
            if ($BulkMin > 1) {
            echo $BulkMin;
            } else {
            echo "0";
            }    
            ?>
            </div>
        </td>        
        <td align="center" nowrap>
            <?php
            if ($Price != 0) {
            ?>
            <div class="col-xs-5">
                <input type="text" name="w[<?php echo $p->pid; ?>]" pid="<?php echo $p->pid; ?>" value="<?php echo floatval($Weight); ?>" class="actual-weight form-control"  style="display:inline;" /><span style="display:inline;"> lbs</span>
                <input type="hidden" class="item-price-lb" />
            </div>
            <?php
            } else {
            ?>
            <div class="col-xs-5 vertical-align">
            N/A
            </div>
            <?php
            }
            ?>
        </td>
        <td>
            <div class="vertical-align">
            <?php
            $CSS = "";
            if ($p->pricelb == 0) {
                $FT = is_array($Items) > 0 ? $Items[$p->pid]["total"] : 0;
            ?><span class="<?php echo $CSS; ?>item-total" id="total-<?php echo $p->pid; ?>">
            <?php echo om_money($FT);
            $FixedTotal += $FT;
            ?></span>
            <?php
            
            } else {
                if ($WeighedTotal == 0) {
                    $CSS = "red-text ";
                } else {
                    $FixedTotal += $WeighedTotal;
                    $CSS = "";
                }
            ?>
            <span class="<?php echo $CSS; ?>item-total" id="total-<?php echo $p->pid; ?>"><?php echo om_money($WeighedTotal); ?></span>
            <?php
            }
            ?>
            </div>
        </td>
    </tr>
    <?php
        }
    }
    if ($ShowTotal == true) {
    ?>
    <tr>
        <td colspan="5" align="right">
           <strong> Weighed Total:</strong>
        </td>
        <td>
            <strong>$<span id="order-total"><?php echo number_format($FixedTotal, 2, ".", ","); ?></span>  </strong>  
        </td>
    </tr>
    <?php
    }
    ?>
    </tbody>
   </table>
  <?php
 return ob_get_clean();
}
?>
<script type="text/javascript">
  jQuery(function() {
    jQuery(".vertical-align").verticalAlign();
    });
</script>