<?php
global $base_url;

drupal_add_js($base_url."/".drupal_get_path('module', 'order_management')."/js/jquery.orders-queue.js");
drupal_add_js($base_url."/".drupal_get_path('module', 'order_management')."/js/jquery.vertical-align.js");

$DeliveryItemID = intval($_REQUEST["d"]);
$Delivery = om_get_delivery_item_details($DeliveryItemID);

if ($Delivery["StatusCode"] == 1) {
    $Delivery = $Delivery["Data"];
} else {
    $Delivery = false;
}

$DeliveryID = $Delivery->deliveryid;
?>
<link rel="stylesheet" href="<?php echo $base_url."/".drupal_get_path('module', 'order_management')?>/css/style.css" type="text/css" media="screen" />
<script type="text/javascript">
    var AJAX_URL = '<?php echo $base_url."/".drupal_get_path('theme', 'bootstrap')."/ajax"; ?>';
</script>
<div class="row admin-wrapper">
    <div class="row well">
        <div class="order-location-wrapper col-md-12">
            <ul class="order-location-header">
                <li>
                    <div>Open Orders: 
                    <span><?php echo intval($Delivery->open); ?></span>
                    </div>
                </li>
                <li>
                    <div>Completed Orders: 
                    <span><?php echo intval($Delivery->completed); ?></span>
                    </div>
                </li>
                <li>
                    <div>Current Time: 
                    <span><?php echo date("g:i A"); ?></span>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-md-12">
            <div class="col-md-7">
                <div id="delivery-location-text">Delivery Location:  <?php echo $Delivery->location; ?></div>
                <div class="delivery-date-time">Date: <?php echo $Delivery->deliverydate; ?></div>
                <div class="delivery-date-time">Time:  <?php echo date("g:ia", $Delivery->starttime); ?> - <?php echo date("g:ia", $Delivery->endtime); ?></div>
            </div>
            <div class="col-md-5">
                <div class="button-wrapper">
                    <?php
                    $ExportData = array(
                                        "id"        => $DeliveryItemID,
                                        "location"  =>  $Delivery->location,
                                        "date"      =>  str_replace(" ", "_", $Delivery->deliverydate)
                                        );
                    $ExportString = base64_encode(json_encode($ExportData));
                    ?>
                    <a href="/admin/order_management/export/?a=orderqueue&d=<?php echo $ExportString; ?>" class="btn btn-info btn-lg btn-block">Download Spreadsheet</a>
                </div>
                <div class="button-wrapper">
                    <?php
                    if ($Delivery->deliverystatus == "open") {
                    ?>
                    <a href="/admin/order_management/closedelivery/?d=<?php echo $DeliveryItemID; ?>" class="btn btn-warning btn-lg btn-block close-delivery" delivery-id="<?php echo $DeliveryItemID; ?>">Close Delivery</a>
                    <?php
                    } else {
                    ?>
                    <div>
                        Delivery was closed on <?php echo date("D M j/y @ g:ia", $Delivery->timeclosed); ?>.
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
    <h3>Order Queue</h3>
    <?php
    
    $QueuedOrders = om_order_queue_get($DeliveryItemID, "queue");
    
    if ($QueuedOrders["StatusCode"] == 1) {
        $QueuedOrders = $QueuedOrders["Data"];
        ?>
        <script type="text/javascript">
        <?php
        $Orders = array();
        foreach($QueuedOrders as $o) {
            $Orders[] = $o->id;
        }
        
        ?>
        var queue = <?php echo json_encode($Orders); ?>;
        </script>
        <?php
    } else {
        $QueuedOrders = false;
    }
    
    if ($QueuedOrders != false) {
    ?>    
    <table class="table table-striped  order-queue">
        <tr>
            <th>
                &nbsp;
            </th>
            <th>
                Line Order
            </th>
            <th>
                Customer
            </th>
            <th>
                Order Placed
            </th>
            <th>
               # of Items
            </th>
            <th align="center">
                Est Total
            </th>
            <th>
               View
                
            </th>
            <th>
                 Review/Alter 
                
            </th>
            <th>
                Weigh In
            </th>
            <th>
                Check Out
            </th>
            <th>
                
            </th>
        </tr>
        <tbody>
            <?php
            $Counter = 1;
            
            foreach($QueuedOrders as $q) { 
            ?>
            <tr id="row-<?php echo $q->id; ?>">
            <td>
                <div class="vertical-align">
                <?php echo $Counter; ?>
                </div>
            </td>
            <?php
            if ($Counter == 1) {
                $FirstAlign = 'align="right"';
            } else {
                $FirstAlign = "";
            }
            ?>
            <td <?php echo $FirstAlign; ?>>
            <div class="vertical-align">
               <?php
               if ($Counter > 1) {
               ?>
                <button type="button" class="btn btn-default order-queue-button" aria-label="Left Align" current-position="<?php echo $Counter; ?>" order-id="<?php echo $q->id;?>" direction="-1" >
                    <span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                </button>              
               <?php
               }
               ?>
               <?php
               if ($Counter < count($QueuedOrders) ) {
               ?>
                <button type="button" class="btn btn-default order-queue-button" aria-label="Left Align" current-position="<?php echo $Counter; ?>" order-id="<?php echo $q->id;?>" direction="1">
                    <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
                </button>
               <?php
               }
               ?>
            </div>
            </td>
            <td>
                <div class="vertical-align">
                <?php echo $q->name; ?>
                </div>
            </td>
            <td>
                <div class="vertical-align">
                <?php echo date("D M d/y @ g:ia", $o->created); ?>
                </div>
            </td>
            <td align="center">
                <div class="vertical-align">
                <?php echo $q->numberitems; ?>
                </div>
            </td>
            <td align="right">
                <div class="vertical-align">
                <?php echo om_money($q->ordertotal); ?>
                </div>
            </td>
            <td align="center">
                <a href="/admin/order_management/vieworder/?o=<?php echo $q->id; ?>&di=<?php echo $DeliveryItemID; ?>&d=<?php echo  $DeliveryID; ?>" class="btn btn-primary">View Order</a>
            </td>
            <td align="center">
                 <a href="/admin/order_management/editorder/?o=<?php echo $q->id; ?>&di=<?php echo $DeliveryItemID; ?>&d=<?php echo $DeliveryID; ?>" class="btn btn-primary">Edit Order</a>
            </td>
            <td align="center">
                <a href="/admin/order_management/weighin/?o=<?php echo $q->id; ?>&d=<?php echo $DeliveryItemID; ?>" class="btn btn-primary">Weigh In</a>
            </td>
            <td align="center">
                <a href="/admin/order_management/checkout/?o=<?php echo $q->id; ?>&d=<?php echo $DeliveryItemID; ?>" class="btn btn-success">Check Out</a>
            </td>
            <td align="center">
                <a href="javascript:void(null);" order-id="<?php echo $q->id; ?>" class="btn btn-danger delete-order"  customer="<?php echo $q->name; ?>" amount="<?php echo $q->ordertotal; ?>">Delete</a>
            </td>            
        </tr>
            <?php
            $Counter++;
            }
            ?>
        </tbody>
    </table>
    <?php
    } else {
    ?>
    <div>
        No Queued Orders to display.
    </div>
    <?php
    }
    ?>
    </div>
    <div class="col-md-12">
        <h3>Open Orders</h3>
        <?php
        
        $OpenOrders = om_order_queue_get($DeliveryItemID, "open");
        
        if ($OpenOrders["StatusCode"] == 1) {
            $OpenOrders = $OpenOrders["Data"];
        } else {
            $OpenOrders = false;
        }
        
        if ($OpenOrders != false) {
        ?>
        <table class="table table-striped order-queue">
              <tr>
                <th>
                    Last Name
                </th>
                <th>
                    First Name
                </th>
                <th>
                    Phone
                </th>
                <th>
                    Email
                </th>
                <th>
                    Order Placed
                </th>
                <th>
                    # of Items
                </th>
                <th>
                    Est Total
                </th>
                <th>
                    View
                </th>
                <th>
                   Review/Alter
                </th>
                <th>
                    Queue
                </th>
                <th>
                    Admin
                </th>
            </tr>
            <tbody>
                <?php
                foreach($OpenOrders as $o) {
                ?>
                <tr id="row-<?php echo $o->id; ?>">
                    <td>
                        <?php echo $o->lastname; ?>
                    </td>
                    <td>
                        <?php echo $o->firstname; ?>
                    </td>
                    <td>
                        <?php echo $o->phone; ?>
                    </td>
                    <td>
                        <?php echo $o->email; ?>
                    </td>                    
                    <td>
                        <?php echo date("D M d/y @ g:ia", $o->created); ?>
                    </td>
                    <td align="center">
                        <?php echo $o->numberitems; ?>
                    </td>
                    <td align="right">
                        <?php echo om_money($o->ordertotal); ?>
                    </td>
                    <td>
                        <a href="/admin/order_management/vieworder/?o=<?php echo $o->id; ?>&di=<?php echo $DeliveryItemID; ?>&d=<?php echo  $DeliveryID; ?>" class="btn btn-primary">View Order</a>
                    </td>
                    <td align="center">
                        <a href="/admin/order_management/editorder/?o=<?php echo $o->id; ?>&di=<?php echo $DeliveryItemID; ?>&d=<?php echo $DeliveryID; ?>" class="btn btn-primary">Edit Order</a>
                    </td>
                    <td align="center">
                        <a href="javascript:void(null);" class="btn btn-success add-to-queue" orderid="<?php echo $o->id; ?>" delivery-id="<?Php echo $DeliveryItemID; ?>" >Add To Queue</a>
                    </td>
                    <td align="center">
                        <a href="javascript:void(null);" customer="<?php echo $o->name; ?>" amount="<?php echo $o->ordertotal; ?>" order-id="<?php echo $o->id; ?>" class="btn btn-danger delete-order" >Delete</a>
                    </td>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        <?php
        } else {
        ?>
        <div>
            No open orders were found.
        </div>    
        <?php
        }
        ?>
    </div>
    <div class="col-md-12">
        <h3>Completed Orders</h3>
        <?php
        
        $CompletedOrders = om_order_queue_get($DeliveryItemID, "cmpt");
        
        if ($CompletedOrders["StatusCode"] == 1) {
            $CompletedOrders = $CompletedOrders["Data"];
        } else {
            $CompletedOrders = false;
        }
        
        if ($CompletedOrders != false) {
        ?>
        <table class="table table-striped order-queue">
            <tr>
                <th>
                    Customer Name
                </th>
                <th>
                    Order Placed
                </th>
                <th>
                    Order Completed
                </th>
                <th>
                    # of 
                    Items
                </th>
                <th>
                    Order Total
                </th>
                <th>
                   Admin
                </th>
            </tr>
            <tbody>
                <?php
                foreach($CompletedOrders as $o) {
                ?>
                <tr>
                    <td>
                        <?php echo $o->name; ?>
                    </td>
                    <td>
                        <?php echo date("D M d/y @ g:ia", $o->created); ?>
                    </td>
                    <td>
                        <?php echo date("D M d/y @ g:ia", $o->completed); ?>
                    </td>
                    <td align="center">
                        <?php echo $o->numberitems; ?>
                    </td>
                    <td align="right">
                        <?php echo om_money($o->totalweighed); ?>
                    </td>
                    <td align="center">
                        <a href="/admin/order_management/vieworder/?o=<?php echo $o->id; ?>&di=<?php echo $DeliveryItemID; ?>&d=<?php echo  $DeliveryID; ?>" class="btn btn-primary">View Order</a>
                    </td>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        <?php
        } else {
        ?>
        <div>
            No completed orders were found.
        </div>    
        <?php
        }
        ?>        
    
</div>
<!-- Modal -->
<div class="modal fade" id="delete-order-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirm Order Deletion</h4>
      </div>
      <div class="modal-body">
        Do you really want to delete <span id="delete-name"></span>'s order for $<span id="delete-amount"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="confirm-delete-button" order-id="">Confirm Delete</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  jQuery(function() {
    jQuery(".vertical-align").verticalAlign();
    });
</script>