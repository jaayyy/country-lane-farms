<?php
date_popup_add();
$FormData = $_REQUEST["f"];
$StartDate = $FormData["start"];
$EndDate = $FormData["end"];

if (strlen($StartDate) > 0 && strlen($EndDate) > 0) {
    $StartDate = date("Y-m-d", strtotime($StartDate));
    $EndDate = date("Y-m-d", strtotime($EndDate));
    $Report = om_get_people_report($StartDate, $EndDate);
    if ($Report["StatusCode"] == 1) {
       $Report = $Report["Data"];
    } else {
        $Report = false;
    }
} else {
    $Report = false;
    $StartDate = date("Y-m-d", strtotime("-1 Month"));
    $EndDate = date("Y-m-d");
}
?>
<script type="text/javascript">
(function ($) {
    $(document).ready(function() {
        $("#start-date").datepicker();
        $("#end-date").datepicker();
    });
})(jQuery);    
</script>
<form action="" method="get">
<div>
<table class="table">
    <tr>
        <td align="right">
            Start Date:
        </td>
    <td>
    <input type="text" name="f[start]" value="<?php echo $StartDate; ?>" id="start-date"/>
    </td>
    <td align="right">
        End Date:
    </td>
    <td>
        <input type="text" name="f[end]" value="<?php echo $EndDate; ?>" id="end-date"/>
    </td>
    <?php
    $Selected[$Status] = " selected='selected' ";
    ?>

    </tr>
    <tr>
        <td colspan="5" align="right">
            <?php
            if ($Report != false) {
                $QueryData = base64_encode(json_encode($FormData));
            ?>
            <a href="/admin/order_management/export/?a=people&d=<?php echo $QueryData; ?>" class="btn btn-primary">Export to Excel</a>
            <?php
            }
            ?>
            <input type="submit" value="Generate Report"  class="btn btn-success"/>
        </td>
    </tr>
</table>
</div>
</form>

<?php
if ($Report != false) {
?>
<table class="table table-striped">
    <tr>
        <th>
           ID Number
        </th>		
        <th>
            Name
        </th>
        <th>
            Address
        </th>

        <th>
            Username/Email
        </th>
        <th>
            Phone Number
        </th>
        <th>
            Pickup Location
        </th>
        <th>
            Receive Newsletter
        </th>
        <th>
            Account Created
        </th>
        <th>
            Member For
        </th>
        <th>
           Last Order
		</th>
        <th>
            Last Access
        </th>
    </tr>
    <tbody>
        <?php
        foreach($Report as $r) {
			
        ?>
    <tr>
        <td>
            <?php echo $r->userid ?>
        </td>
        <td>
            <?php echo $r->name; ?>
        </td>
        <td nowrap>
            <?php echo $r->address;  ?>
        </td>
        <td>
            <?php echo $r->username; ?>
        </td>
        <td>
            <?php echo $r->phone; ?>
        </td>
        <td>
            <?php echo $r->pickuplocation; ?>
        </td>
        <td>
            <?php echo $r->newsletter; ?>
        </td>
        <td>
            <?php echo $r->accountcreated   ?>
        </td>
        <td>
            <?php echo om_member_since_format($r->lifetime); ?>
        </td>
        <td>
            <?php echo $r->lastorder; ?>
        </td>
        <td>
            <?php echo $r->lastaccess; ?>
        </td>
    </tr>
    <?php
        }
        ?>
    <tr>
        <td>
            <strong>--</strong>
        </td>
        <td>
            <strong>Total</strong>
        </td>
        <td>
            <strong><?php echo $RegQuantity; ?></strong>
        </td>
        <td>
            <strong><?php echo $BulkQuantity; ?></strong>
        </td>
        <td>
            <strong><?php echo $RegQuantity + $BulkQuantity; ?></strong>
        </td>        
        <td>
            <strong><?php echo om_money($TotalReg); ?></strong>
        </td>        
        <td>
            <strong><?php echo om_money($TotalRegWeighed); ?></strong>
        </td>
        <td>
            <strong><?php echo om_money($TotalBulk); ?></strong>
        </td>
        <td>
            <strong><?php echo om_money($TotalBulkWeighed); ?></strong>
        </td>
        <td>
            <strong><?php echo om_money($TotalBulk + $TotalReg); ?></strong>
        </td>
        <td>
            <strong><?php echo om_money($TotalBulkWeighed + $TotalRegWeighed); ?></strong>
        </td>
    </tr>            
    </tbody>
</table>
<?php
} else if ($Report == false && strlen($StartDate) > 0) {
            
    ?>
<p>
    No data retrieved for the selected period.
</p>
    </tr>
    <?php
        }
    ?>