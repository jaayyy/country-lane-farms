<?php

$CategoryID = intval($_REQUEST["c"]);
if ($CategoryID > 0) {
    $Category = om_product_categories_get($CategoryID);
    if ($Category["StatusCode"] == 1) {
        $Category = $Category["Data"][0];
        $Products = om_get_products_category_admin($CategoryID);
        if ($Products["StatusCode"] == 1) {
            $Products = $Products["Data"];
        } else {
            $Products = false;
        }
    }
}

$Dropdown = om_product_categories_dd();

if ($Dropdown["StatusCode"] == 1) {
    $Dropdown = $Dropdown["Data"];
} else {
    $Dropdown = false;
}

$DDSelected[$Category->parentid] = "selected='selected'";
if ($CategoryID > 0) {
    $PageAction = "Edit";
} else {
    $PageAction = "Create";
}
?>
<h1 class="page-header"><?php echo $PageAction; ?> Product Category</h1>
<form action="/admin/order_management/omprocessing" method="post" id="product-category-form" class="form-horizontal">
    <input type="hidden" name="a" value="save-product-category" />
    <input type="hidden" name="f[id]" value="<?php echo $CategoryID; ?>" />
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Category Name</label>
        <div class="col-sm-10" >
          <input type="text" class="form-control" id="name" name="f[name]" value="<?php echo $Category->name; ?>">
        </div>
    </div>
        <div class="form-group">
        <label for="parent" class="col-sm-2 control-label">Parent Category</label>
        <div class="col-sm-10" >
          <select class="form-control" id="parentid" name="f[parentid]">
            <option value="0">Top Level</option>
            <?php
            foreach($Dropdown as $d) {
            ?>
            <option value="<?php echo $d->id; ?>" <?php echo $DDSelected[$d->id]; ?>><?php echo $d->name; ?></option>
            <?php
            }
            ?>
          </select>
        </div>        
    </div>        
    
        <div class="form-group">
        <label for="summary" class="col-sm-2 control-label">Summary</label>
        <div class="col-sm-10" >
          <textarea class="form-control" id="description" name="f[summary]" rows="2"><?php echo $Category->summary; ?></textarea>
        </div>        
    </div>
    <div class="form-group">
        <label for="description" class="col-sm-2 control-label">Description</label>
        <div class="col-sm-10" >
          <textarea class="form-control" id="description" name="f[description]" rows="10"><?php echo $Category->description; ?></textarea>
        </div>        
    </div>
    <div class="form-group">
        <label for="description" class="col-sm-2 control-label">Alert</label>
        <div class="col-sm-10" >
          <textarea class="form-control" id="alert" name="f[alert]" rows="3"><?php echo $Category->alert; ?></textarea>
        </div>        
    </div>
    <?php
    if ($CategoryID > 0) {
    ?>
    <fieldset id="included-products">
        <legend>Included Products</legend>
        <?php
        if ($Products !== false) {
            $ListLimit = ceil(count($Products) / 4);
            $Counter = 1;
            foreach($Products as $p) {
            if ($Counter == 1) {
            ?>
            <ul class="associated-product-list">
            <?
            }
            
            ?>
                <li><?php echo $p->name; ?></li>
            <?php
            if ($Counter == 4) {
            ?>
             </ul>
            <?php
            $Counter = 0;
            }
            $Counter++;
            ?>
              
        <?php
            }
        } else {
            echo "No products added yet...";
        }
        ?>
    </fieldset>
    <?php
    }
    ?>
    <div class="form-buttons">
        <a href="javascript:window.history.back();" class="btn btn-warning">Cancel</a>&nbsp;&nbsp;&nbsp;<input type="submit" value="Save" class="btn btn-success" />
    </div>    
</form>
?>