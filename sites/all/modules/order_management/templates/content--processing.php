<?php
$Action = $_REQUEST["a"];

switch($Action) {
   case "save-order":
        $DeliveryID = intval($_REQUEST["d"]);
        $Products = $_REQUEST["quantity"];
        $Prices = $_REQUEST["cost"];
        $PriceModel = $_REQUEST["pricemodel"];
        $PerPound = $_REQUEST["lb"];
        $OrderID = intval($_REQUEST["o"]);
        $DeliveryID = intval($_REQUEST["d"]);
        $OrderTotal = 0;

        om_clear_order_items($OrderID);

        foreach($Products as $k=>$v) {
            if ($v > 0) {

                $Item = array(
                                "orderid"     =>  $OrderID,

                                "productid"   =>  $k,
                                "quantity"    =>  $v,
                                "unitcost"    =>  $Prices[$k],
                                "total"       =>  ($Prices[$k] * $v),
                                "pricemodel"  =>  $PriceModel[$k],
                                "pricelb"     =>  $PerPound[$k]
                              );

                $OrderTotal += ($Prices[$k] * $v);
                $Result = om_order_item_save($Item);


            }
        }

        $Order = array(
                       "id" =>  $OrderID,
                       "deliveryid"  =>  $DeliveryID,
                       "total"  =>  $OrderTotal,
                       "status" =>  'ready'
                       );

        $result  = om_order_save($Order);
            $Parameters = array("d"=>$DeliveryID);
            drupal_goto("/admin/order_management/orderqueue/", array("query"=>$Parameters));
    break;

    case "checkout":

      $OrderTotal = 0;
        $OrderID = intval($_REQUEST["o"]);
        $DeliveryID = intval($_REQUEST["d"]);
        $ItemWeights = $_REQUEST["w"];
        $ItemPrices = $_REQUEST["p"];
        $ItemPricesBulk = $_REQUEST["b"];
        $ItemQuantities = $_REQUEST["quantity"];
        $FixedTotal = floatval($_REQUEST["ft"]);
        $IDs = $_REQUEST["i"];

        $Pricing = array();

        foreach($ItemPrices AS $k=>$v) {
            $Data = explode(":", $v);
            $Prices = array(
                            "bulkmin"       =>  intval($Data[0]),
                            "pricelb"       =>  floatval($Data[1]),
                            "bulkpricelb"   =>  floatval($Data[2])
                            );
            $Pricing[$k] = $Prices;
        }

        $PricingBulk = array();

        foreach($ItemPricesBulk AS $k=>$v) {
            $Data = explode(":", $v);
            $Prices = array(
                            "bulkmin"       =>  intval($Data[0]),
                            "price"       =>  floatval($Data[1]),
                            "bulkprice"   =>  floatval($Data[2])
                            );
            $PricingBulk[$k] = $Prices;
        }

        foreach($ItemQuantities as $k=>$v) {
            if ($IDs[$k] > 0 || $IDs[$k] == "NEW") {
                $Quantity = $v;
                $Weight = floatval($ItemWeights[$k]);
                $BulkMin = $Pricing[$k]["bulkmin"];
                $UnitCost = 0;

                if ($BulkMin > 1 && $Quantity >= $BulkMin) {
                    $Price = floatval($Pricing[$k]["bulkpricelb"]);
                    $PriceModel = "bulk";
                } else {
                    $Price = floatval($Pricing[$k]["pricelb"]);
                    $PriceModel = "regular";
                }
echo floatval($Pricing[$k]["pricelb"]);
                if ($Price == 0) {

                     $BulkMin = $ItemPricesBulk[$k]["bulkmin"];

                     if ($BulkMin > 1 && $Quantity >= $BulkMin) {
                        $PriceModel = "bulk";
                     } else {
                        $PriceModel = "regular";
                     }

                     if ($PriceModel == "regular") {
                        $UnitCost = $PricingBulk[$k]["price"];
                     } else {
                        $UnitCost = $PricingBulk[$k]["pricebulk"];
                     }
                     $Cost = 0;
                     $ItemTotal = $UnitCost * $Quantity;
                     $OrderTotal += $ItemTotal;
                } else {
                     $Cost = $Price * $Weight;
                     $OrderTotal += $Cost;
                }



                $Request = array(
                                "id"            =>  intval($IDs[$k]),
                                "orderid"       =>  $OrderID,
                                "productid"     => $k,
                                "quantity"      =>  $Quantity,
                                "pricelb"       =>  $Price,
                                "weight"        =>  $Weight,
                                "weighedtotal"  =>  $Cost,
                                "pricemodel"    =>  $PriceModel
                                );

                  if ($UnitCost > 0) {
                     $Request["unitcost"] = $UnitCost;
                     $Request["total"] = $ItemTotal;
                  }

                $Result = om_order_item_save($Request);

            } else {

            }
        }


        $Order = array(
                       "id"                 =>  $OrderID,
                       "totalweighed"       =>  $OrderTotal
                       );

        $Result = om_order_save($Order);

        $Parameters = array("d"=>$DeliveryID, "o"=>$OrderID);
        drupal_goto("/admin/order_management/orderreceipt/", array("query"=>$Parameters));
    break;

   case "order-complete":
      $OrderID = intval($_REQUEST["o"]);
      $DetailsID = intval($_REQUEST["d"]);
      $Order = array(
                     "id"                 =>    $OrderID,
                     "paymentreceived"    =>    time(),
                     "status"             =>    "cmpt"
                     );

      $Result = om_order_save($Order);
      om_send_order_receipt($OrderID);
      $Parameters = array("d"=>$DetailsID);
      drupal_goto("/admin/order_management/orderqueue/", array("query"=>$Parameters));
      break;

case "save-product-category":
    $CategoryData = $_REQUEST["f"];
    $Result = om_product_category_save($CategoryData);

    drupal_goto("/admin/order_management/productcategorylist");
    break;


case "save-delivery-date":
    $DeliveryData = $_REQUEST["f"];

    $DeliveryDate = date("Y-m-d", strtotime($DeliveryData["deliverydate"]));
    $Delivery = array(
                    "id"                => $DeliveryData["id"],
                    "deliverydate"      => $DeliveryDate,
                    "deliverytypeid"    => $DeliveryData["deliverytypeid"]
                    );
    $Result = om_delivery_save($Delivery);

    if  ($Result["StatusCode"] == 1) {

        $DeliveryID = intval($DeliveryData["id"]) > 0 ? $DeliveryData["id"] : $Result["Data"];
        om_clear_product_availability($DeliveryID);

        $ProductsAvailable = om_get_available_products($DeliveryData["deliverytypeid"]);

        if ($ProductsAvailable["StatusCode"] == 1) {
            $ProductsAvailable = $ProductsAvailable["Data"];

            foreach($ProductsAvailable as $p) {
                $Request = array(
                                 "productid"    =>  $p->id,
                                 "deliveryid"   =>  $DeliveryID
                                 );

                om_product_availability_save($Request);
            }
        }

        om_clear_delivery_details($DeliveryID);
        $LocationData = $_REQUEST["location"];
        $TimeData = $_REQUEST["times"];

        $DeliveryDetails  = array();

        if (count($LocationData) > 0) {
            foreach($LocationData as $k=>$v) {
                $Detail = array(
                    "id"            =>  intval($v),
                    "deliveryid"    =>  $DeliveryID,
                    "locationid"    =>  $k,
                    "start"         =>  $DeliveryDate." ".$TimeData[$k]["start"].$TimeData[$k]["startap"],
                    "end"           =>  $DeliveryDate." ".$TimeData[$k]["end"].$TimeData[$k]["endap"],
                    "status"        => 'actv'
                );
                $Result = om_delivery_detail_save($Detail);
            }
        }

    }


    drupal_goto("/admin/order_management/deliverydatelist");
    break;

    case "save-product":
         $Product = $_REQUEST["f"];
		$Product["largeimage_inputname"] = $_REQUEST["largeimage_inputname"];
		$Product["thumbnail_inputname"] = $_REQUEST["thumbnail_inputname"];
         $Product["visible"] = intval($Product["visible"]);
         $Product["available"] = intval($Product["available"]);
         $Product["status"] = "actv";
         $Result = om_product_save($Product);

         if (intval($Product["id"]) == 0) {
             $Deliveries = om_get_pending_deliveries();
             if ($Deliveries["StatusCode"] == 1) {
                 $Deliveries = $Deliveries["Data"];
                 foreach ($Deliveries as $d) {
                     $Request = array(
                                      "productid"    =>  $Product["pid"],
                                      "deliveryid"   =>  $d->deliveryid
                                      );
                     om_product_availability_save($Request);
                 }
             }
         }
         drupal_goto("/admin/order_management/productlist");
         break;

    case "weigh-in":

        $OrderTotal = 0;
        $OrderID = intval($_REQUEST["o"]);
        $DeliveryID = intval($_REQUEST["d"]);
        $ItemWeights = $_REQUEST["w"];
        $ItemPrices = $_REQUEST["p"];
        $ItemQuantities = $_REQUEST["quantity"];
        $PriceLB = $_REQUEST["lb"];
        $FixedTotal = floatval($_REQUEST["ft"]);
        $Costs = $_REQUEST["cost"];
        $Pricing = array();

        foreach($ItemPrices AS $k=>$v) {
            $Data = explode(":", $v);
            $Prices = array(
                            "bulkmin"       =>  intval($Data[0]),
                            "pricelb"       =>  floatval($Data[1]),
                            "bulkpricelb"   =>  floatval($Data[2])
                            );
            $Pricing[$k] = $Prices;
        }

        foreach($ItemWeights as $k=>$v) {
            $Quantity = $ItemQuantities[$k];
            $Weight = $ItemWeights[$k];
            $BulkMin = $Pricing[$k]["bulkmin"];
            $Price = $PriceLB[$k];
            if ($BulkMin > 1 && $Quantity >= $BulkMin) {

                $PriceModel = "bulk";
            } else {
                $PriceModel = "regular";
            }

            $Cost = $Price * $Weight;
            $OrderTotal += $Cost;

            $Request = array(
                            "id"            =>  $k,
                            "orderid"       =>  $OrderID,
                            "quantity"      =>  $Quantity,
                            "unitcost"      =>  $Costs[$k],
                            "total"         =>  $Costs[$k] * $Quantity,
                            "pricelb"       =>  $Price,
                            "weight"        =>  $Weight,
                            "weighedtotal"  =>  $Cost,
                            "pricemodel"    =>  $PriceModel
                            );

            $Result = om_order_item_save($Request);
        }
        $OrderTotal += $FixedTotal;

        $Order = array(
                       "id"     =>  $OrderID,
                       "totalweighed"  =>  $OrderTotal
                       );

        $Result = om_order_save($Order);

        $Parameters = array("d"=>$DeliveryID);
        drupal_goto("/admin/order_management/orderqueue/", array("query"=>$Parameters));
    break;

    case "delete-order";
        $Order = array(
                       "id"     =>  intval($_REQUEST["o"]),
                       "status" =>  "cncl"
                       );

        $Result = om_order_save($Order);
    echo intval($_REQUEST["o"]);
    exit;
    break;

        case "reorder-queue":
            $OrderID = intval($_REQUEST["o"]);
            $CurrentPosition = intval($_REQUEST["c"]) - 1;
            $Direction = intval($_REQUEST["d"]);
            $CurrentQueue = json_decode(urldecode($_REQUEST["oq"]));

            $NewPosition = $CurrentPosition + $Direction;
            $Temp = $CurrentQueue[$NewPosition];
            $CurrentQueue[$NewPosition] = $CurrentQueue[$CurrentPosition];
            $CurrentQueue[$CurrentPosition] = $Temp;

            $Position = 1;
            foreach($CurrentQueue as $q) {
                $Request = array(
                                 "id"   =>  $q,
                                 "queueposition"    => $Position
                                 );
                $Result = om_order_save($Request);

                $Position++;
            }
            die();
            break;
        case "delete-delivery":
            $DeliveryID = intval($_REQUEST["d"]);

            $Request = array(
                             "id"   => $DeliveryID,
                             "status"   =>  "nact"
                             );

            $Result = om_delivery_save($Request);
            die();
            break;


    case "close-delivery":
        $DeliveryID  = intval($_REQUEST["d"]);
        $Emails = $_REQUEST["e"];
        if (count($Emails) > 0) {
            foreach($Emails as $k=>$v) {
                if ($v == 1) {
                    $Ret = om_send_missed_delivery_email($k);
                }
            }
        }

        $Result = om_close_delivery($DeliveryID);
        $Parameters = array("d"=>$DeliveryID);
        drupal_goto("/admin/order_management/orderqueue/", array("query"=>$Parameters));
        break;

    default:
        $Deliveries = $_REQUEST["delivery"];
        $Alerts = $_REQUEST["alert"];

        foreach($Alerts as $k=>$v) {

                $Alert = array(
                               "id"  =>  $k,
                               "alert"      =>  $v
                               );

                om_product_save($Alert);
        }

        if (count($Deliveries) > 0) {
            foreach($Deliveries as $did=>$d) {
                om_clear_product_availability($did);

                foreach($d as $pid=>$p) {
                    $Request = array (
                                    "productid" =>  $pid  ,
                                    "deliveryid"    =>  $did
                    );

                    om_product_availability_save($Request);
                }
            }
        }
        drupal_goto("/admin/order_management/productavailability");
        break;


}
?>