<?php

global $base_url;
drupal_add_js("var BASE_URL = '".$base_url."/sites';", "inline");
drupal_add_js($base_url."/sites/all/libraries/simpleajaxupload/SimpleAjaxUploader.min.js");
drupal_add_js($base_url."/".drupal_get_path('module', 'order_management')."/js/jquery.product.js");
$ProductID = intval($_REQUEST["p"]);

$Product = om_get_product_by_id($ProductID);

if ($Product["StatusCode"] == 1) {
    $Product = $Product["Data"];
} else {
    $Product = false;
}

$Dropdown = om_product_categories_dd();

if ($Dropdown["StatusCode"] == 1) {
    $Dropdown = $Dropdown["Data"];
} else {
    $Dropdown = false;
}

$DDSelected[$Product->categoryid] = "selected='selected'";
if ($ProductID > 0) {
    $PageAction = "Edit";
} else {
    $PageAction = "Create";
}
?>
<h1 class="page-header"><?php echo $PageAction; ?> Product</h1>
<style type="text/css">
    .form-horizontal .price-group .form-group label {
        white-space: nowrap;
        font-weight: normal;
    }
    .form-horizontal {
        position: relative;
    }

</style>
<form action="/admin/order_management/omprocessing" method="post" class="form-horizontal" enctype="multipart/form-data">
    <input type="hidden" name="a" value="save-product" />
    <input type="hidden" name="f[id]" value="<?php echo $ProductID; ?>" />
    <div class="form-group">
        <?php
        if ($Product->visible == 1) {
            $ShowChecked = " checked ";
        } else {
            $ShowChecked = "";
        }
        ?>
        <label for="visible" class="col-sm-2 control-label">Show On Site</label>
        <div class="col-sm-10" >
          <input type="checkbox" class="form-control" id="visible" name="f[visible]"  value="1" style="width: auto;" <?php echo $ShowChecked; ?> />
        </div>
    </div>
    <div class="form-group">
        <label for="product-id" class="col-sm-2 control-label">Product ID</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="product-id" name="f[pid]" value="<?php echo $Product->pid; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="product-name" class="col-sm-2 control-label">Product Name</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="product-name" name="f[name]" value="<?php echo $Product->name; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="summary" class="col-sm-2 control-label">Summary</label>
        <div class="col-sm-10">
          <textarea class="form-control" id="product-name" name="f[summary]" rows="3"><?php echo $Product->summary; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="col-sm-2 control-label">Description</label>
        <div class="col-sm-10">
          <textarea class="form-control" id="description" name="f[description]" rows="8"><?php echo $Product->description; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="category" class="col-sm-2 control-label">Category</label>
        <div class="col-sm-10">
          <select class="form-control" id="category" name="f[categoryid]">
            <option value="">Select Category</option>
            <?php
            foreach($Dropdown as $d) {
            ?>
            <option value="<?php echo $d->id; ?>" <?php echo $DDSelected[$d->id]; ?>><?php echo $d->name; ?></option>
            <?php
            }
            ?>
          </select>
        </div>
    </div>
    <div class="form-group">
        <?php
        if ($Product->available == 1) {
            $AvailableChecked = " checked ";
        } else {
            $AvailableChecked = "";
        }
        ?>
        <label for="available" class="col-sm-2 control-label">Available</label>
        <div class="col-sm-10">
          <input type="checkbox" class="form-control" id="available" name="f[available]" value="1"  style="width: auto;" <?php echo $AvailableChecked; ?>/>
        </div>
    </div>
    <div class="form-group">
        <label for="alert" class="col-sm-2 control-label">Alert</label>
        <div class="col-sm-10">
          <textarea class="form-control" id="alert" name="f[alert]" rows="2"><?php echo $Product->alert; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="main-image" class="col-sm-2 control-label">Main Image</label>
        <div class="col-sm-10">
            <?php
            if (strlen($Product->image) == 0) {
                $MainImageBG = "http://placehold.it/715x705";
            } else {
                $MainImageBG = "/sites/default/files/products/large/".$Product->image;
            }
            ?>
            <style type="text/css">
                #largeimageupload {
                    background: url(<?php echo $MainImageBG; ?>);
                    border:5px solid #EAEAEA;
                    text-align: center;
                    width: 725px;
                    height: 715px;
                    line-height: 715px;
                    cursor: pointer;
                }
            </style>
            <div id="largeimageupload" title="Click Me To Change Me!"></div>
            <input type="hidden" id="largeimage_input" name="f[image]" value="" />
            <input type="hidden" id="largeimage_inputname" name="largeimage_inputname" value="" />
            <div id="progressouter" class="progress progress-striped active" style="display:none;">
                <div id="progressbarlg" class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
            <div id="progressOuter" class="progress progress-striped active" style="display:none;">
            </div>
            <div id="msg-box"></div>
        </div>
    </div>

    <div class="form-group">
        <label for="thumbnail" class="col-sm-2 control-label">Thumbnail</label>
        <div class="col-sm-10">
            <?php
            if (strlen($Product->thumbnail) == 0) {
                $MainImageBG = "http://placehold.it/215x215";
            } else {
                $MainImageBG = "/sites/default/files/products/small/".$Product->thumbnail;
            }
            ?>
            <style type="text/css">
                #thumbnailupload {
                    background: url(<?php echo $MainImageBG; ?>);
                    border:5px solid #EAEAEA;
                    text-align: center;
                    width: 215px;
                    height: 215px;
                    line-height: 215px;
                    cursor: pointer;
                }
            </style>
            <div id="thumbnailupload" title="Click Me To Change Me!"></div>
            <input type="hidden" id="thumbnail_input" name="f[thumbnail]" value="" />
            <input type="hidden" id="thumbnail_inputname" name="thumbnail_inputname" value="" />
            <div id="thumbnailprogressouter" class="progress progress-striped active" style="display:none;">
                <div id="thumbnailprogressbarlg" class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
            <div id="progressOuter" class="progress progress-striped active" style="display:none;">
            </div>
            <div id="thumbnail-msg-box"></div>
        </div>
    </div>
</div>
    </div>
<!--
    <div class="form-group">
        <label for="thumbnail" class="col-sm-2 control-label">Thumbnail</label>
        <div class="col-sm-10">
            <?php
            if (strlen($Product->thumbnail) == 0) {
            ?>
            <img src="http://placehold.it/215x215">
            <?php
            } else {
            ?>
            <img src="/sites/default/files/products/small/<?php echo $Product->thumbnail; ?>">
            <?php
            }
            ?>
            <input type="file" name="f[thumbnailimage]" id="thumbnail-uploader" />
            <input type="hidden" name="thumbnail_inputname" id="thumbnail_inputname" />
        </div>
    </div>
    <div class="form-group">
        <label for="additional-image" class="col-sm-2 control-label">Additional</label>
        <!--<div class="col-sm-10">
          <a href="javascrip:void(null);" class="btn btn-default">Add Image</a>
        </div>-->
    </div>
    <div>
        <div class="form-group">
            <label for="price-lb" class="col-sm-2  control-label">Price per lb.</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="price-lb" name="f[pricelb]" value="<?php echo number_format($Product->pricelb, 2, ".", ","); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="estimated-price-per-item"  class="col-sm-2  control-label">Estimated Cost Per Item</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="estimated-price-per-item" name="f[estimatedpriceperitem]" value="<?php echo number_format($Product->estimatedpriceperitem, 2, ".", ","); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="bulk-price-lb" class="col-sm-2  control-label">Bulk Price per lb.</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="bulk-price-lb" name="f[bulkpricelb]" value="<?php echo number_format($Product->bulkpricelb, 2, ".", ","); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="estimated-bulk-price-per-item" class="col-sm-2  control-label">Estimated Bulk Cost Per Item</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="estimated-bulk-price-per-item" name="f[estimatedbulkpriceperitem]" value="<?php echo number_format($Product->estimatedbulkpriceperitem, 2, ".", ","); ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="bulk-min" class="col-sm-2  control-label">Bulk Minimum Order Quality</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="bulk-min" name="f[bulkminquantity]" value="<?php echo $Product->bulkminquantity; ?>">
            </div>
        </div>
         <div class="form-group">
            <label for="url" class="col-sm-2  control-label">Page URL</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="url" name="f[url]" value="<?php echo $Product->url; ?>">
            </div>
        </div>
    </div>
    <div class="form-buttons">
        <a href="javascript:window.history.back();" class="btn btn-warning">Cancel</a>&nbsp;&nbsp;&nbsp;<input type="submit" value="Save" class="btn btn-success" />
    </div>
</form>

