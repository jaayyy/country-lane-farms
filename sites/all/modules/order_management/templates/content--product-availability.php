<?php
$ProductsAvailableData = om_product_availability_get();
$ProductsAvailable = Array();
if ($ProductsAvailableData["StatusCode"] == 1) {
    $ProductsAvailableData = $ProductsAvailableData["Data"];
    foreach($ProductsAvailableData as $p) {
        $ProductsAvailable[$p->productid][$p->deliveryid] = true;
    }
} else  {
    $ProductsAvailableData = false;
}

$Dates = om_get_delivery_dates();
$DateData = array();
if ($Dates["StatusCode"] == 1) {
    $Dates = $Dates["Data"];

} else {
    $Dates = false;
}

$Products = om_get_products();

if ($Products["StatusCode"] == 1) {
    $Products = $Products["Data"];
} else {
    $Products = false;
}
/*
$Alerts = om_get_availability_alerts();

if ($Alerts["StatusCode"] == 1) {
    $Alerts = $Alerts["Data"];
} else {
    $Alerts = false;
}
*/
//var_dump($Dates);
if ($ProductsAvailableData == false) {
    ?>
    <div class="alert alert-info">
        Product Availability is not available. :(
    </div>
<?php
} else {
?>
<form action="/admin/order_management/omprocessing" method="post">
<div class="form-buttons">
    <a href="javascript:window.history.back();" class="btn btn-warning">Cancel</a>&nbsp;&nbsp;&nbsp;<input type="submit" value="Save" class="btn btn-success" />
</div>
<br />
<table class="table table-striped">
    <tbody>
        <tr>
            <th>
                Product
            </th>
            <th>
                Alert Text
            </th>
            <?php
            foreach($Dates as $d) {
            ?>
            <th class="date-header">
                <?php echo date("D M j", strtotime($d->deliverydate)); ?>
            </th>
            <?php
            }
            ?>
        </tr>
        <?php
        foreach ($Products as $p) {
           
        ?>
        <tr>
            <td  style="text-align: left;">
               <?php echo $p->name; ?>
            </td>
            <td>
                <input type="text" value="<?php echo $p->alert; ?>" name="alert[<?php echo $p->id; ?>]" class="form-control" />
            </td>
            <?php
            foreach($Dates as $d) {
            ?>
            <td>
                <?php
                if (isset($ProductsAvailable[$p->id][$d->id])) {
                    $checked = "checked";
                } else {
                    $checked = "";
                }
                ?>
                <input type="checkbox" name="delivery[<?php echo $d->id; ?>][<?php echo $p->id; ?>]" class="form-control" <?php echo $checked; ?> value="1" />
            </td>
            <?php
            }
            ?>
        </tr>        
        <?php
        }
        ?>
    </tbody>
</table>
</form>
<?php
}
?>