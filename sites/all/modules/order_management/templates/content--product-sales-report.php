<?php
date_popup_add();
$FormData = $_REQUEST["f"];
$StartDate = $FormData["start"];
$EndDate = $FormData["end"];
$Status = strlen($FormData["status"]) > 0 ? $FormData["status"] : "all";

if (strlen($StartDate) > 0 && strlen($EndDate) > 0) {
    $StartDate = date("Y-m-d", strtotime($StartDate));
    $EndDate = date("Y-m-d", strtotime($EndDate));
    $Report = om_get_product_sales_report($StartDate, $EndDate, $Status);
    if ($Report["StatusCode"] == 1) {
       $Report = $Report["Data"];
    } else {
        $Report = false;
    }
} else {
    $Report = false;
    $StartDate = date("Y-m-d", strtotime("-1 Month"));
    $EndDate = date("Y-m-d");
}
?>
<script type="text/javascript">
(function ($) {
    $(document).ready(function() {
        $("#start-date").datepicker();
        $("#end-date").datepicker();
    });
})(jQuery);    
</script>
<form action="" method="get">
<div>
<table class="table">
    <tr>
        <td align="right">
            Start Date:
        </td>
    <td>
    <input type="text" name="f[start]" value="<?php echo $StartDate; ?>" id="start-date"/>
    </td>
    <td align="right">
        End Date:
    </td>
    <td>
        <input type="text" name="f[end]" value="<?php echo $EndDate; ?>" id="end-date"/>
    </td>
    <?php
    $Selected[$Status] = " selected='selected' ";
    ?>
    <td>
        <select name="f[status]" >
            <option value="all" <?php echo $Selected["all"] ?>>All Orders</option>
            <option value="actv" <?php echo $Selected["actv"] ?>>Open Orders</option>
            <option value="cmpt" <?php echo $Selected["cmpt"] ?>>Completed Orders</option>
        </select>
    </td>
    </tr>
    <tr>
        <td colspan="5" align="right">
            <?php
            if ($Report != false) {
                $QueryData = base64_encode(json_encode($FormData));
            ?>
            <a href="/admin/order_management/export/?a=product-sales&d=<?php echo $QueryData; ?>" class="btn btn-primary">Export to Excel</a>
            <?php
            }
            ?>
            <input type="submit" value="Generate Report"  class="btn btn-success"/>
        </td>
    </tr>
</table>
</div>
</form>

<?php
if ($Report != false) {
?>
<table class="table table-striped">
    <tr>
        <th>
            Product ID
        </th>
        <th>
            Product
        </th>
        <th>
            Reg Price<br/>Quantity
        </th>
        <th>
            BK Price<br/>Quantity
        </th>
        <th>
            Total<br/>Quantity
        </th>
        <th>
            Reg Price<br/>Revenue Estimated
        </th>
        <th>
            Reg Price<br/>Revenue Weighed
        </th>
        <th>
            BK Price<br/>Revenue Estimated
        </th>
        <th>
            BK Price<br/>Revenue Weighed
        </th>
        <th>
            Total<br/>Revenue Estimated
        </th>
        <th>
            Total Revenue<br/>Weighed
        </th>
    </tr>
    <tbody>
        <?php
        $RegQuantity = 0;
        $BulkQuantity = 0;
        $TotalReg = 0;
        $TotalBulk = 0;
        $Total = 0;
        $TotalRegWeighed = 0;
        $TotalBulkWeighed = 0;
        $TotalWeighed = 0;
        foreach($Report as $r) {
        ?>
    <tr>
        <td>
            <?php echo $r->productid; ?>
        </td>
        <td>
            <?php echo $r->product; ?>
        </td>
        <td>
            <?php echo $r->regquantity;
            $RegQuantity += $r->regquantity;
            ?>
        </td>
        <td>
            <?php echo $r->bulkquantity;
            $BulkQuantity += $r->bulkquantity;
            ?>
        </td>
        <td>
            <?php echo $r->totalquantity; ?>
        </td>
        <td>
            <?php echo om_money($r->regsales);
            $TotalReg += $r->regsales;
            ?>
        </td>
        <td>
            <?php echo om_money($r->weighedregular);
            $TotalRegWeighed += $r->weighedregular;
            ?>
        </td>
        <td>
            <?php echo om_money($r->bulksales);
            $TotalBulk += $r->bulksales;
            ?>
        </td>
        <td>
            <?php echo om_money($r->weighedbulk);
            $TotalBulkWeighed += $r->weighedbulk;
            ?>
        </td>
        <td>
            <?php echo om_money($r->totalsales); ?>
        </td>
        <td>
            <?php echo om_money($r->weighedtotal); ?>
        </td>
    </tr>
    <?php
        }
        ?>
    <tr>
        <td>
            <strong>--</strong>
        </td>
        <td>
            <strong>Total</strong>
        </td>
        <td>
            <strong><?php echo $RegQuantity; ?></strong>
        </td>
        <td>
            <strong><?php echo $BulkQuantity; ?></strong>
        </td>
        <td>
            <strong><?php echo $RegQuantity + $BulkQuantity; ?></strong>
        </td>        
        <td>
            <strong><?php echo om_money($TotalReg); ?></strong>
        </td>        
        <td>
            <strong><?php echo om_money($TotalRegWeighed); ?></strong>
        </td>
        <td>
            <strong><?php echo om_money($TotalBulk); ?></strong>
        </td>
        <td>
            <strong><?php echo om_money($TotalBulkWeighed); ?></strong>
        </td>
        <td>
            <strong><?php echo om_money($TotalBulk + $TotalReg); ?></strong>
        </td>
        <td>
            <strong><?php echo om_money($TotalBulkWeighed + $TotalRegWeighed); ?></strong>
        </td>
    </tr>            
    </tbody>
</table>
<?php
} else if ($Report == false && strlen($StartDate) > 0) {
            
    ?>
<p>
    No data retreived for the selected period.
</p>
    </tr>
    <?php
        }
    ?>