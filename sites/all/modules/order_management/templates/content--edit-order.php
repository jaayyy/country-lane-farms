<?php
$DeliveryItemID = intval($_REQUEST["di"]);
$DeliveryID = $_REQUEST["d"];

drupal_add_js($base_url."/".drupal_get_path('module', 'order_management')."/js/jquery.order-list.js");
drupal_add_js($base_url."/".drupal_get_path('module', 'order_management')."/js/jquery.admin-edit-order.js");
drupal_add_js($base_url."/".drupal_get_path('module', 'order_management')."/js/jquery.vertical-align.js");
$OrderID = intval($_REQUEST["o"]);
$Categories = om_get_product_categories();

if ($Categories["StatusCode"] == 1) {
    $Categories = $Categories["Data"];
} else {
    $Categories = false;
}

$OrderDetails = om_get_delivery_info($OrderID);

if ($OrderDetails["StatusCode"] == 1) {
    $OrderDetails = $OrderDetails["Data"];
} else {
    $OrderDetails = false;
}

$OrderItems = om_order_items($OrderID);

$Items = array();

if ($OrderItems["StatusCode"] == 1) {
    $OrderItems = $OrderItems["Data"];
    
    foreach ($OrderItems as $i) {
        
        $PriceLB = $i->pricelb;
        $EstimatedPerItem = $i->unitcost;
        $Items[$i->productid] = array("quantity"=>$i->quantity, "total"=>$i->total, "pricelb"=>$PriceLB, "estimatedpriceperitem"=>$EstimatedPerItem, "pricemodel"=>$i->pricemodel);        
    }
} else {
    $OrderItems = false;
}

$LocationDropdown = om_delivery_location_dropdpown($OrderID);

if ($LocationDropdown["StatusCode"] == 1) {
    $LocationDropdown = $LocationDropdown["Data"];
} else {
    $LocationDropdown = false;
}
?>
<link rel="stylesheet" href="<?php echo $base_url."/".drupal_get_path('module', 'order_management')?>/css/style.css" type="text/css" media="screen" />
    <form action="/admin/order_management/omprocessing" method="post" id="complete-order-form">
        <input type="hidden" name="a" value="save-order" />
         <input type="hidden" name="d" value="<?php echo $DeliveryItemID; ?>" />
        <input type="hidden" name="o" value="<?php echo $OrderID; ?>" />
    <div class="content col-md-10 col-md-offset-1"> 
        <div class="option-info section row">
            <h2>Customer: <?php echo $OrderDetails->customer; ?></h2>
            <div class="row well">
            <div class="delivery-date-time">Phone Number:  <?php echo $OrderDetails->phone; ?></div>
            <div class="delivery-date-time">Email:  <?php echo $OrderDetails->email; ?></div>
            <h4>Customer Notes</h4>
            <textarea name="n" class="form-control"></textarea>
            </div>
            <h1>Edit Order</h1>
            <div class="row well">
              <div class="left-col col-md-6">
                  <div class="dropdown left-col col-md-5">
                      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                          Delivery Location
                          <span class="caret"></span>
                      </button>            
                      <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenuDivider">
                          <?php
                          foreach($LocationDropdown as $l) {
                              $Item = "<strong>".$l->location."</strong><br/>".date("D M jS", $l->starttime)."<br/>".date("g:ia",$l->starttime);
                              if ($l->locationid == $OrderDetails->locationid) {
                                  $SelectedLocationText = $Item;
                                  $CSS = " hidden ";
                              } else {
                                  $CSS = "";
                              }
                          ?>
                          <li role="presentation" class="location-option-container<?php echo $CSS; ?>"><a role="menuitem" tabindex="-1" href="javascript:void(null);" class="location-option" delivery-id="<?php echo $l->deliveryid; ?>" ><?php echo $Item; ?></a></li>
                          <li role="presentation" class="divider"></li>
                          <?php
                          }
                          ?>
                      </ul>
                  </div>
                  <div class="right-col col-md-5" id="selected-location">
                     <?php
                     $SelectedLocationText = "<strong>".$OrderDetails->location."</strong><br/>".date("D M jS", $OrderDetails->starttime)."<br/>".date("g:ia",$OrderDetails->starttime);
                     echo $SelectedLocationText; ?>
                  </div>
                  <input type="hidden" name="d" value="<?php echo $OrderDetails->deliveryid; ?>" id="delivery-id" /> 
              </div>
              <div class="right-col col-md-6 right">
                <h4>Order Total: $<span id="order-total"><?php echo number_format($OrderDetails->total, 2, ".", ","); ?></span></h4>
                <a href="javascript:window.history.back();" class="btn btn-warning">Cancel</a> <input type="submit" class="btn btn-success" value="Save"/>
              </div>
            </div>
        </div>
<?php
$hidden = array();
    
foreach($Categories as $c) {

    $Products = om_get_products_by_category($c->id, $DeliveryID);
    
    if ($Products["StatusCode"] == 1) {
        
        $Products = $Products["Data"];
        $CategoryEmpty = true;
        
        foreach($Products as $p) {
            if ($p->productavailable > 0 && $p->visible > 0) {
                $CategoryEmpty = false;
            }
        }
            
        if (!$CategoryEmpty) {
        ?>
        <h2><?php echo $c->name; ?></h2>
        <?php
        }
        
        foreach($Products as $p) {
    
            if ($p->productavailable == 0 || $p->visible == 0) {
                $ProductAvailable = false;
            } else {
                $ProductAvailable = true;
            }
            
            if ($ProductAvailable) {
              order_management_product_display($p);
            } else {
              $hidden[$p->pid] = $p;
            }
        }   
    }
}

if (count($hidden) > 0) {
    ksort($hidden, SORT_NUMERIC);
?>
   <h2>Hidden Products</h2>
<?php
    foreach($hidden as $h) {
        order_management_product_display($h);
    }
}
?>
  <input type="hidden" id="order-total-bucket" value="0.00" />
        </div>
    </div>
</form>
<?php
function order_management_product_display($p) {
?>
<div class="row order-item">
  <div class="col-md-1 vertical-align">
  <a href="javascript:void(null);" class="product-id" title="Product ID">
    <?php echo $p->pid; ?>
    </a>

  </div>
    <div class="pic col-md-2">
        <?php
        $Image = strlen( $p->thumbnail) > 0 ?  $p->thumbnail : "NoPhoto.png";
        ?>
        <img src="/sites/default/files/products/small/<?php echo $Image; ?>"  alt="" class="admin-pic">
    </div>
    <div class="col-md-4">
        <h4><?php echo $p->name; ?></h4>
        <p>
        <?php echo $p->summary; ?>
        </p>
        <?php
        if ($p->bulkminquantity > 1) {
        ?>
        <p class="red-text">Minimum Bulk Quantity: <?php echo $p->bulkminquantity; ?>+</p>
        <?php
        }
        ?>
    </div>
    <div class="price col-md-2">
            <?php
            if ($p->pricelb > 0) {
            ?>            
            <p>Price per lb. $<?php echo number_format($p->pricelb, 2, ".", ","); ?></p>
            <?php
            }
            ?>            
            <?php
            if ($p->estimatedpriceperitem > 0) {
            ?>              
            <p>Estimated Cost Per Item $<?php echo number_format($p->estimatedpriceperitem, 2, ".", ","); ?></p>
            <?php
            }
            ?>
            <?php
            if ($p->bulkpricelb > 0) {
            ?>              
            <p class="red-text">Bulk Price per lb. $<?php echo number_format($p->bulkpricelb, 2, ".", ","); ?></p>
            <?php
            }
            ?>
            <?php
            if ($p->estimatedbulkpriceperitem > 0) {
            ?>             
            <p class="red-text">Estimated Bulk Cost Per Item $<?php echo number_format($p->estimatedbulkpriceperitem, 2, ".", ","); ?></p>
            <?php
            }
            ?>            
    </div>
    <div class="quantity col-md-2">
        <h4>Quantity</h4>
        <?php
        $Quantity = intval($Items[$p->pid]["quantity"]);
        ?>            
        <input type="text" value="<?php echo $Quantity; ?>" name="quantity[<?php echo $p->pid; ?>]" bulk-min="<?php echo $p->bulkminquantity; ?>" price-lb="<?php echo $p->pricelb; ?>" price-lb-bulk="<?php echo $p->bulkpricelb; ?>" unit-cost-bulk="<?php echo $p->estimatedbulkpriceperitem; ?>" unit-cost="<?php echo number_format($p->estimatedpriceperitem, 2, ".", ","); ?>" pid="<?php echo $p->pid; ?>" class="quantity-input" <?php echo $ProductAvailable["Disable"]; ?>/>
        <input type="hidden" value="<?php echo isset($Items[$p->pid]["estimatedpriceperitem"]) ? $Items[$p->pid]["estimatedpriceperitem"] : $p->estimatedpriceperitem; ?>" name="cost[<?php echo $p->pid; ?>]"  id="cost-<?php echo $p->pid; ?>" />
        <input type="hidden" value="<?php echo isset($Items[$p->pid]["pricelb"]) ? $Items[$p->pid]["pricelb"] : $p->pricelb; ?>" name="lb[<?php echo $p->pid; ?>]"  id="lb-<?php echo $p->pid; ?>" />
        <input type="hidden" value="<?php echo isset($Items[$p->pid]["pricemodel"]) ? $Items[$p->pid]["pricemodel"] : "regular"; ?>" name="pricemodel[<?php echo $p->pid; ?>]"  id="pricemodel-<?php echo $p->pid; ?>" />
        <input type="hidden" value="<?php echo floatval($Items[$p->pid]["total"]); ?>"  id="total-holder-<?php echo $p->pid; ?>" class="item-total" />
    </div>
        <div class="total col-md-2">
            <h4>Est. Total</h4>
            <?php
            $Total = number_format($Items[$p->pid]["total"], 2, ".", ",");
            ?>
            <span class="red-text" id="total-<?php echo $p->pid; ?>">$<?php echo $Total; ?></span>
        </div>
</div>
<?php
}
?>
<script type="text/javascript">
  jQuery(function() {
    jQuery(".vertical-align").verticalAlign();
    });
</script>