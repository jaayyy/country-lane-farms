<?php

$Categories = om_product_categories_get();

if ($Categories["StatusCode"] == 1) {
    $Categories = $Categories["Data"];
} else {
    $Categories = false;
}
?>
<div class="top-buttons">
    <a href="/admin/order_management/productcategory" class="btn btn-success">Create New Category</a>
</div>
<table class="table table-striped">
    <tbody>
        <tr>
            <th>
                Category ID
            </th>
            <th>
                Name
            </th>
            <th>
                Parent
            </th>
            <th>
                Summary
            </th>
            <th>
                Created
            </th>
            <th>
                Last Update
            </th>
            <th>
                Action
            </th>
        </tr>
        <?php
         foreach($Categories as $c) {
        ?>
        <tr>
            <td>
                <?php echo $c->id; ?>
            </td>
            <td>
                <?php
                echo $c->name;
                ?>
            </td>
            <td>
                <?php echo $c->parent; ?>
            </td>
            <td>
                <?php echo $c->summary; ?>
            </td>
            <td>
                <?php echo date("Y-m-d H:i:s", $c->created); ?>
            </td>
            <td>
                <?php echo date("Y-m-d H:i:s", $c->lastupdated); ?>
            </td>
            <td>
                <a href="/admin/order_management/productcategory/?c=<?php echo $c->id; ?>" class="btn btn-primary btn-sm">Edit</a>
            </td>
        </tr>       
        <?php
        }
        ?>
    </tbody>
</table>