<?php
date_popup_add();
$FormData = $_REQUEST["f"];
$StartDate = $FormData["start"];
$EndDate = $FormData["end"];
$Status = strlen($FormData["status"]) > 0 ? $FormData["status"] : "all";

if (strlen($StartDate) > 0 && strlen($EndDate) > 0) {
    $StartDate = date("Y-m-d", strtotime($StartDate));
    $EndDate = date("Y-m-d", strtotime($EndDate));
    $Report = om_get_product_order_summary_report($StartDate, $EndDate);
    if ($Report["StatusCode"] == 1) {
       $Report = $Report["Data"];
    } else {
        $Report = false;
    }
} else {
    $Report = false;
    $StartDate = date("Y-m-d", strtotime("-1 Month"));
    $EndDate = date("Y-m-d");
}
?>
<script type="text/javascript">
(function ($) {
    $(document).ready(function() {
        $("#start-date").datepicker();
        $("#end-date").datepicker();
    });
})(jQuery);    
</script>
<form action="" method="get">
<div>
<table class="table">
    <tr>
        <td align="right">
            Start Date:
        </td>
    <td>
    <input type="text" name="f[start]" value="<?php echo $StartDate; ?>" id="start-date"/>
    </td>
    <td align="right">
        End Date:
    </td>
    <td>
        <input type="text" name="f[end]" value="<?php echo $EndDate; ?>" id="end-date"/>
    </td>
    </tr>
    <tr>
        <td colspan="5" align="right">
            <?php
            if ($Report != false) {
                $QueryData = base64_encode(json_encode($FormData));
            ?>
            <a href="/admin/order_management/export/?a=product-order-summary&d=<?php echo $QueryData; ?>" class="btn btn-primary">Export to Excel</a>
            <?php
            }
            ?>
            <input type="submit" value="Generate Report"  class="btn btn-success"/>
        </td>
    </tr>
</table>
</div>
</form>

<?php
if ($Report != false) {
?>
<table class="table table-striped">
    <tr>
        <th>
            Location
        </th>
        <th>
            Pickup Date
        </th>
        <th>
            Pickup Time
        </th>
        <th>
            Last Name
        </th>
        <th>
            First Name
        </th>
        <th>
            Phone Number
        </th>
        <th>
            Email
        </th>
        <th>
            Product
        </th>
        <th>
            Quantity
        </th>
        <th>
            Price
        </th>
        <th>
            Order Value
        </th>
    </tr>
    <tbody>
        <?php
 
        foreach($Report as $r) {
        ?>
    <tr>
        <td nowrap style="text-align:left;">
            <?php echo $r->location; ?>
        </td>
        <td nowrap style="text-align:left;">
            <?php echo date("D M d/y", $r->deliverydate); ?>
        </td>
        <td nowrap>
            <?php echo date("g:ia", $r->starttime)." - ".date("g:ia", $r->endtime); ?>
        </td>
        <td style="text-align:left;">
            <?php echo $r->lastname; ?>
        </td>
        <td style="text-align:left;">
            <?php echo $r->firstname; ?>
        </td>
        <td style="text-align:left;">
            <?php echo $r->phone; ?>
        </td>
        <td style="text-align:left;">
            <?php echo $r->email; ?>
        </td>
        <td style="text-align:left;">
            <?php echo $r->product; ?>
        </td>
        <td>
            <?php echo $r->quantity; ?>
        </td>
        <td  style="text-align:right;">
            <?php
            if ($r->weighedtotal > 0) {
                echo om_money($r->weighedtotal);
            } else {
                echo om_money($r->total);
            }
            ?>
        </td>
        <td style="text-align:right;">
            <?php
            if ($r->orderweighed > 0) {
                echo om_money($r->orderweighed);
            } else {
                echo om_money($r->ordertotal);
            }
            ?>
        </td>                
    </tr>
    <?php
        }
        ?>
    </tbody>
</table>
<?php
} else if ($Report == false && strlen($StartDate) > 0) {
            
    ?>
<p>
    No data retreived for the selected period.
</p>
    </tr>
    <?php
        }
    ?>