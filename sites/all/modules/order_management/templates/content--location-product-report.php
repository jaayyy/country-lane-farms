<?php
$DeliveryID = intval($_REQUEST["d"]);
$Summary = om_get_report_summary($DeliveryID);

if ($Summary["StatusCode"] == 1) {
    $Summary = $Summary["Data"];
} else {
    $Summary = false;
}

$Report = om_get_report($DeliveryID);

if ($Report["StatusCode"] == 1) {
    $Report = $Report["Data"];
} else {
    $Report = false;
}
?>
<div class="row">
    <div class="col-md-8">
        LOGO
    </div>
    <div class="col-md-4">
        <a href="" class="btn btn-primary btn-block btn-lg">Export To Excel</a>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
       <h2>Product Sales Report - Actual Sales</h2>
       <h4>Status: Delivery <?php echo ucwords($Summary->deliverystatus); ?></h4>
    </div>
    <div class="col-md-4 report-total">
        <div class="report-total-revenue">
            Total Revenue
        </div>
        <div class="report-total-revenue">
            <?php echo om_money($Summary->totalrevenue); ?>
        </div>        
        
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <h2>Delivery Location: <?php echo $Summary->location; ?></h2>
        <h4>Date: <?php echo date("D M j/y", $Summary->starttime); ?></h4>
        <h4>Time: <?php echo date("g:ia", $Summary->starttime); ?> - <?php echo date("g:ia", $Summary->endtime); ?></h4>
    </div>
    <div class="col-md-2 sales-percentage report-total">
        Regular Price Sales<br/>
        Revenue (<?php echo $Summary->regularpercent; ?>%) <br/>
        <?php echo om_money($Summary->regularrevenue); ?>
    </div>
    <div class="col-md-2 sales-percentage report-total">
        Bulk Price Sales <br/>
        Revenue (<?php echo $Summary->bulkpercent; ?>%) <br />
        <?php echo om_money($Summary->bulkrevenue); ?> <br />
    </div>    
</div>
<table class="table table-striped">
    <tr>
        <th>
            Product
        </th>
        <th align="center">
            Reg Price<br/>Quantity
        </th>
        <th align="center">
            BK Price<br/>Quantity
        </th>
        <th>
            Total<br/>Quantity
        </th>
        <th>
            Reg Price<br/>Revenue
        </th>
        <th>
            Reg Price<br/>Revenue Weighed
        </th>        
        <th>
            BK Price<br/>Revenue
        </th>
        <th>
            BK Price<br/>Revenue Weighed
        </th>        
        <th>
            Total<br/>Revenue
        </th>
        <th>
            Total<br/>Revenue Weighed
        </th>        
    </tr>
    <tbody>
        <?php
        foreach($Report as $r) {
        ?>
    <tr>
        <td>
            <?php echo $r->product; ?>
        </td>
        <td align="center">
            <?php echo $r->quantityregular; ?>
        </td>
        <td align="center">
            <?php echo $r->quantitybulk; ?>
        </td>
        <td align="center">
           <?php echo $r->totalquantity; ?>
        </td>
        <td align="right">
            <?php echo om_money($r->regularrevenue); ?>
        </td>
        <td align="right">
            <?php echo om_money($r->regularrevenueweighed); ?>
        </td>        
        <td align="right">
            <?php echo om_money($r->bulkrevenue); ?>
        </td>
        <td align="right">
            <?php echo om_money($r->bulkrevenuweighed); ?>
        </td>        
        <td align="right">
            <?php echo om_money($r->totalrevenue); ?>
        </td>
        <td align="right">
            <?php echo om_money($r->totalrevenueweighed); ?>
        </td>        
    </tr>
    <?php
        }
    ?>
    </tbody>
</table>