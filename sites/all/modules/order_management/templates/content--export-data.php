<?php
$Action =  $_REQUEST["a"];
$Data = json_decode(base64_decode($_REQUEST["d"]));

switch ($Action) {
    case "product-sales":
        $FileName = "product-sales-report--".$Data->start."--".$Data->end.".csv";
        $RawData = om_get_product_sales_report($Data->start, $Data->end, $Data->status);
        $HeaderArray = array(
                             "Product ID", "Product", "Reg Price Quantity", "BK Price Quantity", "Total Quantity", "Reg Price Revenue", "Reg Price Revenue Weighed", "BK Price Revenue", "BK Price Revenue Weighed", "Total Revenue", "Total Revenue Weighed"
                             );
        if ($RawData["StatusCode"] == 1) {
            foreach($RawData["Data"] as $r) {
                $DataSet[] = get_object_vars($r);
            }
        } else {
            $DataSet = false;
        }

        break;
    
    case "people":
         $FileName = "people-report--".$Data->start."--".$Data->end.".csv";
        $RawData = om_get_people_report($Data->start, $Data->end);
        $HeaderArray = array(
                             "ID Number", "Name", "Address", "Username/Email", "Phone Number", "Pickup Location", "Receive Newsletter", "Account Created", "Member For", "Last Order", "Last Access"
                             );
        if ($RawData["StatusCode"] == 1) {
            foreach($RawData["Data"] as $r) {
                $r->address = om_remove_brs($r->address);
                $r->lifetime  = om_member_since_format($r->lifetime);
                $DataSet[] = get_object_vars($r);
            }
        } else {
            $DataSet = false;
        }
       
        break;
    
    case "product-order-summary":
        $FileName = "product-order-summary-report--".$Data->start."--".$Data->end.".csv";
        $RawData = om_get_product_order_summary_report($Data->start, $Data->end);
        $HeaderArray = array(
                             "Location", "Pickup Date", "Pickup Time", "Last Name", "First Name", "Phone Number", "Email", "Product", "Quantity", "Price", "Order Value"
                             );
        if ($RawData["StatusCode"] == 1) {
            foreach($RawData["Data"] as $r) {
                $Row = get_object_vars($r);
                $DataRow = array(
                                   "location"   => $Row["location"],
                                   "pickupdate" =>  date("D M d/y", $Row["deliverydate"]),
                                   "time"       =>  date("g:ia", $Row["startttime"])." - ".date("g:ia", $Row["endtime"]),
                                   "lastname"   =>  $Row["lastname"],
                                   "firstname"  =>  $Row["firstname"],
                                   "phone"      =>  $Row["phone"],
                                   "email"      =>  $Row["email"],
                                   "product"    =>  $Row["product"],
                                   "quantity"   =>  $Row["quantity"]
                                   );
                
            if ($r->weighedtotal > 0) {
                $DataRow["price"] = $r->weighedtotal;
            } else {
                $DataRow["price"] = $r->total;
            }
            
            if ($r->orderweighed > 0) {
                $DataRow["ordertotal"] = $r->orderweighed;
            } else {
                $DataRow["ordertotal"] = $r->ordertotal;
            }            
                
            $DataSet[] = $DataRow;
            }
        } else {
            $DataSet = false;
        }        
        break;
     
    case "orderqueue":
            $FileName = "upcoming-order-queue-".$Data->location."-".$Data->date."csv";
            $DeliveryItemID = $Data->id;
            $HeaderArray = array(
                                 "Customer Name", "Order Placed", "Total # of Items", "Completed", "Est Total", "Total", "Status"
                                 );
            $DataSet = array();
            
            $QueuedOrders = om_order_queue_get($DeliveryItemID, "queue");
            
            if ($QueuedOrders["StatusCode"] == 1) {
                $QueuedOrders = $QueuedOrders["Data"];
                foreach($QueuedOrders AS $o) {
                    $Row = array(
                                 "name" =>  $o->name,
                                 "orderplaced"  =>  $o->ordercreated,
                                 "items"        =>  $o->numberitems,
                                 "completed"    =>  "",
                                 "esttotal"     =>  $o->ordertotal,
                                 "total"        =>  0,
                                 "status"       =>  "Queued"
                                 );
                    $DataSet[] = $Row;                

                }
            }
            
            $OpenOrders = om_order_queue_get($DeliveryItemID, "open");
            
            if ($OpenOrders["StatusCode"] == 1) {
                $OpenOrders = $OpenOrders["Data"];
                
                foreach($OpenOrders AS $o) {
                    $Row = array(
                                 "name" =>  $o->name,
                                 "orderplaced"  =>  $o->ordercreated,
                                 "items"        =>  $o->numberitems,
                                 "completed"    =>  "",
                                 "esttotal"     =>  $o->ordertotal,
                                 "total"        =>  0,
                                 "status"       =>  "Open"
                                 );
                    $DataSet[] = $Row;
                }
            }
            
            $CompletedOrders = om_order_queue_get($DeliveryItemID, "cmpt");
            
            if ($CompletedOrders["StatusCode"] == 1) {
                $CompletedOrders = $CompletedOrders["Data"];
                    foreach($CompletedOrders AS $o) {
                        $Row = array(
                                     "name" =>  $o->name,
                                     "orderplaced"  =>  $o->ordercreated,
                                     "items"        =>  $o->numberitems,
                                     "completed"    =>  $o->ordercompleted,
                                     "esttotal"     =>  $o->ordertotal,
                                     "total"        =>  $o->totalweighed,
                                     "status"       =>  "Open"
                                     );
                        $DataSet[] = $Row;
                    }            
            }                        
        break;
    
    default:
        die();
        break;
}

if (strlen($FileName) > 0 && $DataSet !== false) {
    drupal_add_http_header('Content-Type', 'text/csv; utf-8');
    drupal_add_http_header('Content-Disposition', 'attachment;filename='.$FileName);    
    $Output .= om_array_to_csv($HeaderArray);
    
    foreach ($DataSet as $d) {
        
        $Output .= om_array_to_csv($d);
    }
    
    print $Output;
    exit;
}

?>