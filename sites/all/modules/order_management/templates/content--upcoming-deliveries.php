<?php
$Deliveries = om_get_upcoming_deliveries();

if ($Deliveries["StatusCode"] == 1) {
    $Deliveries = $Deliveries["Data"];
} else {
    $Deliveries = false;
}
if ($Deliveries == false) {
    ?>
    <div class="alert alert-info">
        There are no Upcoming Deliveries to display. :(
    </div>
    <?php
} else {
?>
<table class="table table-striped">
    <tr>
        <th>
            Delivery Date
        </th>
        <th>
            Location
        </th>
        <th>
            Delivery Type
        </th>
        <th>
            Total Orders
        </th>
        <th>
            Estimated Revenue
        </th>
        <th>
            Orders
        </th>
        <th>
            Sales Report
        </th>
    </tr>
    <tbody
    <?php
    foreach ($Deliveries as $d) {
    ?>
    <tr>
        <td>
            <?php echo $d->deliverydate; ?>
        </td>
        <td>
            <?php echo $d->location; ?>
        </td>
        <td>
            <?php echo $d->deliverytype; ?>
        </td>
        <td class="report-integer">
            <?php echo $d->numberorders; ?>
        </td>
        <td class="report-money">
            <?php echo om_money($d->totalrevenue); ?>
        </td>
        <td>
           <a href="/admin/order_management/orderqueue/?d=<?php echo $d->id; ?>" class="btn btn-primary">Open Orders</a>
        </td>
        <td>
            <a href="/admin/order_management/locationproductsales/?d=<?php echo $d->id; ?>" class="btn btn-primary">Product Sales</a>
        </td>
    </tr>
    <?php
    }
}
    ?>
    </tbody>
</table>