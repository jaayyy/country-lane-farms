<?php
drupal_add_js($base_url."/".drupal_get_path('module', 'order_management')."/js/jquery.delivery-date.js");
drupal_add_js($base_url."/".drupal_get_path('module', 'order_management')."/js/jquery.vertical-align.js");
$DeliveryID = intval($_REQUEST["d"]);
date_popup_add();

$Delivery = om_get_delivery($DeliveryID);

if ($Delivery["StatusCode"] == 1) {
    $Delivery = $Delivery["Data"];
} else {
    $Delivery = false;
}

$DetailsData = om_get_delivery_details($DeliveryID);
if ($DetailsData["StatusCode"] == 1) {
    $Details = array();
    $DetailsData = $DetailsData["Data"];
    foreach($DetailsData as $d) {
        $Details[$d->locationid] = array(
                                "id"        =>  $d->id,
                                "start"     =>  date("h:i", $d->starttime),
                                "startap"   =>  date("A", $d->starttime),
                                "end"       =>  date("h:i", $d->endtime),
                                "endap"       =>  date("A", $d->endtime)
        );
    }
} else {
    $DetailsData = false;
}

$Locations = om_location();

if ($Locations["StatusCode"] == 1) {
    $Locations = $Locations["Data"];
} else {
    $Locations = false;
}

$DeliveryTypes = om_get_delivery_types();

if ($DeliveryTypes["StatusCode"] == 1) {
    $DeliveryTypes = $DeliveryTypes["Data"];
} else {
    $DeliveryTypes = false;
}

$DTSelected[$Delivery->deliverytypeid] = "selected='selected'";

?>

<?php
if (isset($Delivery->deliverydate)) {
    $DeliveryDate = date("m/d/Y", strtotime($Delivery->deliverydate));
} else {
    $DeliveryDate = date("m/d/Y");
}
if ($DeliveryID > 0) {
    $PageAction = "Edit";
} else {
    $PageAction = "Create";
}
?>
<h1 class="page-header"><?php echo $PageAction; ?> Delivery Date</h1>
<form action="/admin/order_management/omprocessing" method="post" class="form-horizontal" id="delivery-date-form">
    <input type="hidden" name="a" value="save-delivery-date" />
    <input type="hidden" name="f[id]" value="<?php echo $DeliveryID; ?>" />
    <div class="form-group">
        <label for="delivery-date" class="col-sm-2 control-label">Delivery Date</label>
        <div class="col-xs-2">
          <input type="text" class="form-control date-day" id="delivery-date" value="<?php echo $DeliveryDate; ?>" name="f[deliverydate]"> 
        </div>        
    </div>
    <div class="form-group">
        <label for="delivery-type" class="col-sm-2 control-label">Delivery Type</label>
        <div class="col-sm-4">
            <select name="f[deliverytypeid]" id="delivery-type" class="form-control">
                <?php
                foreach($DeliveryTypes as $d) {
                ?>
                <option value="<?php echo $d->id; ?>" <?php echo $DTSelected[$d->id]; ?>><?php echo $d->name; ?></option>
                <?php
                }
                ?>
            </select>
        </div>        
    </div>
    <div class="col-lg-12">
        <table class="table table-striped delivery-locations-table table-condensed" cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <th colspan="2">
                        Locations
                    </th>
                    <th>
                        Start Time
                    </th>
                    <th>
                        End Time
                    </th>
                </tr>
                <?php
                foreach($Locations as $l) {
                    if (isset($Details[$l->id])) {
                        $checked = "checked";
                        $CSS = "om-item-selected";
                    } else {
                        $checked = "";
                        $CSS = "";
                    }                    
                ?>
                <tr class="<?php echo $CSS; ?>">
                    <td class="<?php echo $CSS; ?>">
                        <input type="checkbox" class="form-control location-check" name="location[<?php echo $l->id; ?>]" id="" value="<?php echo $Details[$l->id]["id"]?>" <?php echo $checked; ?> location-id="<?php echo $l->id; ?>"/>
                    </td>
                    <td align="left" class="<?php echo $CSS; ?>">
                        <div class="vertical-align"><?php echo $l->name; ?></div>
                    </td>
                    <td align="center" class="<?php echo $CSS; ?>">
                        <div class="col-sm-10">
                            <span class="col-sm-5 t-selector">
                                <input type="text" name="times[<?php echo $l->id; ?>][start]" class="form-control" value="<?php echo $Details[$l->id]["start"]; ?>" placeholder="00:00" id="start-<?php echo $l->id; ?>"/>
                            </span>
                            <span class="col-sm-5 t-selector">
                                <?php echo order_management_ap_select("times[".$l->id."][startap]", $Details[$l->id]["startap"]); ?>
                            </span>
                        </div>
                    </td>
                    <td align="center" class="<?php echo $CSS; ?>">
                        <div class="col-sm-10">
                            <span class="col-sm-5 t-selector">
                                <input type="text" name="times[<?php echo $l->id; ?>][end]" class="form-control" value="<?php echo $Details[$l->id]["end"]; ?>" placeholder="00:00" id="end-<?php echo $l->id; ?>"/>
                            </span>
                            <span class="col-sm-5 t-selector">
                                <?php echo order_management_ap_select("times[".$l->id."][endap]", $Details[$l->id]["endap"]); ?>
                            </span>
                        
                        </div>
                    </td>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        <div class="form-buttons">
            <a href="javascript:window.history.back();" class="btn btn-warning">Cancel</a>&nbsp;&nbsp;&nbsp;<input type="submit" value="Save" class="btn btn-success" />
        </div>         
    </div>
   
</form>

<script type="text/javascript">
  jQuery(function() {
    jQuery(".vertical-align").verticalAlign();
    });
</script>