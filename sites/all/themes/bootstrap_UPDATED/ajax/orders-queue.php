<?php
chdir(getcwd() . '/../../../../../');


define('DRUPAL_ROOT', getcwd());
require_once ('./includes/bootstrap.inc');

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);


$Action = $_REQUEST["a"];

if (strlen($Action) == 0) {
    die();
}

switch ($Action) {
    case "enqueue":
        $DeliveryID = intval($_REQUEST["d"]);
        $QueueData = om_order_queue_data($DeliveryID);
        if ($QueueData["StatusCode"] == 1) {
            $QueueData = $QueueData["Data"];
            $QueuePosition = $QueueData->nextqueue;
        }
        $Request = array(
                        "id"            =>  $_REQUEST["o"],
                        "status"        =>  "queued",
                        "queueposition" =>  $QueuePosition
                         );

        $Result = om_order_save($Request);

        if ($Result["StatusCode"] == 1) {
            echo "OK";
        } else {
            echo "ERROR";
        }
        break;

}

?>