(function ($) {
    $(document).ready(function() {
        $(".location-option").on("click", function() {
            var deliveryID = $(this).attr("delivery-id");
            var text = $(this).html();
            $("#delivery-id").val(deliveryID);
            $(".location-option-container").removeClass("hidden");
            $(this).parent().fadeOut("slow", function() {
                $(this).addClass("hidden").css("display" ,"block");
                });
            $("#selected-location").fadeOut("slow", function() {
                $(this).html(text);
                $(this).fadeIn("slow");
            });
        });
        
    });
})(jQuery);