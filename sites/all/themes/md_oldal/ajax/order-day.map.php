<?php

if (isset($_REQUEST["lat"])) {
    $Latitude = $_REQUEST["lat"];
}
if (isset($_REQUEST["lon"])) {
    $Longitude = $_REQUEST["lon"];
}
?>

<style>
#map-canvas {height: 250px}
#map-canvas img{ max-width: none; }
</style>



    
<script type="text/javascript">
   var map;
    var marker;
  
      function initialize() {
        var latLng = new google.maps.LatLng(<?php echo $Latitude ?>,<?php echo $Longitude; ?>);
        var mapOptions = {
          center: latLng,
          zoom: 15,
          
        };
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        
    
        marker = new google.maps.Marker({
            position: latLng,
            map: map
            
            
        });
        
        
      }
      google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="map-canvas"></div>