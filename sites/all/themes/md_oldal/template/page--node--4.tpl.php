<?php

global $base_url;
global  $base_path;

drupal_add_js($base_url."/".drupal_get_path('theme', 'md_oldal')."/js/jquery.order-day.js");
$DeliveryData = om_delivery_itinerary_get();
if ($DeliveryData["StatusCode"] == 1) {
    $PreferredLocation = om_deliveries_preferred($DeliveryData["Data"]);
    $PreferredLocationData = $PreferredLocation["Preferred"];
    unset($PreferredLocation["Preferred"]);
    $DeliveryData = om_deliveries_array($DeliveryData["Data"]);
} else {
    $DeliveryData = false;
}


$locations = om_location();
?>
<div class="main-wrap">

    <?php include 'header.php'; ?>

    

    <div class="page-banner row">
      <img src="/sites/all/themes/md_oldal/img/content/concept-banner.jpg" alt="">
    </div>

    <div class="content col-md-10 col-md-offset-1"> 

      <div class="section row">
        <h1>From Our Farm To You</h1>
        <p>It’s getting harder and harder to find food in the grocery store that isn’t packed with preservatives or unwanted additives. That’s why we decided to bring wholesome farm-fresh food to you. Simply start an order by selecting the most convenient delivery location, then choose from our growing list of organic and sustainable options, and swing by to pick them up. It couldn’t be easier to put a healthy meal on the family table - just like Grandma used to make.</p>
      </div>   

      <div class="map section row">
        <h1>Where we deliver</h1>
        <div class="left-col col-md-8">
        	<?php
					$markers = array();
        	$map='';
        	foreach ($locations['Data'] as $key=>$value) {
						
        		 foreach($value as $t=>$d) {
								if ($t == 'latitude')
									$lat = $d;
								if ($t == 'longitude')
									$long = $d;
		        		 	
						 }
						$map .= $lat.",".$long."|";
						$markers[] = array('lat' => $lat, 'long' => $long);
        	}
			?>
         <!-- <img src="http://maps.google.com/maps/api/staticmap?center=51.079685,-114.054366&zoom=9&scale=2&size=650x300&markers=<?php echo $map;?>" alt="" class="hide">
			-->	
						<style>
						#map-canvas {width: 100%; height: 489px}
						#map-canvas img{ max-width: none; }
						</style>
						<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGpkIuX9xnX99utIUEueakr1znlWx152Y"></script>
						-->
						<script type="text/javascript">
							
								var markers = <?php echo json_encode($locations['Data']);?>;
							
									function initialize() {
										var mapOptions = {
											center: new google.maps.LatLng(51.533873, -114.029312),
											zoom: 8
											
										};
										var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
										var googleMapMarkers = [];
										var contentStrings = [];
										var infoWindows = [];
										
										for(var i in markers){
												addMarker(map, markers[i]);
												
										}
										
									}
									google.maps.event.addDomListener(window, 'load', initialize);
									
									function addMarker(map, place){
										var latLng = new google.maps.LatLng(place.latitude,place.longitude);
										var marker = new google.maps.Marker({
												position: latLng,
												map: map
										});
										
										
										var infoWindow = new google.maps.InfoWindow({
												content: place.name + "<br />" + place.address
										});
										
										
										google.maps.event.addListener(marker, 'click', function() {
												
											infoWindow.open(map,marker);
											
										});
									}
									
									
						</script>
						<div id="map-canvas"></div>
					
					
        </div>
        <div class="right-col col-md-4">
        	<?php
        	foreach ($locations['Data'] as $key=>$value) {
        		 foreach($value as $t=>$d) {
        		 	if ($t == 'name')
        		 		echo "<div>".$d."</div>";
				 }
        	}
			?>
        </div>
      </div>

      <div class="section row">
        <h1>
          Delivery Options
          <span class="legend-mark green">Regular Delivery</span>
          <span class="legend-mark orange">Turkey Only</span>
          <span class="legend-mark blue">Regular &amp; Turkey Delivery</span>
        </h1>
        <div class="del-btns">
          <a href="#" class="del-prev button">Prev. Delivery</a>
          <a href="#" class="del-next button">Next Delivery</a>
        </div>
        <table class="options-table">          
           <tr>
            <th>Location</th>
            <th class="del-1">Next Delivery</th>
            <th class="del-2">Delivery 2</th>
            <th class="del-3">Delivery 3</th>
            <th class="del-4">Delivery 4</th>
          </tr>
          
          <?php

          foreach ($DeliveryData as $k=>$v) {
          ?>
          <tr class="option-row">
            <td class="loc-title"><?php echo $k; ?></td>
            <?php
            $Count = 1;
            $ThisRowCount = 1;
            foreach($v as $t=>$d) {

                if ($ThisRowCount <= 4) {
            ?>
            <td class="del-<?php echo $Count;?>">
              <a href="javascript:void(null);" class="option-item mark <?php echo $d["class"]; ?><?php echo $SelectedCSS; ?>" dt="<?php echo $d["date"]; ?>" when="<?php echo $d["time"]; ?>" delivery-type="<?php echo $d["type"]; ?>" loc="<?php echo $d["name"]; ?>" lat="<?php echo $d["latitude"]; ?>" long="<?php echo $d["longitude"]; ?>" adr="<?php echo $d["address"]; ?>" detail-id="<?php echo $d["itemid"]; ?>"><span><?php echo $t; ?> <?php echo $d["time"]; ?></span></a>
            </td>
            <?php
                }
            $Count++;
            $ThisRowCount++;
            
          }
            ?>
          </tr>
          <?php
          }
          ?>          
        </table>
      </div>
      
      
      
      
      
      
      
      
      	
      	
      	
 <!--     	
      	
        <h1>
          Delivery Options
          <span class="legend-mark green">Regular Delivery</span>
          <span class="legend-mark orange">Turkey Only</span>
          <span class="legend-mark blue">Regular &amp; Turkey Delivery</span>
        </h1>
        <div class="del-btns">
          <a href="#" class="del-prev button">Prev. Delivery</a>
          <a href="#" class="del-next button">Next Delivery</a>
        </div>
        <table class="options-table">        
          
          <tr>
            <th>Location</th>
            <th class="del-1">Next Delivery</th>
            <th class="del-2">Delivery 2</th>
            <th class="del-3">Delivery 3</th>
            <th class="del-4">Delivery 4</th>
          </tr>
          
          <tr>
            <td>Canmore</td>
            <td class="del-1">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-2">
              <span class="orange mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-3">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-4">
              <span class="blue mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
          </tr>          

          <tr>
            <td>Chestermere</td>
            <td class="del-1">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-2">
              <span class="orange mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-3">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-4">
              <span class="blue mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
          </tr>

          <tr>
            <td>Clarion Hotel</td>
            <td class="del-1">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-2">
              <span class="orange mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-3">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-4">
              <span class="blue mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
          </tr>

          <tr>
            <td>Crowfood</td>
            <td class="del-1">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-2">
              <span class="orange mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-3">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-4">
              <span class="blue mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
          </tr>

          <tr>
            <td>Farm Pickup</td>
            <td class="del-1">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-2">
              <span class="orange mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-3">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-4">
              <span class="blue mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
          </tr>

          <tr>
            <td>Midnapore</td>
            <td class="del-1">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-2">
              <span class="orange mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-3">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-4">
              <span class="blue mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
          </tr>

          <tr>
            <td>Red Deer</td>
            <td class="del-1">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-2">
              <span class="orange mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-3">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-4">
              <span class="blue mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
          </tr>

          <tr>
            <td>Southcentre Mall</td>
            <td class="del-1">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-2">
              <span class="orange mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-3">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-4">
              <span class="blue mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
          </tr>

          <tr>
            <td>Varsity</td>
            <td class="del-1">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-2">
              <span class="orange mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-3">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-4">
              <span class="blue mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
          </tr>

          <tr>
            <td>Westbrook Mall</td>
            <td class="del-1">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-2">
              <span class="orange mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-3">
              <span class="green mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
            <td class="del-4">
              <span class="blue mark">Fri Oct 3 5:30-6:30pm</span>
            </td>
          </tr>
        </table>
   </div>-->
      <div class="steps section row">
        <h1>How to order</h1>
        <p>All of our products must be pre–ordered on this website before you can pick them up at any of our delivery locations. This way you will be ensured to get what you want and this allows us to plan ahead for each delivery. Below is a quick step-by-step guide to place an order with us:</p>
        <div class="row">
          <div class="step-title col-sm-2 col-sm-offset-2">Step 1</div>
          <div class="col-sm-6">
            Log into your account. If you don’t have an account on our new system, it is VERY easy to set up your account. All we require is your name/phone/address/e-mail. We DON’T require ANY payment information.
          </div>
        </div>
        <div class="row">
          <div class="step-title col-sm-2 col-sm-offset-2">Step 2</div>
          <div class="col-sm-6">
            Click on the “START ORDER” button and select a location and delivery date/time that works for you.
          </div>
        </div>
        <div class="row">
          <div class="step-title col-sm-2 col-sm-offset-2">Step 3</div>
          <div class="col-sm-6">
            Choose the products you wish to order by entering the amount in the “Quantity” box. Once you have selected all of the products you want, simply click “PLACE ORDER.” Then an order confirmation will appear where you can CONFIRM your order OR go back to make changes.
          </div>
        </div>
        <div class="row">
          <div class="step-title col-sm-2 col-sm-offset-2">Step 4</div>
          <div class="col-sm-6">
            Stop by your delivery location during the scheduled time to pick up your order. If you miss us at your pick up location, you can also visit us at the next stop to pick up your order.
          </div>
        </div>
         <div class="row">
          <div class="step-title col-sm-2 col-sm-offset-2">Step 5</div>
          <div class="col-sm-6">
            Enjoy - and share your recipes with us!
          </div>
        </div>
      </div>

<div class="section row">
        <h1>Helping Hands</h1>
        <p>If you know someone or a family that is going through hard times and would benefit from getting chicken, we can BOTH help. Here is how:</p>
        <p>YOU - bring us a note with a brief story of the situation of the family in need (including their name)<br>
YOU - purchase the chicken<br>
WE - apply a credit of 50% to the cost of the chicken going to the needy family<br>
SO...you pay half and I pay half (for CHICKEN only)</p>
      </div> 
      <div class="section row">
        <h1>Frequently Asked Questions</h1>
        <div class="panel-group" id="faq" role="tablist" aria-multiselectable="true">

          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#faq" href="#answer1" aria-controls="answer1" class="collapsed">
                  Do You sell eggs?
                </a>
              </h4>
            </div>
            <div id="answer1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                No. We only raise our chickens for meat and we have not found a producer that we can sell eggs for. 
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#faq" href="#answer2" aria-controls="answer2" class="collapsed">
                  Is the chicken fresh?
                </a>
              </h4>
            </div>
            <div id="answer2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                All our chicken is sold fresh. We process the birds on a Tuesday afternoon and deliver fresh to you on the Wednesday, Thursday and Friday of the same week. We also bring in fresh salmon fillets from Tofino.  All our other salmon, beef, and seafood products are sold frozen.
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#faq" href="#answer3" aria-controls="answer3" class="collapsed">
                  Do you sell your products in any stores or farmers markets?
                </a>
              </h4>
            </div>
            <div id="answer3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                No. We sell everything directly to our customers at our pre-determined delivery locations with our refrigerated trailer. This allows us to keep costs to a minimum and provides you with fresh products directly from our farm to you! This also allows you to deal DIRECTLY with ME…the farmer.
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#faq" href="#answer4" aria-controls="answer4" class="collapsed">
                  Are your chickens fed hormones?
                </a>
              </h4>
            </div>
            <div id="answer4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                No. Actually, NO chickens in Alberta are fed hormones and if we advertised that our chickens are NOT fed hormones, it would indicate that others are…which is not true. We do not use any antibiotics, which most other producers DO use as a growth promoting which causes the confusion. We also DO NOT use any animal byproducts or chemicals (even during barn clean out)
              </div>
            </div>
          </div>

        </div>
      </div>

    </div>

</div>


<?php include 'footer.php'; ?>


