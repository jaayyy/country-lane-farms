<div class="main-wrap">

    <?php include 'header.php'; ?>

    
    <div class="page-banner row">
      <img src="/sites/all/themes/md_oldal/img/content/concept-banner.jpg" alt="">
    </div>

    <div class="content col-md-10 col-md-offset-1"> 
        <div class="contact-form form">
            <h1>Contact Us</h1>
            <p>
                Please feel free to contact us if you have any questions or concerns. 
                The majority of our business has been built on word of mouth advertising; customer satisfaction is our prime concern.
            </p>
            <p>
                E-mail is the preferred way of contacting us, but you can also call at <strong>403-934-2755</strong> with any questions or concerns.
            </p>
            <p>Required<sup class="red-text">*</sup></p>
            
            <div class="col-md-8 col-md-offset-2"> 
              <form action="" id="contact-form">
                 
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name">Name<sup class="red-text">*</sup></label>
                            <input type="text" id="name" name="name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email Address<sup class="red-text">*</sup></label>
                            <input type="email" id="email" name="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message">Message<sup class="red-text">*</sup></label>
                        <textarea id="message" name="message"></textarea>
                    </div>
                    <button type="submit" form="contact-form" value="Send" class="button pull-right">Send</button>

              </form>
            </div>
          

        </div>
    </div>

</div>

<?php include 'footer.php'; ?>


