<div class="main-wrap">
<?php include 'header.php';
?>
<script type="text/javascript">
    var AJAX_URL = '<?php echo $base_url."/".drupal_get_path('theme', 'md_oldal')."/ajax"; ?>';
</script>
<?php
global $base_url;
global  $base_path;
/*$OrderID = intval($_SESSION["OrderID"]);
    if ($OrderID == 0) {
        drupal_goto("/order-day");
    }*/
drupal_add_js($base_url."/".drupal_get_path('theme', 'md_oldal')."/js/jquery.order-day.js");

$order = om_order_session_get("info");
$DeliveryData = om_get_delivery_info_id($order->deliveryitemid);

if ($DeliveryData["StatusCode"] == 1) {
    $DeliveryData = $DeliveryData["Data"];
} else {
    $DeliveryData = false;
} 

$OrderTotal = 0;
#$OrderItems = om_order_items($OrderID);
$OrderItems = om_order_session_get("items");

if ($OrderItems) {
    #$OrderItems = $OrderItems["Data"];
    foreach($OrderItems AS $v) {
        $OrderTotal += $v->total;
    }
} else {
    $OrderItems = false;
}
?>
<div class="content col-md-10 col-md-offset-1">
    <div class="option-info section row">
        
        <div class="left-col col-md-5">
            <h1>Order Pending</h1>
            <p>Your order is pending, please review and confirm.</p>
            <div id="pickup-details">
                <?php
                $Date = date("l F jS", $DeliveryData->starttime);
                $Time = date("g:ia", $DeliveryData->starttime)." - ".date("g:ia", $DeliveryData->endtime);
                $Type = $DeliveryData->deliverytype;
                $Name = $DeliveryData->location; 
                $Address = $DeliveryData->address;
                
                require_once(dirname(__FILE__)."/../ajax/order-day.details.php");
                ?>
            </div>
        </div>
        <div class="right-col col-md-7">
        	<div class="submit-buttons-right">
                <a href="/order-list/" class="button">Return To Order</a>
                <a href="/thank-you/" class="button" id="confirm-order-button">Confirm</a>
            </div>
            <div id="location-map-container">
                <?php
                $Latitude = $DeliveryData->latitude;
                $Longitude = $DeliveryData->longitude;
                require_once(dirname(__FILE__)."/../ajax/order-day.map.php");
                ?>            
            </div> 
        </div>
           
    </div>


    <div class="order-confirm section row">
        <div class="row">
            <div class="summary-header-container pull-left col-sm-6">
            <h1>Order Summary</h1>
            <p>PLEASE NOTE: All of the costs are estimates only. Each item will be
            weighed when you pick up your order and you will be charged based
            on the exact amount based on the price per lb cost.</p>
            </div>
            <div class="pull-right col-sm-6">
                <h1>Estimated Order Total:<span class="order-total"><?php echo om_money($OrderTotal); ?></span></h1>
            </div>
        </div>
        
        <?php
        foreach ($OrderItems as $i) {
        ?>
        <div class="row">
            <div class="pic col-md-2">
                <img src="/sites/default/files/products/small/<?php echo $i->thumbnail; ?>"  alt="">
            </div>
            <div class="product-title col-md-4">
                <h2><?php echo $i->name; ?></h2>
                <p>
                  <?php echo $i->summary; ?>
                </p>
            </div>
            <div class="price col-md-2">
                <?php
                if ($i->pricemodel == "bulk") {
                    $DiscountFlag = " (<span class='red-text'>Bulk Discount</span>)";
                } else {
                    $DiscountFlag = "";
                }
                ?>
                <h2>Price</h2>
                <?php
                if ($i->pricelb > 0) {
                ?>
                <p>Price per lb. <?php echo om_money($i->pricelb).$DiscountFlag; ?></p>
                
                <?php
                }
                ?>
                <p>Estimated Cost Per Item <?php echo om_money($i->estimatedpriceperitem); ?></p>
            </div>
            <div class="quantity col-md-2 col-xs-6">
                <h2>Quantity</h2>
                <span class="order-summary-number"><?php echo $i->quantity; ?></span>
            </div>
            <div class="total col-md-2 col-xs-6">
                <h2>Total</h2>
                <span class="order-summary-number"><?php echo om_money($i->total); ?></span>
            </div>
        </div>
        <?php
        }
        ?>
        <div class="submit-buttons-right">
            <a href="/order-list/?o=<?php echo $OrderID; ?>" class="button">Return To Order</a>
            <a href="/thank-you/?o=<?php echo $OrderID; ?>" class="button">Confirm</a>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>



