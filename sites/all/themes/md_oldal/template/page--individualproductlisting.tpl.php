<div class="main-wrap">

    <?php include 'header.php';
    var_dump(arg(1));
    $ProductID = intval($_REQUEST["p"]);
    if ($ProductID > 0) {
        $Product = om_get_product_by_id($ProductID);
        if ($Product["StatusCode"] == 1) {
            $Product = $Product["Data"];
        }
    }
    ?>
    

    <div class="content">    
      <div class="left-col col-md-6">
        <div class="pic-block">
          <img src="/sites/all/themes/md_oldal/img/content/chicken-pic.png" alt="">
        </div>
        <div class="red-message">
          Please note that October 8 - 10 will ONLY 
          be a FRESH TURKEY delivery. We will NOT 
          have FRESH chicken October 8 - 10, 2014
        </div>
      </div>
      <div class="right-col col-md-5">
        <div class="inner">
          <h1><?php echo $Product->name; ?></h1>
          <div class="text-block">
            <p>
              <?php echo $Product->description; ?>
            </p>

            <p>
              Fresh Whole Roasting chicken-approximately 6 - 7 lbs each.
              To receive the bulk pricing, simply place an order for at least 12 chickens.
            </p>

            <p>
              Price per lb. $4.50 <br/>
              Estimated Cost Per Item $30.00
            </p>

            <p class="red-text">
              Bulk Price per lb. $4.00 <br/>
              Estimated Cost Per Item $26.75 <br/>
              Minimum Bulk Quantity: 12+
            </p>

            <p>
              PLEASE NOTE: All of the costs are estimates only. 
              Each item will be weighed when you pick up your order and 
              you will be charged based on the exact amount based on the price per lb cost.
            </p>
          </div>
          <div class="opts">
            <a href="/order-day" class="button pull-right">Start a new order</a>
          </div>
        </div>
      </div>
    </div>

</div>

<?php include 'footer.php'; ?>


