<div class="main-wrap">
<?php
include 'header.php';

drupal_add_js($base_url."/".drupal_get_path('theme', 'md_oldal')."/js/jquery.order-list.js");

/*$OrderID = intval($_SESSION["OrderID"]);

if ($OrderID == 0) {
    drupal_goto("/order-day");
}*/

$Categories = om_get_product_categories();

if ($Categories["StatusCode"] == 1) {
    $Categories = $Categories["Data"];
} else {
    $Categories = false;
}

#$OrderDetails = om_get_order($OrderID);
$OrderDetails = om_order_session_get("info");

/*
if ($OrderDetails["StatusCode"] == 1) {
    $OrderDetails = $OrderDetails["Data"];
} else {
    $OrderDetails = false;
}
*/
$DetailsID = intval($OrderDetails->deliveryitemid);
$DeliveryID = om_get_deliveryid($DetailsID);

if ($DeliveryID["StatusCode"] == 1) {
    $DeliveryID = intval($DeliveryID["Data"]);
}

#$OrderItems = om_order_items($OrderID);
$OrderItems = om_order_session_get("items");

$Items = array();

if ($OrderItems) {
	
    #$OrderItems = $OrderItems["Data"];
	
    foreach ($OrderItems as $i) {
        $Items[$i->productid] = array("quantity"=>$i->quantity, "total"=>$i->total);
    }
} else {
    $OrderItems = false;
}
/*
$Alerts = om_get_availability_alerts();

if ($Alerts["StatusCode"] == 1) {
    $Alerts = $Alerts["Data"];
} else {
    $Alerts = false;
}
*/
global $user;
//$Favourites = om_get_favourite_products($user->uid, $DeliveryID);
$Favourites["StatusCode"] == 0;
$HideProducts = array();
if ($Favourites["StatusCode"] == 1) {
    $Favourites = $Favourites["Data"];

    $FavouritesCategory = new stdClass();
    $FavouritesCategory->name = "Your Favourites";
    $FavouritesCategory->id = -1;
    array_unshift($Categories, $FavouritesCategory);
}

?>
    <form action="/process-complete-order" method="post" onkeypress="return event.keyCode != 13;" id="complete-order-form">
        <input type="hidden" name="o" value="<?php echo $OrderID; ?>" />
        <div class="option-info section row sticky-order" style="">

        <div class="right-col col-md-10 col-md-offset-1">
          <h1>Estimated order total: $<span class="order-total"><?php echo number_format($OrderDetails->total, 2, ".", ","); ?></span></h1>
          <input type="submit" class="button" value="Place Order"/>
        </div>
      </div>

<div class="content col-md-10 col-md-offset-1" style="margin-top: 120px;margin-bottom:20px;">
      <div class="option-info section row" style="margin-bottom:20px;">
        <div class="left-col col-md-12">

          <h1>Select your products</h1>
          <p>
            PLEASE NOTE: All of the costs are estimates only. Each item will be weighed when you pick up your order and
            you will be charged based on the exact amount based on the price per lb cost.
          </p>
        </div>
        <!--<div class="right-col col-md-7">
          <h1>Estimated order total: $<span class="order-total"><?php echo number_format($OrderDetails->total, 2, ".", ","); ?></span></h1>
          <input type="submit" class="button" value="Place Order"/>
        </div>-->
      </div>

    <div class="order-products-list section row" style="margin-top:20px;">

<?php
foreach($Categories as $c) {
    if ($c->id > 0) {
        $Products = om_get_products_by_category($c->id, $DeliveryID);
    } else {
        $Products = array(
                          "StatusCode"  =>  1,
                          "Data"        =>  $Favourites
                          );
    }
    if ($Products["StatusCode"] == 1) {
        $Products = $Products["Data"];
    ?>

    <h1><?php echo $c->name; ?></h1>
    <?php
    if (strlen($c->alert) > 0) {
    ?>
        <div class="row">
            <div class="red-message full-width product-alert">
                  <?php echo $c->alert; ?>
            </div>
        </div>
    <?php
    }
    ?>
    <?php
    foreach($Products as $p) {

        if ($HideProducts[$p->pid] != true && $p->visible == 1) {
        	//if ($c->id > 0 && $HideProducts[$p->pid] != true || $c->id == -1 && $p->visible == 1) {
            $ProductAvailable = array();
            if ($p->productavailable == 0 ) {
                $ProductAvailable["Available"] = false;
                $ProductAvailable["CSS"] = " product-unavailable";
                $ProductAvailable["Disable"] = "disabled='disabled'";
            } else {
                $ProductAvailable["Available"] = true;
                $ProductAvailable["CSS"] = "";
                $ProductAvailable["Disable"] = "";
            }

            if ($ProductAvailable["Available"] == false) {
                if ($p->available == 0 && strlen($p->alert) > 0) {
                    $ProductAvailable["Message"] = $p->alert;
                } elseif($p->productavailable == 0 && strlen($p->alert) > 0) {
                    $ProductAvailable["Message"] = $p->alert;
                } else {
                    $ProductAvailable["Message"] = "";
                }

            } else {
                $ProductAvailable["Message"] = "";
            }

            ?>
        <div class="row<?php echo $ProductAvailable["CSS"] ?>">
            <div class="pic col-md-2">
                <img src="/sites/default/files/products/small/<?php echo $p->thumbnail; ?>"  alt="">
            </div>
            <div class="product-title col-md-4">
                <h2><?php echo $p->name; ?></h2>
                <p>
                <?php echo $p->summary; ?>
                </p>
                <?php
                if ($p->bulkminquantity > 1) {
                ?>
                <p class="red-text">Minimum Bulk Quantity: <?php echo $p->bulkminquantity; ?>+</p>
                <?php
                }
                ?>
            </div>
            <div class="price col-md-2">
                <?php
                if ($p->pricelb > 0) {
                ?>
                <p>Price per lb. $<?php echo number_format($p->pricelb, 2, ".", ","); ?></p>
                <?php
                }
                ?>
                <?php
                if ($p->estimatedpriceperitem > 0) {
                ?>
                <p>Estimated Cost Per Item $<?php echo number_format($p->estimatedpriceperitem, 2, ".", ","); ?></p>
                <?php
                }
                ?>
                <?php
                if ($p->bulkpricelb > 0) {
                ?>
                <p class="red-text">Bulk Price per lb. $<?php echo number_format($p->bulkpricelb, 2, ".", ","); ?></p>
                <?php
                }
                ?>
                <?php
                if ($p->estimatedbulkpriceperitem > 0) {
                ?>
                <p class="red-text">Estimated Bulk Cost Per Item $<?php echo number_format($p->estimatedbulkpriceperitem, 2, ".", ","); ?></p>
                <?php
                }
                ?>
            </div>


            <div class="quantity col-md-2 col-xs-6">
                <h2>Quantity</h2>
                <?php
                $Quantity = intval($Items[$p->pid]["quantity"]);
                ?>
                <input type="text" value="<?php echo $Quantity; ?>" name="quantity[<?php echo $p->pid; ?>]" bulk-min="<?php echo $p->bulkminquantity; ?>" price-lb="<?php echo $p->pricelb; ?>" price-lb-bulk="<?php echo $p->bulkpricelb; ?>" unit-cost-bulk="<?php echo $p->estimatedbulkpriceperitem; ?>" unit-cost="<?php echo number_format($p->estimatedpriceperitem, 2, ".", ","); ?>" pid="<?php echo $p->pid; ?>" class="quantity-input" <?php echo $ProductAvailable["Disable"]; ?>/>
                <input type="hidden" value="<?php echo $p->estimatedpriceperitem; ?>" name="cost[<?php echo $p->pid; ?>]"  id="cost-<?php echo $p->pid; ?>" />
                <input type="hidden" value="<?php echo $p->pricelb; ?>" name="lb[<?php echo $p->pid; ?>]"  id="lb-<?php echo $p->pid; ?>" />
                <input type="hidden" value="regular" name="pricemodel[<?php echo $p->pid; ?>]"  id="pricemodel-<?php echo $p->pid; ?>" />
                <input type="hidden" value="0"  id="total-holder-<?php echo $p->pid; ?>" class="item-total" />

            </div>
            <div class="total col-md-2 col-xs-6">
                <h2>Est. Total</h2>
                <?php
                $Total = number_format($Items[$p->pid]["total"], 2, ".", ",");
                ?>
                <span class="black-text" id="total-<?php echo $p->pid; ?>">$<?php echo $Total; ?></span>
            </div>
        </div>
        <?php
        if (strlen($ProductAvailable["Message"])>0) {
        ?>
            <div class="row">
                <div class="red-message full-width product-alert">
                      <?php echo $ProductAvailable["Message"]; ?>
                </div>
            </div>
        <?php
                }

        }
    }
    if ($c->id == -1) {
        foreach($Favourites as $f) {
            $HideProducts[$f->pid] = true;
        }
    }
    }
    ?>

    <?php
}
?>
   <input type="hidden" id="order-total-bucket" value="0.00" />
</div>
        <div class="option-info section">
			<div class="right-col">
          		<h1>Estimated order total: $<span class="order-total"><?php echo number_format($OrderDetails->total, 2, ".", ","); ?></span></h1>
          		<input type="submit" class="button" value="Place Order"/>
			</div>
        </div>
</div>

</form>

</div>
<?php include 'footer.php'; ?>