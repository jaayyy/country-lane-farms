<div class="main-wrap">

    <?php include 'header.php'; ?>
    <?php
    
    if (!isset($_SESSION["ORDER"]["info"])) {
        drupal_goto("/order-day");
    }   
$OrderID = om_order_save(get_object_vars(om_order_session_get("info")));

if ($OrderID["StatusCode"] == 1) {
    $_SESSION["OrderID"] = $OrderID["Data"];
    $OrderID = $OrderID["Data"];
}
$Products = om_order_session_get("items");
$OrderTotal = 0;

foreach($Products as $k=>$v) {
    $Item = get_object_vars($v);
    $Item["orderid"] = $OrderID;
    $OrderTotal += $Item["total"];
    $Result = om_order_item_save($Item);
}

$Order = array(
               "id" =>  $OrderID,
               "total"  =>  $OrderTotal,
               "status" =>  'ready'
               );

$result  = om_order_save($Order);
om_order_session_clear();
if ($OrderID > 0) {
    $OrderSummary = om_get_order_summary($OrderID);
    if ($OrderSummary["StatusCode"] == 1) {
        $OrderSummary = $OrderSummary["Data"];
    }
}

om_send_order_confirmation($OrderID);
?>
    <div class="thank-you content col-md-10 col-md-offset-1"> 

          <h1>Thank you for your order!</h1>
          <p>
            <strong>An email has been sent to you with all of your order details. See you soon.</strong>
          </p>
          <p>
            <div><strong>Date: <?php echo $OrderSummary->deliverydate; ?></strong></div>
            <div>Time: <?php echo $OrderSummary->start; ?> - <?php echo $OrderSummary->end; ?></div>
          </p>
          
          <div >
            
            <style>
#map-canvas {height: 330px; margin: auto;}
</style>
    
<script type="text/javascript">
  
    
  
      function initialize() {
        var latLng = new google.maps.LatLng(<?php echo $OrderSummary->latitude; ?>,<?php echo $OrderSummary->longitude; ?>);
        var mapOptions = {
          center: latLng,
          zoom: 15,
          
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        
    
        var marker = new google.maps.Marker({
            position: latLng,
            map: map
            
            
        });
        
        
      }
      google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="map-canvas"></div>
          </div>
        
          <div class="location">
            <p><strong class="bigger-text">Pickup Location:</strong></p>
            <p><strong><?php echo $OrderSummary->name; ?></strong></p>
            <p><?php echo $OrderSummary->address; ?></p>
          </div>

          <div class="total">
            Estimated order total: <span>$<?php echo number_format($OrderSummary->ordertotal,2, ".", ","); ?></span>
          </div>

    </div>

</div>

<?php
unset($_SESSION["OrderID"]);
include 'footer.php'; ?>


