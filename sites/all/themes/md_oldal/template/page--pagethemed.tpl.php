<div class="main-wrap">
    <?php include 'header.php'; ?>
    <div class="page-banner row">
      <img src="/sites/all/themes/md_oldal/img/content/concept-banner.jpg" alt="">
    </div>
    <div class="content col-md-10 col-md-offset-1"> 
      <div class="section row">
            <?php
            print render($node->body["und"][0]["value"]);
            ?>
        </div>
      </div>
    </div>
</div>
<?php include 'footer.php'; ?>