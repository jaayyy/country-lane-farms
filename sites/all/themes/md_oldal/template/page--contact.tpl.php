<div class="main-wrap">
    <?php include 'header.php'; ?>
    <div class="page-banner row">
      <img src="/sites/all/themes/md_oldal/img/content/concept-banner.jpg" alt="">
    </div>
    <div class="content col-md-10 col-md-offset-1"> 
      <div class="section row">

       <div class="contact-form form">
			<?php print render($page["content"]); ?>

		</div>
        </div>
      </div>
    </div>
</div>
<?php include 'footer.php'; ?>