<?php

require(DRUPAL_ROOT."/sites/all/libraries/simpleajaxupload/uploader.php");

// Directory where we store uploaded images
// Remember to set correct permissions or it won't work
$upload_dir = DRUPAL_ROOT."/sites/default/files/products/small";

$uploader = new FileUpload("uploadfile");

// Handle the upload
$result = $uploader->handleUpload($upload_dir);
if (!$result) {
  exit(json_encode(array("success" => false, "msg" => $uploader->getErrorMsg())));
}

echo json_encode(array("success" => true));
header("HTTP/1.1 200 OK");
exit();