<div class="main-wrap">

    <?php include 'header.php';

    $ProductID =  $node->field_productid['und'][0]['value'];
    //$ProductID =  $_GET['p'];

    if ($ProductID > 0) {
        $Product = om_get_product_by_id($ProductID);
        if ($Product["StatusCode"] == 1) {
            $Product = $Product["Data"];
        }
    }
	if ($Product->image != '') {
          $image = $Product->image;
  	}
	elseif ($Product->thumbnail != '') {
          $image = $Product->thumbnail;
  	}
  	else {
  		$image = "NoPhoto.png";
  	}
    ?>


    <div class="content">
      <div class="left-col col-md-6">
        <div class="pic-block">
          <img src="/sites/default/files/products/large/<?php echo $image; ?>" alt="">
        </div>
        <?php
        if (strlen($Product->alert) > 0) {
        ?>
        <div class="red-message">
            <?php
            echo $Product->alert;
            ?>
        </div>
        <?php
        }
        ?>
      </div>
      <div class="right-col col-md-5">
        <div class="inner">
          <h1><?php echo $Product->name; ?></h1>
          <div class="text-block">
            <p><?php echo $Product->description; ?></p>


            <p>
			<?php
			if ($Product->pricelb > 0) {
			?>
				Price per lb. <?php echo om_money($Product->pricelb); ?> <br/>
				Estimated Cost Per Item: <?php echo om_money($Product->estimatedpriceperitem); ?>
			<?php
			} else {
			?>
				Price <?php echo om_money($Product->estimatedpriceperitem); ?> each
			<?php

				if ($Product->estimatedbulkpriceperitem > 0) {
			?>
			<span class="red-text">
			<br/>Bulk price <?php echo om_money($Product->estimatedbulkpriceperitem); ?> each<br/>
			<?php
				}
				if ($Product->bulkminquantity  > 1) {
				?>
				  Minimum Bulk Quantity: <?php echo $Product->bulkminquantity; ?>+
				  </span>
				  <?php
				}
			}
			?>
            </p>

            <p class="red-text">
            <?php
            if ($Product->bulkpricelb) {
            ?>
              Bulk Price per lb. <?php echo om_money($Product->bulkpricelb); ?> <br/>
              <?php
            }
              ?>
            <?php
            if ($Product->estimatedbulkpriceperitem && $Product->pricelb > 0) {
            ?>
              Bulk Estimated Cost Per Item: <?php echo om_money($Product->estimatedbulkpriceperitem); ?><br/>
              <?php
            }
			if ($Product->bulkminquantity  > 1 && $Product->pricelb > 0) {
			?>
			  Minimum Bulk Quantity: <?php echo $Product->bulkminquantity; ?>+
			  <?php
			}
			?>
            </p>
			<?php
			if ($Product->pricelb > 0) {
			?>
            <p>
              PLEASE NOTE: All of the costs are estimates only.
              Each item will be weighed when you pick up your order and
              you will be charged based on the exact amount based on the price per lb cost.
            </p>
			<?php
			}
			?>
          </div>
          <div class="opts">
            <a href="/order-day" class="button pull-right">Start a new order</a>
          </div>
        </div>
      </div>
    </div>

</div>

<?php include 'footer.php'; ?>


