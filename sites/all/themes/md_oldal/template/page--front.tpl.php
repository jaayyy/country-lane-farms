<div class="main-wrap">

<?php
    if ($page['slideshow']): ?>
    <div class="header-slider">
        <?php
        print render($page['slideshow']); ?>
    </div>
    <?php
    endif; ?>
    
 <!--   
    <div class="home-hero" style="background-image: url(/sites/all/themes/md_oldal/img/content/home-hero-bg-beef.jpg);">
        <div class="container">
            <a href="#" class="logo"></a>
            <div class="hero-nav nav">
                <div class="hero-nav-inner">
                    <a href="#">Log In / Sign Up</a> <a href="#" class="button">Start Order</a>
                </div>
            </div>

            <a href="#" class="hero-cta">
                <img src="/sites/all/themes/md_oldal/img/content/home-hero-beef-description.png" alt="">
            </a>
        </div>
    </div>
-->
<?php include 'header.php'; ?>

<div class="content">
    
    <a class="home-product chicken" href="/chicken/">
        <img src="/sites/all/themes/md_oldal/img/content/home-chicken.jpg" alt="">
        <div class="description right"><img src="/sites/all/themes/md_oldal/img/content/home-chicken-description.png" alt=""></div>
    </a>

    <a class="home-product beef-ribs" href="/beef/">
        <img src="/sites/all/themes/md_oldal/img/content/home-beef.jpg" alt="">
        <div class="description left"><img src="/sites/all/themes/md_oldal/img/content/home-alberta-beef-description.png" alt=""></div>
    </a>

    <a class="home-product salmon" href="/salmon/">
        <img src="/sites/all/themes/md_oldal/img/content/home-salmon.jpg" alt="">
        <div class="description right"><img src="/sites/all/themes/md_oldal/img/content/home-salmon-description.png" alt=""></div>
    </a>

    <a class="home-product lobster-scallops" href="/seafood/">
        <img src="/sites/all/themes/md_oldal/img/content/home-lobster-scallops.jpg" alt="">
        <div class="description left"><img src="/sites/all/themes/md_oldal/img/content/home-lobster-scallops-description.png" alt=""></div>
    </a>

    <a class="home-product honey" href="/honey/">
        <img src="/sites/all/themes/md_oldal/img/content/home-honey.jpg" alt="">
        <div class="description right"><img src="/sites/all/themes/md_oldal/img/content/home-honey-description.png" alt=""></div>
    </a>

</div>

<div class="home-bottom-cta">
    <div class="container">
        <div class="quote">
            &ldquo;Our goal is to provide our customers with a healthy product that tastes great and is reasonably priced.&rdquo;
        </div>
        <a href="/about" class="button regular">About Us</a>
    </div>
</div>

<?php
if ($page['content']): ?>
<?php
print render($page['content']); ?>
<?php
endif; ?>
</div>

<?php include 'footer.php'; ?>

