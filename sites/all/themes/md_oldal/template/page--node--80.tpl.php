<div class="main-wrap">

<?php include 'header.php';


global $user;
$Profile = om_customer_profile_load($user->uid);
if ($Profile["StatusCode"] == 1) {
    $Profile = $Profile["Data"];
} else {
    $Profile = false;
}

?>
<div class="content col-md-10 col-md-offset-1"> 
    <div class="register-form form">
        <h1>User Profile</h1>
        <p>
            Register with us to start your first order and receive newsletter updates on upcoming specials.
        </p>
        <form action="/process-profile" method="post">
            <input type="hidden" name="f[uid]" value="<?php echo $user->uid; ?>" />
            <div class="left-col col-md-4">            
                <div class="form-group">
                    <label for="first-name">First Name</label>
                    <input type="text" id="first-name" name="f[firstname]" value="<?php echo $Profile->firstname; ?>">
                </div>
                <div class="form-group">
                    <label for="last-name">Last Name</label>
                    <input type="text" id="last-name" name="f[lastname]" value="<?php echo $Profile->lastname; ?>" >
                </div>
                <div class="form-group">
                    <label for="email">Email Address</label>
                    <input type="email" id="email" name="f[email]" value="<?php echo $user->mail; ?>">
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="text" id="phone" name="f[phone]" value="<?php echo $Profile->phone; ?>">
                </div>
            </div>
            <div class="right-col col-md-8">
                <div class="fields row">
                    <div class="col-md-6 nopadding">
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" id="address" name="f[address]" value="<?php echo $Profile->address; ?>">
                        </div>
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" id="city" name="f[city]" value="<?php echo $Profile->city; ?>">
                        </div>
                        <div class="form-group">
                            <label for="postal-code">Postal Code</label>
                            <input type="text" id="postal-code" name="f[postalcode]" value="<?php echo $Profile->postalcode; ?>">
                        </div>
                    </div>
                </div>
                <div class="submit-buttons">
                    <input type="submit" value="Update Info" class="button" style="border: none;" />
                </div>
              </div>
        </form>
    </div>
</div>
<?php include 'footer.php'; ?>