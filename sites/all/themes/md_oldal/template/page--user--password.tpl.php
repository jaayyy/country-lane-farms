<div class="main-wrap">
    <?php
    require_once("header.php");
?>
<style type="text/css">
    .fp-input = {
        padding: 5px 7px;
        width: 100%;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        border: 2px solid #c5c5c5;        
        
    }
    section.line {
        padding: 0;
    }
    #user-pass--2 {
        margin-top: 0;
    }
</style>
<?php
    global $base_url;
    
    drupal_add_js($base_url."/".drupal_get_path('theme', 'md_oldal')."/js/jquery.forgot-password.js");
    ?>
    <div class="content col-sm-10 col-sm-offset-1">
        <div class="login-form form">
            <h1>Forgot Password</h1>
            <p>
                Please enter your email address in the form below to request a password reset.
            </p>
            <?php
            print $messages;
            print drupal_render(drupal_get_form('user_pass'));
            ?>
            <div class="container">
            <div class="col-sm-4">
                    <a href="/login">Return to Login Page</a>
            </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once("footer.php");
?>
