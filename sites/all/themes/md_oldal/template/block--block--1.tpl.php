<div id="<?php print $block_html_id; ?>" class="text <?php print $classes; ?> widget"<?php print $attributes; ?>>

	<?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
        <h3><span><?php print $block->subject;?></span></h3>
    <?php endif;?>
    <?php print render($title_suffix); ?>
       
    <?php print $content ;?>
</div>
