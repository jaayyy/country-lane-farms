<?php
if ((theme_get_setting('footer_options', 'md_oldal') == "style1") || !theme_get_setting('footer_options', 'md_oldal')): ?>
<footer class="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="footer-widget">
                    <h2>Products</h2>
                    <div class="widget-content">
                        <ul>
                            <li><a href="/chicken">Chicken</a></li>
                            <li><a href="/beef">Beef</a></li>
                            <li><a href="/salmon">Salmon</a></li>
                            <li><a href="/seafood">Seafood</a></li>
                            <li><a href="/honey">Honey</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="footer-widget">
                    <h2>We recommend</h2>
                    <div class="widget-content">
                        <ul>
                            <li><a target="_blank" href="http://jerry.myfreiburgstudy.com">Natural Solution to Lowering Cholesterol</a></li>
                            <li><a target="_blank" href="http://jerry.naturalsolutionswork.com">Natural Solutions Work</a></li>
                            <li><a target="_blank" href="http://jerry.momsmakemore.com">Moms working from Home</a></li>
                            <li><a target="_blank" href="http://jerry.dreambusinessbuilders.com">A Recession Proof Business</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="contact-box col-sm-4">
                <div class="footer-widget">
                    <h2>Contact</h2> 
                    <div class="widget-content">
                        <div class="icon-phone"><span>1.403.934.2755</span></div>
                        <div class="icon-email"><a href="mailto:info@countrylanefarms.com">info@countrylanefarms.com</a></div>
                        <div class="icon-facebook"><a href="https://www.facebook.com/CountryLaneFarms">facebook.com/CountryLaneFarms</a></div>
                        <?php
                        // $array = theme_get_setting('contact_info', 'md_oldal');
                        // if (!empty($array)):
                        //     foreach ($array as $key => $value) {
                        //         print '<address><i class="' . $value['icon']['icon'] . '"></i>' . $value['detail'] . '</address>';
                        //     }
                        //     endif;
                            ?>

                    </div>
                </div>
            </div>
                
                <div class="col-sm-2">
                    <img src="/sites/all/themes/md_oldal/img/bbb.png" alt="">
                    <div class="footer-info text-right">
                            <!-- <ul class="footer-social">
                            <?php
                            $array = theme_get_setting('ft_social', 'md_oldal');
                            if (!empty($array)):
                                foreach ($array as $key => $value) {
                                    $arr_parent_icon = $value['icon'];
                                    $arr_parent_link = $value['link'];
                                    print '<li><a href="' . $arr_parent_link . '"><i class="' . $arr_parent_icon['icon'] . '"></i></a></li>';
                                }
                                endif;
                                ?>
                            </ul> -->
                            <?php
                            print theme_get_setting('footer_text', 'md_oldal'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <?php
        else: ?>
        <footer class="simple-footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="row">
                            <?php
                            $array = theme_get_setting('contact_info_style_2', 'md_oldal');
                            if (!empty($array)):
                                $col_num = 12 / count($array);
                            foreach ($array as $key => $value) {
                                print '<div class="col-sm-' . $col_num . '"><div class="footer-group">
                                <div class="oldal-icon white-icon"><i class="' . $value['icon']['icon'] . '"></i></div>
                                <div class="content">' . $value['detail'] . '</div>
                            </div></div>';
                        }
                        endif;
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <?php
                            $array = theme_get_setting('ft_social', 'md_oldal');
                            if (!empty($array)):
                                foreach ($array as $key => $value) {
                                    $arr_parent_icon = $value['icon'];
                                    $arr_parent_link = $value['link'];
                                    print '<a href="' . $arr_parent_link . '"><div class="oldal-icon white-icon fill-icon sm-icon"><i class="' . $arr_parent_icon['icon'] . '"></i></div></a>';
                                }
                                endif;
                                ?>
                            </div>
                            <div class="col-sm-12 text-center">
                                <div class="copyright">
                                    <?php
                                    print strip_tags(theme_get_setting('footer_style_2_text', 'md_oldal'), '<a><i>'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <?php
        endif; ?>