<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Amatic+SC:400,700|Open+Sans:400italic,600italic,700italic,400,700,600' rel='stylesheet' type='text/css'>

<header id="fixed-header" class="navbar">
    <div class="col-md-10 col-md-offset-1">
        <div class="logo-box">
           <a href="/" class="logo sticky-show"></a>
        </div>

        <div class="menu-box">

                <?php
                if ($user->uid > 1) {
                        $BarCSS = " logged-in-user";
                }
                ?>
                <div class="login-order-wrap">
                    <div class="login-order<?php echo $BarCSS; ?>">
                            <?php
                            global $user;

                            if ($user->uid < 2) {
                            ?>
                        <a href="/login" class="sticky-show">Log In / Sign Up</a>
                        <a href="/order-day" class="startorderbtn button sticky-show">Start Order</a>

                        <?php
                        } else {
                            $Customer = om_customer_profile_load($user->uid);
                            if ($Customer["StatusCode"] == 1) {
                                    $Customer = $Customer["Data"];
                            }
                          ?>
                        <ul class="nav navbar-nav user-dropdown">
                            <li id="user-menu" class="dropdown sticky-show">
                                <a id="user-options-dropdown" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                    <?php echo $Customer->firstname." ".$Customer->lastname; ?>
                                    <span class="caret"></span>
                                </a>
                              <ul class="dropdown-menu" role="menu" aria-labelledby="user-options-dropdown">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="/user-account-profile">Account Info</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="/user/logout" class="sticky-show">Log Out</a></li>

                              </ul>
                            </li>
                        </ul>
                        <a href="/order-day" class="startorderbtn button sticky-show">Start Order</a>
                        <?php } ?>
                    </div>
                </div>
        </div>
    </div>
</header>

<header id="header" class="navbar navbar-inverse normal-color">

        <div class="col-md-10 col-md-offset-1">
            <div class="navbar-header">

                <!-- Desktop header -->
                <div class="desktop-header hidden-xs">
                    <div class="logo-box">
                       <a href="/" class="logo sticky-show"></a>
                    </div>
                    <div class="menu-box">

                            <?php
                            if ($user->uid > 1) {
                                    $BarCSS = " logged-in-user";
                            }
                            ?>
                            <div class="login-order-wrap">
                                <div class="login-order<?php echo $BarCSS; ?>">
                                        <?php
                                        global $user;

                                        if ($user->uid < 2) {
                                        ?>
                                    <a href="/login" class="sticky-show">Log In / Sign Up</a>
                                    <a href="/order-day" class="button sticky-show">Start Order</a>

                                    <?php
                                    } else {
                                        $Customer = om_customer_profile_load($user->uid);
                                        if ($Customer["StatusCode"] == 1) {
                                                $Customer = $Customer["Data"];
                                        }
                                      ?>
                                    <ul class="nav navbar-nav user-dropdown">
                                        <li id="user-menu" class="dropdown sticky-show">
                                            <a id="user-options-dropdown" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                                <?php echo $Customer->firstname." ".$Customer->lastname; ?>
                                                <span class="caret"></span>
                                            </a>
                                          <ul class="dropdown-menu" role="menu" aria-labelledby="user-options-dropdown">
                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/user-account-profile">Account Info</a></li>
                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="/user/logout" class="sticky-show">Log Out</a></li>

                                          </ul>
                                        </li>
                                    </ul>
                                    <a href="/order-day" class="startorderbtn button sticky-show">Start Order</a>
                                    <?php } ?>
                                </div>
                            </div>


                            <!-- START NAVIGATION MENU ITEMS -->
                            <div class="menu-wrap">
                                <ul class="menu clearfix">
                                    <li class="first">
                                        <a href="<?php echo base_url(); ?>" class="active">Home</a>
                                    </li>
                                    <li><a href="/about">About</a></li>
                                    <li><a href="#" class="has-subnav">Products</a></li>
                                    <li><a href="/how-to-order">How to Order</a></li>
                                    <li><a href="/contact">Contact</a></li>
                                </ul>
                            </div>
                    </div>
                </div>

                <!-- Mobile header -->
                <div class="mobile-header visible-xs">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".menu-box" aria-expanded="false">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo-box">
                       <a href="/" class="logo sticky-show"></a>
                    </div>
                    <div class="menu-box collapse">

                        <?php if ($user->uid > 1) { $BarCSS = " logged-in-user"; } ?>

                        <div class="login-order<?php echo $BarCSS; ?>">
                            <?php
                                global $user;
                                if ($user->uid < 2) {
                            ?>
                            <a href="/login" class="sticky-show">Log In / Sign Up</a>
                            <a href="/order-day" class="button sticky-show">Start Order</a>

                            <?php
                            } else {
                                $Customer = om_customer_profile_load($user->uid);
                                if ($Customer["StatusCode"] == 1) {
                                    $Customer = $Customer["Data"];
                                }
                            ?>
                                <ul>
                                    <li><?php echo $Customer->firstname." ".$Customer->lastname; ?></li>
                                    <li><a href="/user-account-profile">Account Info</a></li>
                                    <li><a href="/user/logout" class="sticky-show">Log Out</a></li>
                                    <li><a href="/order-day" class="startorderbtn button sticky-show">Start Order</a></li>
                                </ul>

                            <?php } ?>
                        </div>

                        <!-- START NAVIGATION MENU ITEMS -->
                        <div class="menu-wrap">
                            <ul class="menu clearfix">
                                <li class="first">
                                    <a href="<?php echo base_url(); ?>" class="active">Home</a>
                                </li>
                                <li><a href="/about">About</a></li>
                                <li>
                                    <a href="#" class="mobile-has-subnav" data-toggle="collapse" data-target=".mobile-subnav" aria-expanded="false">Products</a>
                                    <div class="mobile-subnav collapse">
                                        <a class="navheader" href="/chicken/"><h2>Chicken</h2></a>
                                        <ul>
                                            <?php
                                            //chicken
                                            $Products = om_get_products_by_category_nav(3);
                                            //var_dump($Products);
                                            if ($Products["StatusCode"] == 1) {
                                                $Products = $Products["Data"];
                                                foreach ($Products as $key=>$value) {
                                                     foreach($value as $t=>$d) {
                                                            if ($t == 'url')
                                                                $url = $d;
                                                            if ($t == 'name')
                                                                $name = $d;
                                                        //echo $value['url'];
                                                     }
                                                     echo '<li><a href="'.$url.'">'.$name.'</a></li>';
                                                }
                                            }
                                            ?>
                                        </ul>
                                        <a class="navheader" href="/turkey/"><h2>Turkey</h2></a>
                                        <ul>
                                            <?php
                                            //turkey
                                            $Products = om_get_products_by_category_nav(7);
                                            if ($Products["StatusCode"] == 1) {
                                                $Products = $Products["Data"];

                                                //var_dump($Products);
                                                foreach ($Products as $key=>$value) {
                                                     foreach($value as $t=>$d) {
                                                            if ($t == 'url')
                                                                $url = $d;
                                                            if ($t == 'name')
                                                                $name = $d;
                                                        //echo $value['url'];
                                                     }
                                                     echo '<li><a href="'.$url.'">'.$name.'</a></li>';
                                                }
                                            }

                                            ?>
                                        </ul>
                                        <a class="navheader" href="/beef/"><h2>Beef</h2></a>
                                        <ul>
                                            <?php
                                            //beef
                                            $Products = om_get_products_by_category_nav(2);
                                            if ($Products["StatusCode"] == 1) {
                                                $Products = $Products["Data"];
                                                foreach ($Products as $key=>$value) {
                                                     foreach($value as $t=>$d) {
                                                            if ($t == 'url')
                                                                $url = $d;
                                                            if ($t == 'name')
                                                                $name = $d;
                                                        //echo $value['url'];
                                                     }
                                                     echo '<li><a href="'.$url.'">'.$name.'</a></li>';
                                                }
                                            }
                                            ?>
                                        </ul>
                                        <a class="navheader" href="/honey/"><h2>Honey</h2></a>
                                        <ul>
                                            <?php
                                            //honey
                                            $Products = om_get_products_by_category_nav(8);
                                            if ($Products["StatusCode"] == 1) {
                                                $Products = $Products["Data"];
                                                //var_dump($Products);
                                                foreach ($Products as $key=>$value) {
                                                     foreach($value as $t=>$d) {
                                                            if ($t == 'url')
                                                                $url = $d;
                                                            if ($t == 'name')
                                                                $name = $d;
                                                        //echo $value['url'];
                                                     }
                                                     echo '<li><a href="'.$url.'">'.$name.'</a></li>';
                                                }
                                            }

                                            ?>
                                        </ul>
                                        <a class="navheader" href="/seafood/"><h2>Seafood</h2></a>
                                        <ul>
                                            <?php
                                            //seafood
                                            $Products = om_get_products_by_category_nav(15);
                                            if ($Products["StatusCode"] == 1) {
                                                $Products = $Products["Data"];
                                                //var_dump($Products);
                                                foreach ($Products as $key=>$value) {
                                                     foreach($value as $t=>$d) {
                                                            if ($t == 'url')
                                                                $url = $d;
                                                            if ($t == 'name')
                                                                $name = $d;
                                                        //echo $value['url'];
                                                     }
                                                     echo '<li><a href="'.$url.'">'.$name.'</a></li>';
                                                }
                                            }
                                            ?>
                                        </ul>
                                        <a class="navheader" href="/salmon/"><h2>Salmon</h2></a>
                                        <ul>
                                            <?php
                                            //honey
                                            $Products = om_get_products_by_category_nav(5);
                                            if ($Products["StatusCode"] == 1) {
                                                $Products = $Products["Data"];
                                                //var_dump($Products);
                                                foreach ($Products as $key=>$value) {
                                                     foreach($value as $t=>$d) {
                                                            if ($t == 'url')
                                                                $url = $d;
                                                            if ($t == 'name')
                                                                $name = $d;
                                                        //echo $value['url'];
                                                     }
                                                     echo '<li><a href="'.$url.'">'.$name.'</a></li>';
                                                }
                                            }

                                            ?>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="/how-to-order">How to Order</a></li>
                                <li><a href="/contact">Contact</a></li>
                            </ul>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <?php
   /*$Product = om_get_product_by_id($ProductID);
        if ($Product["StatusCode"] == 1) {
            $Product = $Product["Data"];
        }
    *
    * *$Products = om_get_products_by_category(3)
    *
*/
?>
        <!-- PRODUCTS MEGA MENU -->
        <div class="subnav">
            <div class="inner">
                <div class="col-md-6">
                    <div class="col-md-6">
                        <a class="navheader" href="/chicken/"><h2>Chicken</h2></a>
                        <ul>
                            <?php
                            //chicken
                            $Products = om_get_products_by_category_nav(3);
							//var_dump($Products);
                            if ($Products["StatusCode"] == 1) {
                                $Products = $Products["Data"];
                                foreach ($Products as $key=>$value) {
                                     foreach($value as $t=>$d) {
                                            if ($t == 'url')
                                                $url = $d;
                                            if ($t == 'name')
                                                $name = $d;
                                        //echo $value['url'];
                                     }
                                     echo '<li><a href="'.$url.'">'.$name.'</a></li>';
                                }
                            }
                            ?>
                        </ul>
                        <a class="navheader" href="/turkey/"><h2>Turkey</h2></a>
                        <ul>
                            <?php
                            //turkey
                            $Products = om_get_products_by_category_nav(7);
                            if ($Products["StatusCode"] == 1) {
                                $Products = $Products["Data"];

                                //var_dump($Products);
                                foreach ($Products as $key=>$value) {
                                     foreach($value as $t=>$d) {
                                            if ($t == 'url')
                                                $url = $d;
                                            if ($t == 'name')
                                                $name = $d;
                                        //echo $value['url'];
                                     }
                                     echo '<li><a href="'.$url.'">'.$name.'</a></li>';
                                }
                            }

                            ?>
                            <!--<li><a href="/product/?p=">Frozen</a></li>
                            <li><a href="/product/?p=">Fresh 15-17 lb</a></li>
                            <li><a href="/product/?p=">Fresh 18-20 lb</a></li>
                            <li><a href="/product/?p=">Fresh 21-22 lb</a></li>
                            <li><a href="/product/?p=">Fresh 23 lb plus</a></li>-->
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <a class="navheader" href="/beef/"><h2>Beef</h2></a>
                        <ul>
                            <?php
                            //beef
                            $Products = om_get_products_by_category_nav(2);
                            if ($Products["StatusCode"] == 1) {
                                $Products = $Products["Data"];
                                foreach ($Products as $key=>$value) {
                                     foreach($value as $t=>$d) {
                                            if ($t == 'url')
                                                $url = $d;
                                            if ($t == 'name')
                                                $name = $d;
                                        //echo $value['url'];
                                     }
                                     echo '<li><a href="'.$url.'">'.$name.'</a></li>';
                                }
                            }
                            ?>
                        </ul>
                        <a class="navheader" href="/honey/"><h2>Honey</h2></a>
                        <ul>
                            <?php
                            //honey
                            $Products = om_get_products_by_category_nav(8);
                            if ($Products["StatusCode"] == 1) {
                                $Products = $Products["Data"];
                                //var_dump($Products);
                                foreach ($Products as $key=>$value) {
                                     foreach($value as $t=>$d) {
                                            if ($t == 'url')
                                                $url = $d;
                                            if ($t == 'name')
                                                $name = $d;
                                        //echo $value['url'];
                                     }
                                     echo '<li><a href="'.$url.'">'.$name.'</a></li>';
                                }
                            }

                            ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="pic col-md-12">
                        <a href="/chicken"><img src="/sites/all/themes/md_oldal/images/FEATURED-CHICKEN.jpg" alt=""></a>
                    </div>
                    <div class="col-md-6">
                        <a class="navheader" href="/seafood/"><h2>Seafood</h2></a>
                        <ul>
                            <?php
                            //seafood
                            $Products = om_get_products_by_category_nav(15);
                            if ($Products["StatusCode"] == 1) {
                                $Products = $Products["Data"];
                                //var_dump($Products);
                                foreach ($Products as $key=>$value) {
                                     foreach($value as $t=>$d) {
                                            if ($t == 'url')
                                                $url = $d;
                                            if ($t == 'name')
                                                $name = $d;
                                        //echo $value['url'];
                                     }
                                     echo '<li><a href="'.$url.'">'.$name.'</a></li>';
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <a class="navheader" href="/salmon/"><h2>Salmon</h2></a>
                        <ul>
                            <?php
                            //honey
                            $Products = om_get_products_by_category_nav(5);
                            if ($Products["StatusCode"] == 1) {
                                $Products = $Products["Data"];
                                //var_dump($Products);
                                foreach ($Products as $key=>$value) {
                                     foreach($value as $t=>$d) {
                                            if ($t == 'url')
                                                $url = $d;
                                            if ($t == 'name')
                                                $name = $d;
                                        //echo $value['url'];
                                     }
                                     echo '<li><a href="'.$url.'">'.$name.'</a></li>';
                                }
                            }

                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    <?php
    if ($page['search']): ?>
    <div class="search-wrap">
      <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php
                print render($page['search']); ?>
            </div>
        </div>
    </div>
</div>
<?php
endif; ?>
</header>

<div class="header-spacer"></div>