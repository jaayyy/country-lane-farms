<section class="line">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="<?php print $block_html_id; ?>" class="similar-slider <?php print $classes; ?>"<?php print $attributes; ?>>
                
                    <?php print render($title_prefix); ?>
                    <?php if ($block->subject): ?>
                        <h2><?php print $block->subject;?></h2>
                    <?php endif;?>
                    <?php print render($title_suffix); ?>
                       
                    <?php print $content ;?>
                </div>
             </div>
        </div>
	</div>
</section>