<div class="main-wrap" style="background: url(/sites/all/themes/md_oldal/img/bg-wood.jpg);">

    <?php include 'header.php'; ?>

    

    <div class="page-banner row">
      <img src="/sites/all/themes/md_oldal/img/content/about-banner.jpg" alt="">
    </div>
    <div class="about-content-wrap">
    <div class="content col-md-10 col-md-offset-1"> 
        <div class="quote col-md-10 col-md-offset-1">
          &laquo;Our goal is to provide our customers with a healthy product that tastes great and is reasonably priced&raquo;
        </div>
        <div class="section">
          <h1>About Country Lane Farms</h1>
          <p>
            We, as a family, have been raising chickens since 1984. Jerry grew up in the poultry industry and his father 
            at one point owned the largest poultry farm in Alberta. Jerry had the opportunity to take over this farm when 
            he was 18 years of age but being a typical teenager, decided chicken farming was not for him. Eight years later, 
            along with some maturity and a wife, we bought what is now Country Lane Farms. We are located just 20 minutes 
            east of the city, so we are your true local supplier!
          </p>
          <p>
            We started off with a typical commercial farm with 40,000 broilers and 10,000 laying hens. 
            It was during this time that we first started researching whole wheat feeding and eliminating the use of 
            antibiotics. In 1999 we sold that farm and moved to our present site which is only 2 miles north of our 
            old farm. We kept 2300 units of quota, built a very unique barn that we designed ourselves and began 
            selling all antibiotic free chickens directly to the end consumer.
          </p>
          <p>
            Our business grew with the addition of naturally-raised beef, salmon and seafood to our product line. 
            All the products we sell are raised naturally and of the highest quality.
          </p>
        </div>

        <div class="about-things section">

          <h1>A few things that make us different</h1>

          <div class="row">
            <div class="col-sm-3">
              <!-- <div class="circle black">
                <div class="inner">
                  <p>Antibiotic Free</p>
                  <p>No Hormones</p>
                  <p>No Chemicals</p>
                </div>
              </div> -->
              <div class="thing-pic">
                <img src="/sites/all/themes/md_oldal/img/about-thing-1.png">
              </div>
            </div>
            <div class="col-sm-9">
              <div class="text">
                <div class="inner" style="padding-top: 60px;">
                  <h2>We believe in real food, year round</h2>
                  <p>We raise our chickens in a barn without the use of antibiotics, hormones or animal by-products. We also do not use any chemicals in the birds environment. We don't use any chemicals in our home so why would we want to use them in the barn. We extend this philosophy to all the products we sell. Ensuring that all of our fish and beef products are free of any and all antibiotics, hormones and animal by-products.</p>
                </div>
              </div>
            </div>
          </div>

          <div class="row">            
            <div class="col-sm-8">
              <div class="text">
                <div class="inner" style="padding-top: 30px;">
                  <h2>Quality Feed for all our animals</h2>
                  <p>
                    Years of research and consultation with the nutritionist we have been able to come up with a 
                    feeding program that is far superior than what any of our competitors use. We use high-quality 
                    vitamins and organic minerals to balance the Ration for the animals so the animals get what they 
                    need to maintain optimum health and growth.
                  </p>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="thing-pic">
                <img src="/sites/all/themes/md_oldal/img/about-thing-2.png">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-3">
              <div class="thing-pic">
                <img src="/sites/all/themes/md_oldal/img/about-thing-3.png">
              </div>
            </div>
            <div class="col-sm-9">
              <div class="text">
                <div class="inner">
                  <h2>Great food affordable &amp; accessible</h2>
                  <p>
                    Due to the relationship between Country Lane Farms and our customers we have a sustainable and 
                    viable family farming operation. Each and every time that our customers pick up their order they 
                    are dealing directly with the farmer.  Having pick up locations around the city allows us not to have 
                    storefronts that are expensive and require employees.
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div class="row">            
            <div class="col-sm-9">
              <div class="text">
                <div class="inner">
                  <h2>Please feel free to visit</h2>
                  <p>
                    We have an open door policy year round. WE can answer any questions you may have and you can see the 
                    daily operations. Visit the farm in the summer and bring the family. We have a bouncy castle for 
                    the little ones to play on as well as horses, cats and dogs.
                  </p>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="thing-pic">
                <img src="/sites/all/themes/md_oldal/img/about-thing-4.png">
              </div>
            </div>
          </div>

        </div>

    </div>
  </div>

</div>

<?php include 'footer.php'; ?>


