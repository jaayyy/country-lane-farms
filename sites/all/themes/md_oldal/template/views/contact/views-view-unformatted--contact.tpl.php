<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<section class="line">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-4 col-xs-12">
                    <div class="title"><?php print $view->human_name;?></div>
                </div>
                <div class="col-sm-8 col-xs-12">
                    <div class="title-content">
                        <?php
							$array = theme_get_setting('contact_info','md_oldal') ;
							if(!empty($array)) :
								foreach($array as $key => $value) {
									print '<address><i class="'.$value['icon']['icon'].'"></i>'.$value['detail'].'</address>'; 
								}
							endif;
						?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="line contact-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-4">
                </div>
                <div class="col-sm-8">
                	<?php foreach ($rows as $id => $row): ?>
						<?php print $row; ?>
                    <?php endforeach; ?>
                </div>
        	</div>
        </div>
     </div>
</section>