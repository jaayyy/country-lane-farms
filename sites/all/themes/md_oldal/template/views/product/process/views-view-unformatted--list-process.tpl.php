<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<section class="line grey">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Our Process</h1>
            </div>
		    <?php foreach ($rows as $id => $row): ?>
            	<?php print $row; ?>
            <?php endforeach; ?>
        </div>
    </div>
</section>