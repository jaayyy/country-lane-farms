<?php
/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>

<?php
  global $base_url;
  $term = FALSE;
  if ($id = arg(1)){
    $term = taxonomy_term_load($id);
  }
?>


<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <section class="line">
    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <aside class="categories">
            <h3><?php print t('Categories'); ?></h3>
            <?php print views_embed_view('product_category', 'block'); ?>
          </aside>
        </div>

        <div class="col-sm-9">
          <div class="product-list">
            <div class="col-sm-12">
              <div class="no-items">
                <?php if ($header): ?>
                  <strong><?php print $header;?></strong>
                  <?php print $term ? " items in {$term->name}" : "items"; ?>
                <?php endif; ?>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="sort">
                <div class="text"><?php print t('Sort items by'); ?></div>
                <?php if ($exposed): ?>
                  <div class="view-filters">
                    <?php print $exposed; ?>
                  </div>
                <?php endif; ?>
              </div>
              <?php if ($pager): ?>
                <?php print $pager; ?>
              <?php endif; ?>
            </div>
            <?php if ($rows): ?>
              <?php print $rows; ?>
            <?php elseif ($empty): ?>
              <div class="view-empty">
                <?php print $empty; ?>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>