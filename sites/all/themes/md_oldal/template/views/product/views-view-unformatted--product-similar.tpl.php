<?php $_rows = array_chunk($rows, 4); ?>
<div class="slider-controls">
    <button class="left"><i class="entyp-left-open-1"></i></button>
    <button class="right"><i class="entyp-right-open-1"></i></button>
</div>
<ul class="slides">
    <?php foreach ($_rows as $__rows): ?>
          <?php foreach ($__rows as $id => $row): ?>
            <!--render row-->
            <?php print '<li><div class="row">'.$row.'</div></li>'; ?>
          <?php endforeach; ?>
    <?php endforeach; ?>
</ul>