<div class="col-sm-3">
    <div class="similar-product">
        <figure>
            <a href="<?php print $fields['path']->content; ?>"><img src="<?php print file_create_url($fields['field_images']->content); ?>" alt="img"/></a>
        </figure>
        <div class="name"><?php print $fields['title']->content; ?></div>
        <div class="price"><?php print $fields['commerce_price']->content; ?></div>
    </div>
</div>