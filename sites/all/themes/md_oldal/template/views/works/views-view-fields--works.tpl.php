<?php 
$Title = $fields['title']->content;
$Subtitle = $fields['field_works_subtitle']->content;
$Description = $fields['field_works_description']->content;
$Categories = $fields['field_works_categories']->content;
$Image = $fields['field_works_image']->content;
$Link = $fields['field_works_link']->content;
$Path = $fields['path']->content;
?>

<div class="col-sm-4">
    <figure>
        <a href="<?php print $Path; ?>"><img src="<?php print file_create_url($Image) ?>" alt="img"/></a>
        <div class="overlay">
            <a href="<?php print file_create_url($Image) ?>" class="trigger-fancybox" title="<?php print $Title; ?>">
                <i class="entyp-picture-1"></i>
                <!--<i class="entyp-resize-full-1"></i>-->
            </a>
        </div>
  </figure>
    <h4><strong><a href="<?php print $Path; ?>"><?php print $Title; ?></a></strong></h4>
    <a class="category" href="#"><?php print $Subtitle; ?></a>
</div>