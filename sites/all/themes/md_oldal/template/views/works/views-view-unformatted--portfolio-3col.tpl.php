<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php
$voca = taxonomy_vocabulary_machine_name_load('works');
$terms = taxonomy_term_load_multiple(array(), array('vid' => $voca->vid));
?>
<section class="line top">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="title">
                        <?php print $view->human_name;?>
                        <ul class="current-page">
                            <li><a href="<?php print base_path(); ?>"><?php print t('Home'); ?></a></li>
                            <li>&gt;</li>
                            <li><?php print $view->human_name;?></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <div class="title-content">
                        <?php
						  $summary = variable_get("view_{$view->name}_summary", array('value' => '', 'format' => 'full_html'));
						  $view_summary = check_markup($summary['value'], $summary['format']);
						  print $view_summary;
						?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="line">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="btn-group default-group isotopeFilters">
          <button type="button" class="btn btn-default active" data-filter="*"><?php print t('Everything'); ?></button>
          <?php foreach ($terms as $id => $term): ?>
            <button type="button" class="btn btn-default" data-filter=".<?php print str_replace(' ', '-', strtolower($term->name)); ?>"><?php print $term->name; ?></button>
          <?php endforeach; ?>
        </div>
      </div>
      <div class="isotopeContainer">
        <?php foreach ($rows as $id => $row): ?>
          <?php print $row; ?>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</section>
