<?php
$count = 0;
?>
<div class="recent-work">
    <div class="slider-controls">
        <button class="left"><i class="entyp-left-open-1"></i></button>
        <button class="right"><i class="entyp-right-open-1"></i></button>
    </div>
    <ul class="slides">
        <?php foreach ($rows as $id => $row): ?>
            <?php
                if($count%3 == 0 ) {
                 print '<li><div class="row">' ;
                 for($j=$count ; $j<$count+3;$j++ ) {
                    print $rows[$j];
                 }
                 print '</div></li>' ;
                }
                $count++ ;
            ?>
        <?php endforeach; ?>
    </ul>
</div>