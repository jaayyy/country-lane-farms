<section class="line">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="col-sm-4 col-xs-12">
          <div class="title"><?php print $view->human_name;?></div>
        </div>
        <div class="col-sm-8 col-xs-12">
          <?php
          $summary = variable_get("view_{$view->name}_summary", array('value' => '', 'format' => 'full_html'));
          $view_summary = check_markup($summary['value'], $summary['format']);
          print $view_summary;
          ?>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="line pt-0">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php if ($rows): ?>
          <?php print $rows; ?>
        <?php elseif ($empty): ?>
          <div class="view-empty">
            <?php print $empty; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>