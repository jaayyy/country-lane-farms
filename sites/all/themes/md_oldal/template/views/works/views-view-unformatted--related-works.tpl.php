<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$count = 0;
?>
<section class="line">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="col-sm-4 col-xs-12">
          <div class="title"><?php print $view->human_name;?></div>
        </div>
        <div class="col-sm-8 col-xs-12">
          <?php
          $summary = variable_get("view_{$view->name}_summary", array('value' => '', 'format' => 'full_html'));
          $view_summary = check_markup($summary['value'], $summary['format']);
          print $view_summary;
          ?>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="line pt-0">
  <div class="container">
    <div class="recent-work">
      <div class="slider-controls">
        <button class="left"><i class="entyp-left-open-1"></i></button>
        <button class="right"><i class="entyp-right-open-1"></i></button>
      </div>
      <ul class="slides">
        <?php foreach ($rows as $id => $row): ?>
          <?php
          if ($count % 3 == 0) {
            print '<li><div class="row">';
            for ($j = $count; $j < $count + 3; $j++) {
              print $rows[$j];
            }
            print '</div></li>';
          }
          $count++;
          ?>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</section>
