<?php
$Title = $fields['line_item_title']->content;
$Price = $fields['commerce_unit_price']->content;
$Quantity = $fields['edit_quantity']->content;
$Remove = $fields['edit_delete']->content;
$Total = $fields['commerce_total']->content;
?>

<div class="col-sm-4">
    <div class="product-name"><span class="label"><?php print t('Product name'); ?>: </span><?php print $Title; ?></div>
</div>
<div class="col-sm-2">
    <div class="product-price"><span class="label"><?php print t('Price'); ?>: </span><?php print $Price; ?></div>
</div>
<div class="col-sm-2">
    <div class="product-quantity"><span class="label"><?php print t('Quantity'); ?>: </span><?php print $Quantity; ?></div>
</div>
<div class="col-sm-2">
    <div class="product-remove"><?php print $Remove; ?></div>
</div>
<div class="col-sm-2">
    <div class="product-total"><span class="label"><?php print t('Total'); ?>: </span><?php print $Total; ?></div>
</div>