<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<section class="line">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="products">
          <div class="cart-header-row">
                <div class="col-sm-4">
                    <h3 class="commerce-title"><?php print t('Product'); ?></h3>
                </div>
                <div class="col-sm-2">
                    <h3 class="commerce-title"><?php print t('Price'); ?></h3>
                </div>
                <div class="col-sm-2">
                    <h3 class="commerce-title"><?php print t('Quantity'); ?></h3>
                </div>
                <div class="col-sm-2">
                    <h3 class="commerce-title"><?php print t('Remove'); ?></h3>
                </div>
                <div class="col-sm-2">
                    <h3 class="commerce-title"><?php print t('Total'); ?></h3>
                </div>
          </div>
		  <?php foreach ($rows as $id => $row): ?>
            <div class="cart-row"><?php print $row; ?></div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</section>
