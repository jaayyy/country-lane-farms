<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<section class="line">
  <div class="container">
    <div class="testimonials">
      <div class="row">
        <div class="col-sm-12 text-center">
            <?php foreach ($rows as $id => $row): ?>
              <?php print $row; ?>
            <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</section>