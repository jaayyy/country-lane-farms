<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<section class="posts">
  <div class="container">
    <div class="row">
      <div class="col-sm-9">
        <?php foreach ($rows as $id => $row): ?>
          <?php print $row; ?>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</section>
