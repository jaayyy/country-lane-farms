<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php
$summary = variable_get("view_{$view->name}_summary", array('value' => '', 'format' => 'full_html'));
$view_summary = check_markup($summary['value'], $summary['format']);
?>
<section class="line top">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="col-md-4 col-sm-5 col-xs-12">
          <div class="title">
            <?php print $view->human_name;?>
            <ul class="current-page">
                <li><a href="<?php print base_path(); ?>"><?php print t('Home'); ?></a></li>
                <li>&gt;</li>
                <li><?php print $view->human_name;?></li>
            </ul>
          </div>
        </div>
        <div class="col-md-8 col-sm-7 col-xs-12">
          <div class="title-content"><?php print $view_summary; ?></div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="posts">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php foreach ($rows as $id => $row): ?>
          <?php print $row; ?>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</section>