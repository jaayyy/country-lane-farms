<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="col-sm-3">
  <div class="box">
    <header>
      <div class="title sm-title">
        <?php if (!empty($title)): ?>
          <?php print $title; ?>
        <?php endif; ?>
      </div>
    </header>
    <div class="content text-editor">
      <ul class="unstyled">
        <?php foreach ($rows as $id => $row): ?>
          <li>— <?php print $row; ?></li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</div>