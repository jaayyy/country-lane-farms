<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<section class="line colored">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <ul class="clients">
                    <?php foreach ($rows as $id => $row): ?>
						<?php print $row; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</section>