<div class="main-wrap">

    <?php include 'header.php'; ?>

    

    <div class="content col-sm-10 col-sm-offset-1"> 
        <div class="login-form form">
          <h1>You must be logged in to start your order</h1>
          <p>
            Your account will allow you to select a preferred pickup location, as well as keep track of your previously ordered items in the "favourites" section on your order form. Please log in below or register a new account to start your order.
          </p>

          <form action="/user-login" id="user-login" accept-charset="UTF-8" method="post">
            <div class="left-col col-sm-6">  
              <h1>Customer log in</h1>
              <div class="form-group">
                <label for="email">Email Address</label>
                <input type="text" id="email" name="name">
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" id="password" name="pass">
              </div>
              <div class="form-group">
                <div><a href="/user/password/?destination=/login">Forgot your password?</a></div>
                <input type="submit" form="user-login" value="Sign In" class="button">
              </div>
            </div>

            <div class="right-col col-sm-6">
              <h1>New Customers</h1>
              <p>Register with us to start your first order and receive newsletter updates on upcoming specials.</p>
              <a href="/register" class="button">Register Now</a>
            </div>

          </form>

          

        </div>
    </div>

</div>

<?php include 'footer.php'; ?>


