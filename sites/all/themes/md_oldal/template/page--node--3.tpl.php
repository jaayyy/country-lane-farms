<div class="main-wrap">
    <?php
    include 'header.php';
    $CategoryID = 3;
    if ($CategoryID > 0) {
        $Category = om_get_product_category($CategoryID);
        if ($Category["StatusCode"] == 1) {
            $Category = $Category["Data"];
        } else {
            $Category = false;
        }
    }
    ?>
    <div class="content" style="background-image: url(/sites/all/themes/md_oldal/img/bg-wood.jpg);">    
      <div class="top-block row">
        <div class="col-md-10 col-md-offset-1">
          <h1>Our <?php echo $Category->name; ?></h1>
          <div class="text-block">
            <p class="uppercased"><?php echo $Category->summary; ?></p>

            <p><?php echo $Category->description; ?></p>

          </div>
          <?php
          if (strlen($Category->alert) > 0) {
          ?>
          <div class="red-message full-width">
                <?php echo $Category->alert; ?>
          </div>
          <?php
          }
          ?>
        </div>        
      </div>
      <?php
      $Products = om_get_products_by_category($CategoryID);
      if ($Products["StatusCode"] == 1) {
        $Products = $Products["Data"];
      } else {
        $Products = false;
      }
      ?>
      <div class="row products-list two-col">
        <?php
        foreach($Products as $p) {
        ?>
        <div class="products-list-item col-md-6">
          <div class="pic-block col-sm-6">
            <img src="/sites/all/themes/md_oldal/img/content/chicken-pic.png" alt="">
          </div>
          <div class="text col-sm-6">
            <h2><?php echo $p->name; ?></h2>
            <?php
            if ($p->pricelb > 0) {
            ?>
            <div>Price per lb $<?php echo number_format($p->pricelb, 2, ".", ","); ?></div>
            <?php
            }
            ?>
            <?php
            if ($p->price > 0) {
            ?>            
            <div>Estimated Cost Per Item: $<?php echo number_format($p->price, 2, ".", ","); ?></div>
            <?php
            }
            ?>            
            <?php
            if ($p->bulkpricelb  > 0) {
            ?>            
            <div>Bulk Price per lb $<?php echo number_format($p->bulkpricelb, 2, ".", ","); ?></div>
            <?php
            }
            ?>            
            <?php
            if ($p->bulkpriceperitem  > 0) {
            ?>            
            <div>Estimated Bulk Cost</div>
            <div>Per Item: $<?php echo number_format($p->bulkpriceperitem, 2, ".", ","); ?></div>
            <?php
            }
            ?>            
            <?php
            if ($p->bulkminquantity > 1) {
            ?>            
            <div>Minimum Bulk Quantity: <?php echo $p->bulkminquantity; ?>+</div>
            <?php
            }
            ?>            
            <a href="/order-day" class="arrow-btn">Place order</a>
            
          </div>          
        </div>

<?php
        }
?>
      </div>
</div>

<?php include 'footer.php'; ?>



