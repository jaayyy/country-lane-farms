<?php 
  $pane = $variables['elements']['#fieldable_panels_pane'];
  $type = isset($pane->field_type['und'][0]['value']) && $pane->field_type['und'][0]['value'];
  $color = isset($pane->field_pane_color['und'][0]['rgb']) ? $pane->field_pane_color['und'][0]['rgb'] : '#ededed';
  $image_url = isset($pane->field_image['und'][0]['uri']) ? $pane->field_image['und'][0]['uri'] : false;
  $bg_image_url = isset($pane->field_background_image['und'][0]['uri']) ? $pane->field_background_image['und'][0]['uri'] : false;
?>

<?php if($type === true) :?> 
  <?php if(!$image_url) : ?>
	  <?php
        if($bg_image_url) {
            print '<section class="statement" style="background-image: url('.file_create_url($bg_image_url).')">';	
        }else {
            print '<section class="statement" style="background-color: '.$color.'">';
        }
      ?>
        <?php if($pane->title) : ?><h2><?php print $pane->title; ?></h2><?php endif; ?>
        <div class="content"><?php print render($content['field_description']);?></div>
      </section>
  <?php else: ?>
  	  <section class="line">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="presentation">
                            <div class="col-sm-5">
                                <figure>
                                    <img src="<?php print file_create_url($image_url); ?>" alt="img"/>
                                </figure>
                            </div>
                            <div class="col-sm-7">
                                <h3><?php print $pane->title; ?></h3>
                                <div class="content">
                                    <?php print render($content['field_description']);?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </section>
  <?php endif;?>
<?php else: ?>
  <section class="line">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
            <?php
				if($bg_image_url) {
					print '<div class="action-call red" style="background-image: url('.file_create_url($bg_image_url).')">';	
				}else {
					print '<div class="action-call red">';
				}
			?>
              	<?php print render($content['field_description']);?>
            </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>
