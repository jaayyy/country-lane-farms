<div class="main-wrap">

    <?php include 'header.php';

    drupal_add_js($base_url."/".drupal_get_path('theme', 'md_oldal')."/js/jquery.valid-email.js");
    drupal_add_js($base_url."/".drupal_get_path('theme', 'md_oldal')."/js/bootstrap.min.js");
    drupal_add_js($base_url."/".drupal_get_path('theme', 'md_oldal')."/js/jquery.register.js");
    drupal_add_js("https://maps.googleapis.com/maps/api/js?key=" . OM_MAPS_API_KEY, "external");
    //drupal_add_js($base_url."/".drupal_get_path('theme', 'md_oldal')."/js/register-map.js");
    $Locations = om_location();
    if ($Locations["StatusCode"] == 1) {
        $Locations = $Locations["Data"];
    } else {
        $Locations = false;
    }
    ?>
<style type="text/css">
    #map-container {
        width: 650px;
        height: 400px;
    }
    #map-container img{ max-width: none; }
</style>


    <div class="content col-md-10 col-md-offset-1">
        <div class="register-form form">
          <h1>Create a new account</h1>
          <p>
            Register with us to start your first order and receive newsletter updates on upcoming specials.
          </p>
          <p>Required<sup class="red-text">*</sup></p>


          <form action="<?php echo base_url()."/?q=".drupal_lookup_path("alias", "node/12"); ?>" method="post" id="register-form">
            <div class="left-col col-md-4">
              <div class="form-group">
                <label for="first-name">First Name<sup class="red-text">*</sup></label>
                <input type="text" id="first-name" name="f[firstname]" >
              </div>
              <div class="form-group">
                <label for="last-name">Last Name<sup class="red-text">*</sup></label>
                <input type="text" id="last-name" name="f[lastname]" >
              </div>
              <div class="form-group">
                <label for="email">Email Address<sup class="red-text">*</sup></label>
                <input type="text" id="email" name="f[email]" ok="false" >
              </div>
              <div class="form-group">
                <label for="conf-email">Confirm Email Address<sup class="red-text">*</sup></label>
                <input type="text" id="conf-email" name="f[emailconfirm]" >
              </div>
              <div class="form-group">
                <label for="password">Password<sup class="red-text">*</sup> (must be 8 characters or more)</label>
                <input type="password" id="password" name="f[password]">
              </div>
              <div class="form-group">
                <label for="conf-password">Confirm Password<sup class="red-text">*</sup> (must be 8 characters or more)</label>
                <input type="password" id="conf-password" name="f[passwordconfirm]" data-container="body" data-trigger="focus" data-toggle="popover" data-placement="bottom">
              </div>
              <div class="form-group">
                <label for="phone">Phone<sup class="red-text">*</sup></label>
                <input type="text" id="phone" name="f[phone]">
              </div>
              <div class="form-group">
                <label for="hear-about">How did you hear about us?</label>
                <select id="hear-about" class="form-control" style="padding: 6px 12px; border-width: 2px;">
                    <option value="">Select an Option</option>
                    <option value="Friend">Friend</option>
                    <option value="Internet">Internet</option>
                    <option value="Other">Other</option>
                </select>
                <input type="text" id="hear-option">
                <input type="hidden"  name="f[hear]" value="" id="hear-value" />
              </div>
            </div>

            <div class="right-col col-md-8">
<?php
                  if (!isset($FormData["locationid"])) {
                    $FormData["locationid"] = 0;
                  }
                  $Selected[$FormData["locationid"]] = "checked";
?>
              <div class="inner row">
                <div class="col-sm-4 hidden-xs">
                  <label id="preferred-location">Preferred Pick up location<sup class="red-text">*</sup></label>
				    <div class="radio" id="preferred-location-div">
                    <label>
                      <input type="radio" class="location0" name="f[locationid]" value="0" <?php echo $Selected[0]; ?>  />None
                    </label>
                  </div>
                  <?php

                  foreach($Locations as $idx => $l) {
                  ?>
                  <div class="radio">
                    <label>
                      <input type="radio" class="location<?php echo $l->id; ?>" name="f[locationid]" value="<?php echo $l->id; ?>" <?php echo $Selected[$l->id]; ?> onclick="javascript:PickupSelected(<?php echo $idx; ?>);" /><?php echo $l->name; ?>
                    </label>
                  </div>
                  <?php } ?>

                </div>

                <style>
						#map-canvas {width: 100%; height: 489px}
						#map-canvas img{ max-width: none; }
						</style>
                <script type="text/javascript">

								var markers = <?php echo json_encode($Locations);?>;
                var map;
									function initialize() {
										var mapOptions = {
											center: new google.maps.LatLng(51.533873, -114.029312),
											zoom: 8

										};
										map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
										var googleMapMarkers = [];
										var contentStrings = [];
										var infoWindows = [];

										for(var i in markers){

												addMarker(map, markers[i]);

										}

									}
									google.maps.event.addDomListener(window, 'load', initialize);

									function addMarker(map, place){
										var latLng = new google.maps.LatLng(place.latitude,place.longitude);
										var marker = new google.maps.Marker({
												position: latLng,
												map: map
										});


										var infoWindow = new google.maps.InfoWindow({
												content: place.name + "<br />" + place.address
										});


										google.maps.event.addListener(marker, 'click', function() {

											infoWindow.open(map,marker);

										});
									}

									function PickupSelected(idx){

                    var place = markers[idx];
                    var latLng = new google.maps.LatLng(place.latitude,place.longitude);
                    map.setCenter(latLng);
                    map.setZoom(15);
                  }
						</script>
                <div class="map col-sm-8">
                    <div id="map-canvas">

                    </div>
                    <!--
                  <img src="http://maps.google.com/maps/api/staticmap?center=51.079685,-114.054366&zoom=9&scale=2&size=650x300&markers=51.052068,-113.823654|51.127442,-114.069121|51.130128,-114.198562|51.069330,-114.013168|50.654566,-112.832137|51.051205,-114.144317|50.950956,-114.068786|51.107285,-114.162170|50.914821,-114.059345" alt="">
                  -->
                </div>
              </div>

              <div class="fields row">
                <div class="col-md-6 nopadding">
                  <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" id="address" name="f[address]">
                  </div>
                  <div class="form-group">
                    <label for="city">City</label>
                    <input type="text" id="city" name="f[city]">
                  </div>
                  <div class="form-group">
                    <label for="postal-code">Postal Code</label>
                    <input type="text" id="postal-code" name="f[postalcode]">
                  </div>
                </div>
              </div>

              <div class="pickup-mobile row visible-xs">
                <label>Preferred Pick up location<sup class="red-text">*</sup></label>
                <select class="form-control" name="f[location]" id="preferred-location-mobile">
                  <?php
                  if (!isset($FormData["locationid"])) {
                    $FormData["locationid"] = 0;
                  }
                  $Selected[$FormData["locationid"]] = "checked";
				  ?>
				  <option value="0" <?php echo $Selected[0]; ?>>None</option>
				  <?php
                  foreach($Locations as $l) { ?>
                  <option value="<?php echo $l->id; ?>" <?php if($Selected[$l->id] === 'checked') echo 'selected'; ?>><?php echo $l->name; ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="row">
               <div class="checkbox">
                  <label>
                    <input type="checkbox" name="f[newsletter]" value="1" checked>Newsletter Signup (no more than three times per month)<br/>
                    <span class="note">PLEASE NOTE: Order confirmation &amp; Reminders will always be sent.</span>
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="f[privacy]" value="1"  id="privacy" data-container="body" data-trigger="focus" data-toggle="popover" data-placement="left">I agree to the <a href="/privacy-policy" target="_blank">Privacy Policy</a>
                  </label>
                </div>
                <div class="checkbox" >
                  <label>
                    <input type="checkbox" name="f[terms]" value="1" id="terms" data-container="body" data-trigger="focus" data-toggle="popover" data-placement="left">I agree to the <a href="/terms-of-use" target="_blank">Terms of Use</a>
                  </label>
                </div>

                <div class="submit-buttons">
                    <input type="submit" value="Create" class="button" style="border: none;" />
                    <span class="login-link">or&nbsp;&nbsp;<a href="/login">Log In</span></a>
                </div>

              </div>


            </div>

          </form>



        </div>
    </div>

</div>
<script>
	jQuery(document).ready(function($) {
		$('#preferred-location-mobile').change(function() {
			var value = $('#preferred-location-mobile').val();
			var activeclass = "location"+value;
			$('.'+activeclass+'').prop('checked', true);;
		});
	});

</script>
<?php include 'footer.php'; ?>


