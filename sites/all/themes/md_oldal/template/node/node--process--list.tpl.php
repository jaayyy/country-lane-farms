<div class="col-sm-3 col-xs-6">
    <div class="process">
        <div class="oldal-icon extra-icon fill-icon">
            <i class="<?php print trim(render($content['field_process_icon'])); ?>"></i>
        </div>
        <h3><?php print $title;?></h3>
        <div class="content">
            <?php print render($content['field_process_description']); ?>
        </div>
    </div>
</div>