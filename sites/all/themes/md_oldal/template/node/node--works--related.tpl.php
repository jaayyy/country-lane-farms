<div class="col-sm-4">
  <figure>
    <a href="#"><img src="<?php print render($content['field_works_image']); ?>" alt="img" /></a>
    <div class="overlay">
      <a href="<?php print trim(render($content['field_works_image'])); ?>" class="trigger-fancybox" title="<?php print $title; ?>">
        <i class="entyp-picture-1"></i>
        <!--<i class="entyp-resize-full-1"></i>-->
      </a>
    </div>
  </figure>
  <h4><strong><a href="<?php print $node_url; ?>"><?php print $title;?></a></strong></h4>
  <a class="category" href="#"><?php print render($content['field_works_subtitle']); ?></a>
</div>