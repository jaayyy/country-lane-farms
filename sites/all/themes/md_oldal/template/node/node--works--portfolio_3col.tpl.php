<?php 
  $items = field_get_items('node', $node, 'field_works_categories');
  $term_classes = array();
  if ($items) {
    foreach ($items as $item) {
      $term_classes[] = str_replace(' ', '-', strtolower($item['taxonomy_term']->name));
    }
  }
    
?>
<div class="col-sm-4 isotopeSelector <?php print implode(" ", $term_classes);?>">
  <div class="portfolio-post">
    <figure>
      <a href="<?php print $node_url; ?>"><img src="<?php print render($content['field_works_image']); ?>" alt="<?php print $title; ?>" /></a>
      <div class="overlay">
        <div class="inner">
          <a href="<?php print $node_url; ?>">
            <div class="oldal-icon grey-icon fill-icon sm-icon">
              <i class="entyp-forward-1"></i>
            </div>
          </a>
          <a href="<?php print trim(render($content['field_works_image'])); ?>" class="fancybox-product">
            <div class="oldal-icon grey-icon fill-icon sm-icon">
              <i class="entyp-search-1"></i>
            </div>
          </a>
        </div>
        <div class="background"></div>
      </div>
    </figure>
    <h4><strong><a href="<?php print $node_url; ?>"><?php print $title; ?></a></strong></h4>
    <a class="category" href="#"><?php print render($content['field_works_subtitle'])?></a>
  </div>
</div>