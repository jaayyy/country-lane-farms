<div class="col-sm-4">
  <article class="recent-post">
    <figure>
      	<img src="<?php print render($content['field_blog_thumb']); ?>" alt="img" />
      <div class="overlay">
        <a href="<?php print $node_url; ?>" class="btn btn-default"><?php print t('Read Article'); ?></a>
        <div class="background"></div>
      </div>
    </figure>
    <h4><strong><a href="<?php print $node_url; ?>"><?php print $title;?></a></strong> <span class="comment-count"><i class="entyp-comment-1"></i><?php print $node->comment_count;?></span></h4>
    <div class="date"><?php print format_date($node->created, 'custom', 'F dS, Y');?></div>
    <?php print render($content['field_blog_body'])?>
  </article>
</div>