<!--Get categories list-->
<?php global $base_url; ?>

<section class="line">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="portfolio-header">
          <div class="controls">
            <?php if ($prev = oldal_custom_get_previous_node($node)) :?>
              <a href="<?php print $base_url.'/'.drupal_lookup_path('alias',"node/".$prev); ?>" class="btn btn-default btn-control"><?php print t('Previous Page'); ?></a>
            <?php endif;?>
            <?php if ($next = oldal_custom_get_next_node($node)) :?>
              <a href="<?php print $base_url.'/'.drupal_lookup_path('alias',"node/".$next); ?>" class="btn btn-default btn-control"><?php print t('Next Page'); ?></a>
            <?php endif;?>
          </div>
          <div class="portfolio-post-title">
            <h3><?php print $title;?></h3>
            <div class="category"><?php print render($content['field_works_subtitle'])?></div>
          </div>
        </div>
        <div class="portfolio-single-post">
          <figure>
            <img src="<?php print render($content['field_works_image'])?>" alt="<?php print $title;?>" />
            <div class="overlay overlay-general">
              <a href="<?php print file_create_url($node->field_works_image['und'][0]['uri']); ?>" class="trigger-fancybox">
                <i class="entyp-picture-1"></i>
              </a>
            </div>
          </figure>
          <div class="col-sm-9">
            <div class="description-title"><?php print t('Project Description'); ?></div>
            <div class="description-content">
              <?php print render($content['field_works_description']); ?>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="description-title">Project Description</div>
            <div class="description-content">
              <ul>
                <li>
                  <div class="oldal-icon grey-icon xs-icon">
                    <i class="entyp-calendar-1"></i>
                  </div>
                  <?php print format_date($node->created, 'custom', 'Y, F d');?>
                </li>
                <li>
                  <div class="oldal-icon grey-icon xs-icon">
                    <i class="entyp-tag-1"></i>
                  </div>
                  <?php print oldal_custom_field_term_plain($content['field_works_categories']); ?>
                </li>
                <li>
                  <div class="oldal-icon grey-icon xs-icon">
                    <i class="entyp-link-1"></i>
                  </div>
                  <a href="<?php print render($content['field_works_link']); ?>" target="_blank">Visit Client Site</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Render block related works-->
<?php print views_embed_view('related_works', 'block'); ?>