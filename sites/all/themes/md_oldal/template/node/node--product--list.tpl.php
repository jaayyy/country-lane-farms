<?php
$img_items = $content['product:field_images']['#items'];
if (!empty($img_items)) {
  $img_item = reset($img_items);
  $img_url = file_create_url($img_item['uri']);
}
?>

<div class="col-xs-6 col-sm-3">
  <div class="product">
    <figure>
      <?php if ($img_url): ?>
        <img src="<?php print $img_url; ?>" alt="img"/>
      <?php endif; ?>
      <div class="overlay">
        <div class="overlay-info">
          <div class="inner">
            <a class="buy" href="<?php print $node_url; ?>">
              <div class="oldal-icon black-icon">
                <i class="entyp-basket-1"></i>
              </div>
              <br/>
              <?php print t('buy'); ?>
            </a>
            <hr/>
            <a class="compare" href="<?php print $node_url; ?>">
              <div class="oldal-icon grey-icon">
                <i class="entyp-docs-1"></i>
              </div>
              <br/>
              <?php print t('compare'); ?>
            </a>
          </div>
        </div>
        <div class="background"></div>
      </div>
    </figure>
    <div class="product-name"><?php print $title; ?></div>
    <div class="product-price"><?php print render($content['product:commerce_price']); ?></div>
    <?php if (isset($content['field_category']) && !empty($content['field_category'])) : ?>
      <div class="category"><?php print oldal_custom_field_term_plain($content['field_category']); ?></div>
    <?php endif; ?>
  </div>
</div>