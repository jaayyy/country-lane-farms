<div class="col-sm-4">
    <div class="panel panel-default pricing-box">
        <div class="panel-heading"><?php print $title;?></div>
        <div class="panel-body">
            <div class="price">
                <div class="value"><sup>$</sup><?php print render($content['field_pricing_price']); ?></div>
                <div class="per">/<?php print t('month'); ?></div>
            </div>
            <ul class="features">
				<?php 
                    $items = field_get_items('node', $node, 'field_pricing_features');
                    foreach($items as $item) {
                        print '<li>'.$item['value'].'</li>';
                    }
                ?>
            </ul>
            <a href="#" class="btn btn-primary"><?php print t('BUY NOW'); ?></a>
        </div>
    </div>
</div>