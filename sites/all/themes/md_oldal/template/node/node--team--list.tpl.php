<div class="col-sm-2">
  <figure>
    <img src="<?php print trim(render($content['field_team_image'])); ?>" alt="img" />
    <div class="overlay"></div>
    <div class="buttons">
      <div class="inner">
        <ul class="buttons-wrap">
          <?php 
            if ($items = field_get_items('node', $node, 'field_team_social_icons')) {
              foreach ($items as $item) {
                $fc = entity_load('field_collection_item', array($item['value']));
                $fc = reset($fc);
                print '<li><a href="' . $fc->field_team_social_link['und'][0]['value'] . '"><i class="' . $fc->field_team_social_icon['und'][0]['value'] . '"></i></a></li>';
              }
            }
          ?>
        </ul>
      </div>
    </div>
  </figure>
  <h5><strong><?php print $title;?></strong></h5>
  <div class="position"><?php print render($content['field_team_position']); ?></div>
</div>
