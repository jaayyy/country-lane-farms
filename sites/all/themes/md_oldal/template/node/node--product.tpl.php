<section class="line">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-xs-12">
                <div class="product-slider">
                    <ul class="slides">
                        <!--Node Images-->
                        <?php 
                          $content['product:field_images']['#prefix'] = '';
                          $content['product:field_images']['#suffix'] = '';
                        ?>
                        <?php print render($content['product:field_images']); ?>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="product-details">
                    <div class="product-title"><?php print $title; ?></div>
                    <div class="product-price"><?php print render($content['product:commerce_price']); ?></div>
                    <div class="description">
                        <!--Node description-->
                        <?php print render($content['body']); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="product-selectors">
                    <div class="row">
                        <!--Add to cart form-->
                        <?php print render($content['field_product']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="line grey">
    <div class="container">
        <div class="row">
            <div class="col-sm-10">
                <div class="product-tabs">
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#details" data-toggle="tab"><?php print t('Descriptions Details'); ?></a></li>
                        <li><a href="#reviews" data-toggle="tab"><?php print t('Reviews'); ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="details">
                            <!--Node detail-->
                            <?php print render($content['field_detail']); ?>
                        </div>
                        <div class="tab-pane text-editor" id="reviews">
                            <!--Node review items-->
                            <?php print render($content['comments']); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
