<article class="post bordered">
  <h2><?php print $title; ?></h2>
  <ul class="info">
    <li>
      <a href="#">
        <i class="entyp-pencil-1"></i>
        <div class="text"><?php print $node->name; ?></div>
      </a>
    </li>
    <li>
      <i class="entyp-calendar-1"></i>
      <div class="text"><?php print format_date($node->created, 'custom', 'Y, F d'); ?></div>
    </li>
    <li>
      <a href="#">
        <i class="entyp-list-1"></i>
        <div class="text"><?php print oldal_custom_field_term_plain($content['field_blog_categories']); ?></div>
      </a>
    </li>
    <li>
      <i class="entyp-chat-1"></i>
      <div class="text"><?php print $node->comment_count . t(' comment(s)'); ?></div>
    </li>
    <li>
      <a href="#">
        <i class="entyp-tag-1"></i>
        <div class="text"><?php print oldal_custom_field_term_plain($content['field_blog_tags']);?></div>
      </a>
    </li>
  </ul>
  <?php 
  	$count = isset($node->field_blog_multimedia['und']) ? count($node->field_blog_multimedia['und']) : 0;
	if($count > 1):
  ?>
    <div class="post-images">
        <div class="slider-controls">
            <button class="left"><i class="entyp-left-open-1"></i></button>
            <button class="right"><i class="entyp-right-open-1"></i></button>
        </div>
        <ul class="slides">
            <?php
            	$items =  field_get_items('node', $node, 'field_blog_multimedia');
				foreach($items as $item) {
					print '<li><figure><img src="'.file_create_url($item['file']->uri).'" alt="img"/></figure></li>';
				}
			?>
        </ul>
    </div>
  <?php else :?>
    <figure><div class="full-width"><?php print render($content['field_blog_multimedia'])?></div></figure>
  <?php endif;?>
  <div class="content">
    <?php print render($content['field_blog_body'])?>
  </div>
  <div class="btn btn-default"><a href="<?php print $node_url; ?>"><?php print t('Read More'); ?></a></div>
</article>