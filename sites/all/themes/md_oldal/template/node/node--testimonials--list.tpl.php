<div class="col-sm-6">
  <div class="col-sm-10 col-sm-offset-1">
    <div class="testimonial">
      <div class="content">
        <div class="text">
          <?php print render($content['field_testimonials_quote']); ?>
        </div>
      </div>
      <div class="author">
        <div class="name"><?php print $title;?></div>
        <div class="position"><?php print render($content['field_testimonials_position']); ?></div>
      </div>
    </div>
  </div>
</div>