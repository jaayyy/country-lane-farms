<div class="col-sm-6">
    <div class="box">
        <header>
            <div class="title">
                <div class="oldal-icon grey-icon lg-icon">
                    <i class="<?php print render($content['field_services_icon']); ?>"></i>
                </div>
                <?php print $title;?>
            </div>
        </header>
        <div class="content lg-padded">
            <?php print render($content['field_services_description']); ?>
        </div>
    </div>
</div>