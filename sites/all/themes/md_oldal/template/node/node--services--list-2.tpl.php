<div class="col-sm-3">
    <div class="box text-center">
        <header>
            <div class="oldal-icon red-icon lg-icon fill-icon">
                <i class="<?php print render($content['field_services_icon']); ?>"></i>
            </div>
            <br/>
            <div class="title"><?php print $title;?></div>
        </header>
        <div class="content">
            <?php print render($content['field_services_description']); ?>
        </div>
    </div>
</div>