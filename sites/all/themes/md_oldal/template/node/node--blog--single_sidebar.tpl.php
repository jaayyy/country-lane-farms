<section class="posts">
  <div class="container">
    <div class="row">
      <div class="col-sm-9">
        <article class="post ">
          <h3 class="uppercase"><?php print $title;?></h3>
          <ul class="info">
            <li>
              <div class="text date"><?php print format_date($node->created, 'custom', 'F dS, Y'); ?></div>
            </li>
            <li>
              <i class="entyp-pencil-1"></i>
              <div class="text">Posted by
                <a href="#"><?php print $node->name; ?></a>
              </div>
            </li>
            <li>
              <i class="entyp-chat-1"></i>
              <div class="text"><?php print $node->comment_count;?> comments</div>
            </li>
            <li>
              <a href="#">
                <i class="entyp-list-1"></i>
                <div class="text"><?php print oldal_custom_field_term_plain($content['field_blog_categories']); ?></div>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="entyp-tag-1"></i>
                <div class="text"><?php print oldal_custom_field_term_plain($content['field_blog_tags']);?></div>
              </a>
            </li>
          </ul>
          <figure>
            <?php print render($content['field_blog_multimedia']); ?>
            <div class="overlay overlay-general">
              <a href="<?php print oldal_get_field_image_url($node, 'field_blog_multimedia'); ?>" class="trigger-fancybox">
                <i class="entyp-picture-1"></i>
              </a>
            </div>
          </figure>
          <div class="content">
            <?php print render($content['field_blog_body']);?>
          </div>
          <a class="share-link" href="#">
            <div class="oldal-icon black-icon">
              <i class="entyp-share"></i>
            </div>
            Share
          </a>
        </article>
        <?php print render($content['comments']); ?>
      </div>
    </div>
  </div>
</section>