<div class="col-sm-4">
  <div class="product">
    <figure>
      <?php print $product_img;?>
      <div class="overlay">
        <div class="overlay-info">
          <div class="inner">
            <a class="buy" href="<?php print $node_url; ?>">
              <div class="oldal-icon black-icon">
                <i class="entyp-basket-1"></i>
              </div>
              <br/>
              buy
            </a>
            <hr/>
            <a class="compare" href="<?php print $node_url; ?>">
              <div class="oldal-icon grey-icon">
                <i class="entyp-docs-1"></i>
              </div>
              <br/>
              <?php print t('compare'); ?>
            </a>
          </div>
        </div>
        <div class="background"></div>
      </div>
    </figure>
    <div class="product-name"><?php print $title; ?></div>
    <div class="product-price"><?php print render($content['product:commerce_price']); ?></div>
    <div class="category"><?php print oldal_custom_field_term_plain($content['field_category']); ?></div>
  </div>
</div>