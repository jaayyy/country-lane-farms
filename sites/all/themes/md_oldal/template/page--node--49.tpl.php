<div class="main-wrap">
    <header class="navbar navbar-inverse">
        <div class="normal-color with-slider">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?php print base_path(); ?>"><img src="<?php print $logo ?>" alt="brand" /></a>
                        </div>
                  </div>
                    <div class="col-sm-9">
                        <div class="navbar-collapse collapse navbar-right">
                            <ul class="nav navbar-nav social-nav">
                                <?php
									$array = theme_get_setting('ft_social','md_oldal') ;
									if(!empty($array)) :
										foreach($array as $key => $value) {
											$arr_parent_icon  = $value['icon'] ; 
											$arr_parent_link  = $value['link'] ;
											print '<li><a href="'.$arr_parent_link.'"><i class="'.$arr_parent_icon['icon'].'"></i></a></li>'; 
										}
									endif;
								?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if($page['slideshow']):?>
        <div class="header-slider">
            <?php print render($page['slideshow']);?>
        </div>
        <?php endif;?>
        <div class="inverse-color">
          <div class="container">
                <div class="row">
                    <div class="navbar-collapse collapse">
                        <div class="col-sm-3 visible-xs">
                            <?php if($page['search_3']):?>
								<?php print render($page['search_3']);?>
                            <?php endif; ?>
                        </div>
                        <div class="col-sm-11">
                            <!-- START NAVIGATION MENU ITEMS -->
                              <?php if (module_exists('i18n_menu')) {
                                  $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
                                } else {
                                  $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
                                }
                                print drupal_render($main_menu_tree);
                              ?>
                            <!-- END NAVIGATION MENU ITEMS -->
                        </div>
                        <div class="col-sm-1 hidden-xs text-right">
                            <button class="btn search-trigger" type="submit"><i class="entyp-search-1"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if($page['search']):?>
        <div class="search-wrap">
          <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <?php print render($page['search']);?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </header>
    <section class="line top page-title-block">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="col-md-4 col-sm-5 col-xs-12">
                <?php if ($title): ?>
                    <div class="title">
                      <?php print $title; ?>
                      <ul class="current-page">
                        <li><a href="<?php print base_path(); ?>"><?php print t('Home'); ?></a></li>
                        <li>></li>
                        <li><?php print $title; ?></li>
                      </ul>
                    </div>
                <?php endif; ?>
              </div>
              <div class="col-md-8 col-sm-7 col-xs-12">
                <div class="title-content">
                  <div class="col-md-3 hidden-sm"></div>
                  <div class="col-sm-9 col-md-6 search-region-2">
                  	<?php if($page['search_2']):?>
                    	<?php print render($page['search_2']);?>
					<?php endif;?>		  
                  </div>
                  <div class="col-sm-3">
                    <a class="btn btn-primary btn-cart" href="<?php print base_path() ?>/cart">
                      <div class="oldal-icon sm-icon">
                        <i class="entyp-basket-1"></i>
                      </div>
                      <?php print t('Cart'); ?>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
    <?php if($page['sidebar']):?>
    	<section class="post">
           <div class="container">
				<?php if(theme_get_setting('sidebar_position','md_oldal') != 'no'):?>
                    <?php if(theme_get_setting('sidebar_position','md_oldal') == 'left'):?>
                        <div class="col-md-2 col-sm-3">
                            <aside class="widgets">
                                <?php print render($page['sidebar']);?>
                            </aside>
                        </div>
                        <div class="col-md-1 hidden-sm"></div>	
                    <?php endif;?>
                    <div class="col-sm-9">
                        <?php print $messages; ?>
                        <?php if ($tabs): ?><div class="tabs node-tabs"><?php print render($tabs); ?></div><?php endif; ?>
                        <?php if($page['content']):?>
                            <?php print render($page['content']);?>
                        <?php endif;?>
                    </div>
                    <?php if(theme_get_setting('sidebar_position','md_oldal') == 'right'):?>
                        <div class="col-md-1 hidden-sm"></div>
                        <div class="col-md-2 col-sm-3">
                            <aside class="widgets">
                                <?php print render($page['sidebar']);?>
                            </aside>
                        </div>	
                    <?php endif;?>
                <?php else:?>
                    <div class="col-sm-12">
						<div class="container"><?php print $messages; ?></div>
						<div class="container"><?php if ($tabs): ?><div class="tabs node-tabs"><?php print render($tabs); ?></div><?php endif; ?></div>
                        <?php if($page['content']):?>
                            <?php print render($page['content']);?>
                        <?php endif;?>
                    </div>
                <?php endif;?>
            </div>
        </section>
    <?php else: ?>
    	<div class="container"><?php print $messages; ?></div>
		<div class="container"><?php if ($tabs): ?><div class="tabs node-tabs"><?php print render($tabs); ?></div><?php endif; ?></div>
        <?php if($page['content']):?>
            <?php print render($page['content']);?>
        <?php endif;?>
    <?php endif; ?>
</div>

<?php include 'footer.php'; ?>

<script>
	$(document).ready(function() {
		$('.btn-fancybox').click(function() {
			$.fancybox.open([
				{
					href : '<?php print base_path() ?>/sites/default/files/col1-portfolio01.jpg',
					title : '1st title'
				},
				{
					href : '<?php print base_path() ?>/sites/default/files/col1-portfolio02.jpg',
					title : '2nd title'
				},
				{
					href : '<?php print base_path() ?>/sites/default/files/col1-portfolio03.jpg',
					title : '3rd title'
				},
				{
					href : '<?php print base_path() ?>/sites/default/files/col1-portfolio04.jpg',
					title : '4th title'
				}
			], {
				padding : 0,
				autoResize: true,
				arrows : false,
				closeBtn : false,
				prevEffect : 'fade',
				nextEffect : 'fade',
				helpers : {
					buttons	: {
						position : 'top'
					}
				},
				beforeShow: function() {
					var playButton = $('<button class="oldal-play oldal-fancy"><i class="entyp-play-1"></i></button>');
					var prevButton = $('<button class="oldal-prev oldal-fancy"><i class="entyp-left-dir-1"></i></button>');
					var nextButton = $('<button class="oldal-next oldal-fancy"><i class="entyp-right-dir-1"></i></button>');
					var closeButton = $('<button class="oldal-close oldal-fancy"><i class="entyp-cancel-1"></i></button>');
					var fullscreenButton = $('<button class="oldal-fullscreen oldal-fancy"><i class="entyp-resize-full-1"></i></button>');
					var leftContainer = $('<div class="left-container"></div>');
					leftContainer.append(playButton);
					leftContainer.append(prevButton);
					leftContainer.append(nextButton);
					playButton.click(function() {
						$.fancybox.play();
					});
					prevButton.click(function() {
						$.fancybox.prev();
					});
					nextButton.click(function() {
						$.fancybox.next();
					});
					closeButton.click(function() {
						$.fancybox.close();
					});
					fullscreenButton.click(function() {
						$.fancybox.toggle();
					});
					$.fancybox.wrap.append(leftContainer);
					$.fancybox.wrap.append(closeButton);
					$.fancybox.wrap.append(fullscreenButton);
				}
			});
		});
	});
</script>