<div class="main-wrap">
	<?php
    include 'header.php';
	?>
    <?php if($page['sidebar']):?>
    	<section class="post">
           <div class="container">
				<?php if(theme_get_setting('sidebar_position','md_oldal') != 'no'):?>
                    <?php if(theme_get_setting('sidebar_position','md_oldal') == 'left'):?>
                        <div class="col-md-2 col-sm-3">
                            <aside class="widgets">
                                <?php print render($page['sidebar']);?>
                            </aside>
                        </div>
                        <div class="col-md-1 hidden-sm"></div>	
                    <?php endif;?>
                    <div class="col-sm-9">
                        <?php print $messages; ?>
                        <?php if($page['content']):?>
                            <?php print render($page['content']);?>
                        <?php endif;?>
                    </div>
                    <?php if(theme_get_setting('sidebar_position','md_oldal') == 'right'):?>
                        <div class="col-md-1 hidden-sm"></div>
                        <div class="col-md-2 col-sm-3">
                            <aside class="widgets">
                                <?php print render($page['sidebar']);?>
                            </aside>
                        </div>	
                    <?php endif;?>
                <?php else:?>
                    <div class="col-sm-12">
						<div class="container"><?php print $messages; ?></div>
						<div class="container"><?php if ($tabs): ?><div class="tabs node-tabs"><?php print render($tabs); ?></div><?php endif; ?></div>
                        <?php if($page['content']):?>
                            <?php print render($page['content']);?>
                        <?php endif;?>
                    </div>
                <?php endif;?>
            </div>
        </section>
    <?php else: ?>
    	<div class="container"><?php print $messages; ?></div>
        <?php if($page['content']):?>
            <?php print render($page['content']);?>
        <?php endif;?>
    <?php endif; ?>
</div>
<?php include 'footer.php'; ?>