<div class="main-wrap">

    <?php include 'header.php'; ?>

    

    <div class="content col-sm-10 col-sm-offset-1"> 
        <div class="login-form form">
          <h1>You must be logged in to start your order</h1>
          <p>
            The best way to check if fully cooked is inserting a meat thermometer in 
              the thigh meat and once a temperature of 160 F is reached, remove from heat and let stand for 5 minutes 
              before carving. The tantalizing smell while cooking will set your mouth watering.
          </p>

          <form action="/user/login" id="user-login">
            <div class="left-col col-sm-6">  
              <h1>Customer log in</h1>
              <div class="form-group">
                <label for="email">Email Address</label>
                <input type="email" id="email" name="name">
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" id="password" name="pass">
              </div>
              <div class="form-group">
                <div><a href="/user/password/">Forgot your password?</a></div>
                <input type="submit" form="user-login" value="Sign In" class="button">
              </div>
            </div>

            <div class="right-col col-sm-6">
              <h1>New Customers</h1>
              <p>Register with us to start your first order and receive newsletter updates on upcoming specials.</p>
              <a href="/register" class="button">Register Now</a>
            </div>

          </form>

          

        </div>
    </div>

</div>

<?php include 'footer.php'; ?>


