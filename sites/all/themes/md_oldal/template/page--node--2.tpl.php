<div class="main-wrap">

    <?php include 'header.php';
?>

    

    <div class="content">    
      <div class="left-col col-md-6">
        <div class="pic-block">
          <img src="/sites/all/themes/md_oldal/img/content/chicken-pic.png" alt="">
        </div>
        <div class="red-message">
          Please note that October 8 - 10 will ONLY 
          be a FRESH TURKEY delivery. We will NOT 
          have FRESH chicken October 8 - 10, 2014
        </div>
      </div>
      <div class="right-col col-md-5">
        <div class="inner">
          <h1>Chicken Whole</h1>
          <div class="text-block">
            <p>
              Great for a 'no worry' meal. Just roast 'low and slow' stuffed or not for a mouth-watering meal 
              that everyone will love. The best way to check if fully cooked is inserting a meat thermometer in 
              the thigh meat and once a temperature of 160 F is reached, remove from heat and let stand for 5 minutes 
              before carving. The tantalizing smell while cooking will set your mouth watering.
            </p>

            <p>
              Fresh Whole Roasting chicken-approximately 6 - 7 lbs each.
              To receive the bulk pricing, simply place an order for at least 12 chickens.
            </p>

            <p>
              Price per lb. $4.50 <br/>
              Estimated Cost Per Item $30.00
            </p>

            <p class="red-text">
              Bulk Price per lb. $4.00 <br/>
              Estimated Cost Per Item $26.75 <br/>
              Minimum Bulk Quantity: 12+
            </p>

            <p>
              PLEASE NOTE: All of the costs are estimates only. 
              Each item will be weighed when you pick up your order and 
              you will be charged based on the exact amount based on the price per lb cost.
            </p>
          </div>
          <div class="opts">
            <a href="/order-day" class="button pull-right">Start a new order</a>
          </div>
        </div>
      </div>
    </div>

</div>

<?php include 'footer.php'; ?>


