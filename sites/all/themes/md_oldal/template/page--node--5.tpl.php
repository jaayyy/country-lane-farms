<?php

define("REQUIRE_AUTH", true);
?><div class="main-wrap">
<?php include 'header.php';
?>
<script type="text/javascript">
    var AJAX_URL = '<?php echo $base_url."/".drupal_get_path('theme', 'md_oldal')."/ajax"; ?>';
</script>
<?php
global $base_url;
global  $base_path;

drupal_add_js($base_url."/".drupal_get_path('theme', 'md_oldal')."/js/jquery.order-day.js", array('type' => 'file', 'scope' => 'footer', 'weight' => 5));
$DeliveryData = om_delivery_itinerary_get();
if ($DeliveryData["StatusCode"] == 1) {
    $PreferredLocation = om_deliveries_preferred($DeliveryData["Data"]);
    $PreferredLocationData = $PreferredLocation["Preferred"];
    unset($PreferredLocation["Preferred"]);
    $DeliveryData = om_deliveries_array($DeliveryData["Data"]);
} else {
    $DeliveryData = false;
}
?>
    <div class="content col-sm-10 col-sm-offset-1">
      
      <div class="option-info section row hidden-xs">
        <h1>
          Select a location &amp; day
        </h1>
        <div class="left-col col-md-5">
            <div class="pickup-details">
                <?php
                $Date = $PreferredLocationData["date"];
                $Time = $PreferredLocationData["time"];
                $Type = $PreferredLocationData["type"];
                $Name = $PreferredLocationData["name"]; 
                $Address = $PreferredLocationData["address"];
                
                require(dirname(__FILE__)."/../ajax/order-day.details.php");
                ?>
            </div>
        </div>
        <div class="right-col col-md-7">
            <div class="location-map-container">
                <?php
                $Latitude = $PreferredLocationData["latitude"];
                $Longitude = $PreferredLocationData["longitude"] ;
                require(dirname(__FILE__)."/../ajax/order-day.map.php");
                ?>          
            </div>
            <form action="/process-create-order" method="post">
            <input type="hidden" name="f[deliveryitemid]" class="delivery-item-id" value="<?php echo $PreferredLocationData["id"]; ?>" />
                <input type="submit" class="button pull-right" value="Next" />
            </form>
        </div>
      </div>

      <div class="options-box section row">
        <h1 class="visible-xs">
          Select a location &amp; day
          <span class="legend-mark green">Regular Delivery</span>
          <span class="legend-mark orange">Turkey Only</span>
          <span class="legend-mark blue">Regular &amp; Turkey Delivery</span>
        </h1>
        <h2 class="hidden-xs">
          Delivery Options
          <span class="legend-mark green">Regular Delivery</span>
          <span class="legend-mark orange">Turkey Only</span>
          <span class="legend-mark blue">Regular &amp; Turkey Delivery</span>
        </h2>
        <div class="del-btns">
          <a href="#" class="del-prev button">Prev. Delivery</a>
          <a href="#" class="del-next button">Next Delivery</a>
        </div>
        <table class="options-table">          
          <tr>
            <th>Preferred Location</th>
            <th class="del-1">Next Delivery</th>
            <th class="del-2">Delivery 2</th>
            <th class="del-3">Delivery 3</th>
            <th class="del-4">Delivery 4</th>
          </tr>
          <?php
          $limit =0;
          foreach ($PreferredLocation as $k=>$v) {
          	
          ?>
          <tr>
            <td><?php echo $k; ?></td>
            <?php
            $Count = 0;
            foreach($v as $t=>$d) {
            	if ($limit < 4) {
                if ($Count == 0) {
                    $SelectedCSS = " selected";
                } else {
                    $SelectedCSS = "";
                }
                $Count++;
            ?>
            <td class="del-<?php echo $Count;?>">
              <a href="javascript:void(null);" class="option-item mark <?php echo $d["class"]; ?><?php echo $SelectedCSS; ?>" dt="<?php echo $d["date"]; ?>" when="<?php echo $d["time"]; ?>" delivery-type="<?php echo $d["type"]; ?>" loc="<?php echo $d["name"]; ?>" lat="<?php echo $d["latitude"]; ?>" long="<?php echo $d["longitude"]; ?>" adr="<?php echo $d["address"]; ?>" detail-id="<?php echo $d["itemid"]; ?>"><span><?php echo $t; ?> <?php echo $d["time"]; ?></span></a>
            </td>
            <?php
          }
		      $limit++;
          }
            ?>
          </tr>
          <?php
          }
          ?>        
          <tr>
            <th>Location</th>
            <th class="del-1">Next Delivery</th>
            <th class="del-2">Delivery 2</th>
            <th class="del-3">Delivery 3</th>
            <th class="del-4">Delivery 4</th>
          </tr>
          <?php
          foreach ($DeliveryData as $k=>$v) {
          ?>
          <tr class="option-row">
            <td class="loc-title"><?php echo $k; ?></td>
            <?php
            $Count = 1;
            $ThisRowCount = 1;
            foreach($v as $t=>$d) {
                if ($ThisRowCount <= 4) {
            ?>
            <td class="del-<?php echo $Count;?>">
              <a href="javascript:void(null);" class="option-item mark <?php echo $d["class"]; ?>" dt="<?php echo $d["date"]; ?>" when="<?php echo $d["time"]; ?>" delivery-type="<?php echo $d["type"]; ?>" loc="<?php echo $d["name"]; ?>" lat="<?php echo $d["latitude"]; ?>" long="<?php echo $d["longitude"]; ?>" adr="<?php echo $d["address"]; ?>" detail-id="<?php echo $d["itemid"]; ?>"><span><?php echo $t; ?> <?php echo $d["time"]; ?></span></a>
            </td>
            <?php
                }
            $Count++;
            $ThisRowCount++;
            
          }
            ?>
          </tr>
          <?php
          }
          ?>          
        </table>
      </div>

      <div class="option-info section row visible-xs">
        <div class="left-col col-md-5">
            <div class="pickup-details">
                <?php
                $Date = $PreferredLocationData["date"];
                $Time = $PreferredLocationData["time"];
                $Type = $PreferredLocationData["type"];
                $Name = $PreferredLocationData["name"]; 
                $Address = $PreferredLocationData["address"];
                
                require(dirname(__FILE__)."/../ajax/order-day.details.php");
                ?>
            </div>
        </div>
        <div class="right-col col-md-7">
            <div class="location-map-container">

            
            </div>
            <form action="/process-create-order" method="post">
            <input type="hidden" name="f[deliveryitemid]" class="delivery-item-id" value="<?php echo $PreferredLocationData["id"]; ?>" />
                <input type="submit" class="button pull-right" value="Next" />
            </form>
        </div>
      </div>

    </div>

</div>

<?php include 'footer.php'; ?>



