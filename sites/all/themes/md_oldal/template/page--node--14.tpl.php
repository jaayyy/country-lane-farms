<?php
$Products = $_REQUEST["quantity"];
$Prices = $_REQUEST["cost"];
$PriceModel = $_REQUEST["pricemodel"];
$PerPound = $_REQUEST["lb"];
#$OrderID = intval($_SESSION["OrderID"]);
$OrderTotal = 0;

#om_clear_order_items($OrderID);
$order = array();
foreach($Products as $k=>$v) {
    if ($v > 0) {
        
        $Item = array(
                      "orderid" =>  $OrderID,
                      "productid"   =>  $k,
                      "quantity"    =>  $v,
                      "unitcost"    =>  $Prices[$k],
                      "total"       =>  ($Prices[$k] * $v),
                      "pricemodel"  =>  $PriceModel[$k],
                      "pricelb"     =>  $PerPound[$k]
                      );
        
        $OrderTotal += ($Prices[$k] * $v);
        $order[] = $Item;
        #$Result = om_order_item_save($Item);
    }
}
/*
$Order = array(
               "id" =>  $OrderID,
               "total"  =>  $OrderTotal,
               "status" =>  'ready'
               );

$result  = om_order_save($Order);
*/
om_order_session_store("items", $order);

header("Location:/order-confirmation/");
exit();
?>