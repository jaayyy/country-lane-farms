<?php

include_once './' . drupal_get_path('theme', 'md_oldal') . '/inc/front/template.process.inc';
/**
 * Global $base_url
 */
function base_url() {
    global $base_url;
    return $base_url;
}
/**
 * Implements theme_menu_tree().
 */
function md_oldal_menu_tree($variables) {
    return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function md_oldal_field__taxonomy_term_reference($variables) {
    $output = '';

    // Render the label, if it's not hidden.
    if (!$variables['label_hidden']) {
        $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
    }

    // Render the items.
    $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
    foreach ($variables['items'] as $delta => $item) {
        $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
    }
    $output .= '</ul>';

    // Render the top-level DIV.
    $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '">' . $output . '</div>';

    return $output;
}

/**
 * Override of theme('textarea').
 * Deprecate misc/textarea.js in favor of using the 'resize' CSS3 property.
 */
function md_oldal_textarea($variables) {
    $element = $variables['element'];
    $element['#attributes']['name'] = $element['#name'];
    $element['#attributes']['id'] = $element['#id'];
    $element['#attributes']['cols'] = $element['#cols'];
    $element['#attributes']['rows'] = $element['#rows'];
    _form_set_class($element, array('form-textarea'));

    $wrapper_attributes = array(
        'class' => array('form-textarea-wrapper'),
    );

    // Add resizable behavior.
    if (!empty($element['#resizable'])) {
        $wrapper_attributes['class'][] = 'resizable';
    }

    $output = '<div' . drupal_attributes($wrapper_attributes) . '>';
    $output .= '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
    $output .= '</div>';
    return $output;
}

/**
 * @param $variables
 * @return Main menu
 */
function md_oldal_links__system_main_menu($variables) {
    $html = "<div id='nav' class='desktop-screen-navigation'>\n";
    $html .= "  <ul>\n";

    foreach ($variables['links'] as $link) {

        if(isset($link['fragment'])) {
            $link['attributes']['class'] = array('scroll-link transition');
        } else {
            $link['attributes']['class'] = array('transition unscroll-link');
        }
        $html .= "<li class='transition scrollable'>".l($link['title'], $link['href'], $link)."</li>";

    }
    $html .= "  </ul>\n";
    $html .= "</div>\n";

    return $html;
}
/**
 * @param $variables
 * @return Main menu
 */
function md_oldal_links__menu_header_navigation($variables) {
    $html = "<div class='link-holder fade'>\n";
    $html .= "  <ul>\n";

    foreach ($variables['links'] as $link) {

        if(isset($link['fragment'])) {
            $link['attributes']['class'] = array('scroll-link transition');
        } else {
            $link['attributes']['class'] = array('transition unscroll-link');
        }
        $html .= "<li>".l($link['title'], $link['href'], $link)."</li>";

    }
    //kpr($variables);die;
    $html .= "  </ul>\n";
    $html .= "</div>\n";

    return $html;
}
/**
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function md_oldal_form_alter(&$form, &$form_state, $form_id) {
    if (strpos((string)($form_id),"webform_client_form") === false) {
        switch ($form_id) {
            case 'user_login':
                $form['name']['#attributes']['class'][] = '';
                $form['name']['#prefix'] = '<div class="control-group"><div class="controls">';
                $form['name']['#suffix'] = '</div></div>';
                $form['pass']['#attributes']['class'][] = '';
                $form['pass']['#prefix'] = '<div class="control-group"><div class="controls">';
                $form['pass']['#suffix'] = '</div></div>';
                $form['actions']['submit']['#value'] = t('Login');
                $form['actions']['submit']['#prefix'] = '';
                $form['actions']['submit']['#suffix'] = '';
                break;
            case 'user_register_form':
                $form['account']['name']['#attributes']['class'][] = '';
                $form['account']['name']['#prefix'] = '<div class="control-group"><div class="controls">';
                $form['account']['name']['#suffix'] = '</div></div>';
                $form['account']['mail']['#attributes']['class'][] = '';
                $form['account']['mail']['#prefix'] = '<div class="control-group"><div class="controls">';
                $form['account']['mail']['#suffix'] = '</div></div>';
                $form['actions']['submit']['#value'] = t('Create new account');
                $form['actions']['submit']['#prefix'] = '';
                $form['actions']['submit']['#suffix'] = '';
                break;
            case 'user_login_block':
                $form['name']['#attributes']['class'][] = '';
                $form['name']['#prefix'] = '<div class="control-group"><div class="controls">';
                $form['name']['#suffix'] = '</div></div>';
                $form['pass']['#attributes']['class'][] = '';
                $form['pass']['#prefix'] = '<div class="control-group"><div class="controls">';
                $form['pass']['#suffix'] = '</div></div>';
                $form['actions']['submit']['#value'] = t('Login');
                $form['actions']['submit']['#prefix'] = '';
                $form['actions']['submit']['#suffix'] = '';
                break;
            case 'user_pass':
                $form['name']['#attributes']['class'][] = '';
                $form['name']['#prefix'] = '<div class="control-group"><div class="controls">';
                $form['name']['#suffix'] = '</div></div>';
                $form['pass']['#attributes']['class'][] = '';
                $form['pass']['#prefix'] = '';
                $form['pass']['#suffix'] = '';
                $form['actions']['submit']['#value'] = t('Request new password');
                $form['actions']['submit']['#prefix'] = '';
                $form['actions']['submit']['#suffix'] = '';
                break;
            case 'search_block_form':
                $form['search_block_form']['#attributes']['class'][] = 'search';
                $form['search_block_form']['#attributes']['placeholder'] = t('Search..');
                $form['#attributes']['class'][] = 'searh-holder';

                $form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/img/search-icon-top.png');
                $form['actions']['submit']['#attributes'] = array(
                    'class' => array('search-submit'),
					'alt'   => 'search',
                );
                break;
            case 'search_form':
                //kpr($form);die;
                $form['basic']['submit']['#prefix'] = '<div class="form-submit"><div class="form-controls">';
                $form['basic']['submit']['#suffix'] = '</div></div>';
                $form['basic']['submit']['#attributes'] = array(
                    'class' => array('transition button'),
                );
                $form['advance']['#prefix'] = '<div class="row"><div class="container">';
                $form['advance']['#suffix'] = '</div></div>';
                $form['advance']['submit']['#prefix'] = '<div class="form-submit"><div class="form-controls">';
                $form['advance']['submit']['#suffix'] = '</div></div>';
                $form['advance']['submit']['#attributes'] = array(
                    'class' => array('transition button'),
                );
                break;
        }
    } else {
        $form['#prefix'] = '<fieldset id="contact_form">';
        $form['#suffix'] = '</fieldset>';
        $form['actions']['submit']['#value'] = 'Send Message';
        $form['actions']['submit']['#attributes']['class'][] = 'submit_btn transition';
        $form['actions']['submit']['#field_prefix'] = '<i class="fa fa-search transition"></i>';
        $form['actions']['submit']['#sufix'] = '';
    }


}
/**
 * Process variables for comment.tpl.php.
 *
 * @see comment.tpl.php
 */
function md_oldal_preprocess_comment(&$variables) {
    $comment = $variables['elements']['#comment'];
    $node = $variables['elements']['#node'];
    $variables['comment']   = $comment;
    $variables['node']      = $node;
    $variables['author']    = theme('username', array('account' => $comment));

    $variables['created']   = date('d F Y',$comment->created);

    // Avoid calling format_date() twice on the same timestamp.
    if ($comment->changed == $comment->created) {
        $variables['changed'] = $variables['created'];
    }
    else {
        $variables['changed'] = format_date($comment->changed);
    }

    $variables['new']       = !empty($comment->new) ? t('new') : '';
    $variables['picture']   = theme_get_setting('toggle_comment_user_picture') ? theme('user_picture', array('account' => $comment)) : '';
    $variables['signature'] = $comment->signature;

    $uri = entity_uri('comment', $comment);
    $uri['options'] += array('attributes' => array('class' => 'permalink', 'rel' => 'bookmark'));

    $variables['title']     = l($comment->subject, $uri['path'], $uri['options']);
    $variables['permalink'] = l(t('Permalink'), $uri['path'], $uri['options']);
    $variables['submitted'] = t('!username  on !datetime', array('!username' => $variables['author'], '!datetime' => date('d F Y',$comment->created)));

    // Preprocess fields.
    field_attach_preprocess('comment', $comment, $variables['elements'], $variables);

    // Helpful $content variable for templates.
    foreach (element_children($variables['elements']) as $key) {
        $variables['content'][$key] = $variables['elements'][$key];
    }

    // Set status to a string representation of comment->status.
    if (isset($comment->in_preview)) {
        $variables['status'] = 'comment-preview';
    }
    else {
        $variables['status'] = ($comment->status == COMMENT_NOT_PUBLISHED) ? 'comment-unpublished' : 'comment-published';
    }

    // Gather comment classes.
    // 'comment-published' class is not needed, it is either 'comment-preview' or
    // 'comment-unpublished'.
    if ($variables['status'] != 'comment-published') {
        $variables['classes_array'][] = $variables['status'];
    }
    if ($variables['new']) {
        $variables['classes_array'][] = 'comment-new';
    }
    if (!$comment->uid) {
        $variables['classes_array'][] = 'comment-by-anonymous';
    }
    else {
        if ($comment->uid == $variables['node']->uid) {
            $variables['classes_array'][] = 'comment-by-node-author';
        }
        if ($comment->uid == $variables['user']->uid) {
            $variables['classes_array'][] = 'comment-by-viewer';
        }
    }
}

/**
 * template_preprocess_user_picture()
 */
function md_oldal_preprocess_user_picture(&$variables) {
    $variables['user_picture'] = '';
    if (variable_get('user_pictures', 0)) {
        $account = $variables['account'];
        if (!empty($account->picture)) {
            // @TODO: Ideally this function would only be passed file objects, but
            // since there's a lot of legacy code that JOINs the {users} table to
            // {node} or {comments} and passes the results into this function if we
            // a numeric value in the picture field we'll assume it's a file id
            // and load it for them. Once we've got user_load_multiple() and
            // comment_load_multiple() functions the user module will be able to load
            // the picture files in mass during the object's load process.
            if (is_numeric($account->picture)) {
                $account->picture = file_load($account->picture);
            }
            if (!empty($account->picture->uri)) {
                $filepath = $account->picture->uri;
            }
        }
        elseif (variable_get('user_picture_default', '')) {
            $filepath = variable_get('user_picture_default', '');
        }
        if (isset($filepath)) {
            $alt = t("@user's picture", array('@user' => format_username($account)));
            // If the image does not have a valid Drupal scheme (for eg. HTTP),
            // don't load image styles.
            if (module_exists('image') && file_valid_uri($filepath) && $style = variable_get('user_picture_style', '')) {
                $variables['user_picture'] = theme('image_style', array('style_name' => $style, 'path' => $filepath, 'alt' => $alt, 'title' => $alt, 'attributes' => array('class' => array('thumb img-rounded'))));
            }
            else {
                $variables['user_picture'] = theme('image', array('path' => $filepath, 'alt' => $alt, 'title' => $alt));
            }
            if (!empty($account->uid) && user_access('access user profiles')) {
                $attributes = array(
                    'attributes' => array('title' => t('View user profile.')),
                    'html' => TRUE,
                );
                $variables['user_picture'] = l($variables['user_picture'], "user/$account->uid", $attributes);
            }
        }
    }

}

/**
 * @param $theme_registry
 * An implementation of hook_theme_registry_alter() Substitute our own custom version of the standard 'theme_form_element' function. If the theme has overridden it, we'll be bypassed, but in most cases this will work nicely..
 */
function md_oldal_theme_registry_alter(&$theme_registry) {
    if (!empty($theme_registry['form_element'])) {
        $theme_registry['form_element']['function'] = 'md_oldal_form_element';
    }
}
/**
 * Returns HTML for a form element.
 *
 * Each form element is wrapped in a DIV container having the following CSS
 * classes:
 * - form-item: Generic for all form elements.
 * - form-type-#type: The internal element #type.
 * - form-item-#name: The internal form element #name (usually derived from the
 *   $form structure and set via form_builder()).
 * - form-disabled: Only set if the form element is #disabled.
 *
 * In addition to the element itself, the DIV contains a label for the element
 * based on the optional #title_display property, and an optional #description.
 *
 * The optional #title_display property can have these values:
 * - before: The label is output before the element. This is the default.
 *   The label includes the #title and the required marker, if #required.
 * - after: The label is output after the element. For example, this is used
 *   for radio and checkbox #type elements as set in system_element_info().
 *   If the #title is empty but the field is #required, the label will
 *   contain only the required marker.
 * - invisible: Labels are critical for screen readers to enable them to
 *   properly navigate through forms but can be visually distracting. This
 *   property hides the label for everyone except screen readers.
 * - attribute: Set the title attribute on the element to create a tooltip
 *   but output no label element. This is supported only for checkboxes
 *   and radios in form_pre_render_conditional_form_element(). It is used
 *   where a visual label is not needed, such as a table of checkboxes where
 *   the row and column provide the context. The tooltip will include the
 *   title and required marker.
 *
 * If the #title property is not set, then the label and any required marker
 * will not be output, regardless of the #title_display or #required values.
 * This can be useful in cases such as the password_confirm element, which
 * creates children elements that have their own labels and required markers,
 * but the parent element should have neither. Use this carefully because a
 * field without an associated label can cause accessibility challenges.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #title, #title_display, #description, #id, #required,
 *     #children, #type, #name.
 *
 * @ingroup themeable
 */
function md_oldal_form_element($variables) {
    $element = &$variables['element'];

    // This function is invoked as theme wrapper, but the rendered form element
    // may not necessarily have been processed by form_builder().
    $element += array(
        '#title_display' => 'before',
    );

    // Add element #id for #type 'item'.
    if (isset($element['#markup']) && !empty($element['#id'])) {
        $attributes['id'] = $element['#id'];
    }
    // Add element's #type and #name as class to aid with JS/CSS selectors.
    $attributes['class'] = array('form-item');
    if (!empty($element['#type'])) {
        $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
    }
    if (!empty($element['#name'])) {
        $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
    }
    // Add a class for disabled elements to facilitate cross-browser styling.
    if (!empty($element['#attributes']['disabled'])) {
        $attributes['class'][] = 'form-disabled';
    }
    $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

    // If #title is not set, we don't display any label or required marker.
    if (!isset($element['#title'])) {
        $element['#title_display'] = 'none';
    }
    $prefix = isset($element['#field_prefix']) ? '' . $element['#field_prefix'] . '' : '';
    $suffix = isset($element['#field_suffix']) ? '' . $element['#field_suffix'] . '' : '';

    switch ($element['#title_display']) {
        case 'before':
        case 'invisible':
            $output .= ' ' . theme('form_element_label', $variables);
            $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
            break;

        case 'after':
            $output .= ' ' . $prefix . $element['#children'] . $suffix;
            $output .= ' ' . theme('form_element_label', $variables) . "\n";
            break;

        case 'none':
        case 'attribute':
            // Output no label and no required marker, only the children.
            $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
            break;
    }

    if (!empty($element['#description'])) {
        $output .= '<div class="description">' . $element['#description'] . "</div>\n";
    }

    $output .= "</div>\n";

    return $output;
}
/**
 * Check file path upload in theme setting
 */
/*function md_oldal_theme_setting_check_path($path) {
    $path_scheme = file_uri_scheme($path);
    if ($path_scheme == 'public') {
        $return_path = file_create_url($path);
    } else if (($path_scheme == 'http') || ($path_scheme == 'https')) {
        $return_path = $path;
    } else {
        $return_path = file_create_url(file_build_uri($path));
    }
    return $return_path;
}*/
/**
 * Theme textfield
 */
function md_oldal_textfield($variables) {
    $element = $variables['element'];
    $element['#attributes']['type'] = 'text';
    element_set_attributes($element, array('id', 'name', 'value', 'size', 'maxlength'));
    _form_set_class($element, array('form-text '));

    $extra = '';
    if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
        drupal_add_library('system', 'drupal.autocomplete');
        $element['#attributes']['class'][] = 'form-autocomplete';

        $attributes = array();
        $attributes['type'] = 'hidden';
        $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
        $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
        $attributes['disabled'] = 'disabled';
        $attributes['class'][] = 'autocomplete';
        $extra = '<input' . drupal_attributes($attributes) . ' />';
    }

    $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

    return $output . $extra;
}
/**
 * Theme_button
 */
function md_oldal_button($variables) {
    $element = $variables['element'];
    $element['#attributes']['type'] = 'submit';
    element_set_attributes($element, array('id', 'name', 'value'));

    $element['#attributes']['class'][] = 'form-' . $element['#button_type'] . ' transition button';
    if (!empty($element['#attributes']['disabled'])) {
        $element['#attributes']['class'][] = 'form-button-disabled';
    }

    return '<input' . drupal_attributes($element['#attributes']) . ' />';
}

function md_oldal_preprocess_node(&$vars) {
  $view_mode = $vars['view_mode'];
  if ($view_mode == 'list') {
    
  }
  
  $node = $vars['node'];  
  if ($node->type == 'product' && $view_mode == 'list_view') {
    $items = $vars['content']['product:field_images']['#items'];
    $item = reset($items);
    if (isset($item['uri'])) {
      $vars['product_img'] = '<img src="' . file_create_url($item['uri']) . '" alt="img"/>';
    } else {
      $vars['product_img'] = '';
    }
  }
  
}

function md_oldal_form_webform_client_form_50_alter(&$form, &$form_state) {
	$form['submitted']['name']['#title_display'] = 'invisible';
	$form['submitted']['name']['#attributes'] = array('placeholder' => t('Name'), 'required' => 'required');
	$form['submitted']['name']['#attributes']['class'][] = 'form-control';
	$form['submitted']['name']['#prefix'] = '<div class="row"><div class="col-sm-6">';
	$form['submitted']['name']['#suffix'] = '</div></div>';
	
	$form['submitted']['email']['#title_display'] = 'invisible';
	$form['submitted']['email']['#attributes'] = array('placeholder' => t('E-mail'), 'required' => 'required');
	$form['submitted']['email']['#attributes']['class'][] = 'form-control';
	$form['submitted']['email']['#prefix'] = '<div class="row"><div class="col-sm-6">';
	$form['submitted']['email']['#suffix'] = '</div></div>';
	
	$form['submitted']['message']['#title_display'] = 'invisible';
	$form['submitted']['message']['#attributes'] = array('placeholder' => t('Message'), 'required' => 'required');
	$form['submitted']['message']['#attributes']['class'][] = 'form-control';
	$form['submitted']['message']['#prefix'] = '<div class="row"><div class="col-sm-8">';
	$form['submitted']['message']['#suffix'] = '</div></div>';
	
	$form['actions']['submit']['#attributes']['class'][] = 'btn btn-default';
}

function md_oldal_form_comment_node_blog_form_alter(&$form, &$form_state) {	
	unset($form['subject']);
	unset($form['actions']['preview']);
	
	$form['author']['name']['#attributes'] = array('placeholder' => t('Name'), 'required' => 'required');
	$form['author']['name']['#title_display'] = 'invisible';
	$form['author']['name']['#required'] = TRUE;
	$form['author']['name']['#attributes']['class'][] = 'form-control';
	$form['author']['name']['#prefix'] = '<div class="row"><div class="col-sm-6">';
	$form['author']['name']['#suffix'] = '</div></div>';
	
	$form['field_comment_email'][$form['language']['#value']][0]['email']['#attributes'] = array('placeholder' => t('Email'), 'required' => 'required');
	$form['field_comment_email'][$form['language']['#value']][0]['email']['#title_display'] = 'invisible';
	$form['field_comment_email'][$form['language']['#value']][0]['email']['#required'] = TRUE;
	$form['field_comment_email'][$form['language']['#value']][0]['email']['#attributes']['class'][] = 'form-control';
	$form['field_comment_email'][$form['language']['#value']][0]['email']['#prefix'] = '<div class="row"><div class="col-sm-6">';
	$form['field_comment_email'][$form['language']['#value']][0]['email']['#suffix'] = '</div></div>';
	
	$form['comment_body'][$form['language']['#value']][0]['value']['#attributes'] = array('placeholder' => t('Message ...'), 'required' => 'required');
	$form['comment_body'][$form['language']['#value']][0]['value']['#title_display'] = 'invisible';
	$form['comment_body'][$form['language']['#value']][0]['value']['#required'] = TRUE;
	$form['comment_body'][$form['language']['#value']][0]['value']['#attributes']['class'][] = 'form-control';
	$form['comment_body'][$form['language']['#value']][0]['value']['#prefix'] = '<div class="row"><div class="col-sm-8">';
	$form['comment_body'][$form['language']['#value']][0]['value']['#suffix'] = '</div></div>';
	
	$form['actions']['submit']['#value'] = 'Send message';
	$form['actions']['submit']['#attributes']['class'][] = 'btn btn-primary';
}

function md_oldal_form_commerce_cart_add_to_cart_form_alter(&$form, &$form_state) {	
	$form['attributes']['field_size']['#attributes']['class'][] = 'custom-select';
	$form['attributes']['field_size']['#prefix'] = '<div class="col-xs-12">';
	$form['attributes']['field_size']['#suffix'] = '</div>';
	
	$form['attributes']['field_color']['#attributes']['class'][] = 'custom-select';
	$form['attributes']['field_color']['#prefix'] = '<div class="col-xs-12">';
	$form['attributes']['field_color']['#suffix'] = '</div>';
	
	$form['quantity']['#attributes']['class'][] = 'form-control';
	$form['quantity']['#prefix'] = '<div class="col-xs-12 col-sm-4">';
	$form['quantity']['#suffix'] = '</div>';
	
	$form['submit']['#attributes']['class'][] = 'btn btn-default btn-block';
	$form['submit']['#prefix'] = '<div class="col-xs-12 col-sm-8">';
	$form['submit']['#suffix'] = '</div>';
}

function md_oldal_form_views_form_commerce_cart_form_default_alter(&$form, &$form_state) {
	
	$form['#prefix'] = '<section class="line pt-0"><div class="container"><div class="row"><div class="col-sm-12">';
	$form['#suffix'] = '</div></div></div></section>';
		
	$form['actions']['#prefix'] = '<section class="line pt-0"><div class="container"><div class="row"><div class="col-sm-4 align-right">';
	$form['actions']['#suffix'] = '</div></div></div></section>';
	
	$form['actions']['submit']['#attributes']['class'][] = 'btn btn-primary cart-button';
	$form['actions']['checkout']['#attributes']['class'][] = 'btn btn-primary cart-button';
}

function md_oldal_form_commerce_checkout_form_checkout_alter(&$form, &$form_state) {	
	$form['#prefix'] = '<section class="line pt-0"><div class="container"><div class="row"><div class="col-sm-12">';
	$form['#suffix'] = '</div></div></div></section>';
	
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['name_block']['#prefix'] = '<section class="line pt-2"><div class="col-sm-4">';
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['name_block']['#suffix'] = '</div></section>';
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['name_block']['name_line']['#attributes']['class'][] = 'form-control';
	
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['country']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['country']['#suffix'] = '</div></section>';
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['country']['#attributes']['class'][] = 'custom-select';
	
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['street_block']['thoroughfare']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['street_block']['thoroughfare']['#suffix'] = '</div></section>';
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['street_block']['thoroughfare']['#attributes']['class'][] = 'form-control';
	
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['street_block']['premise']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['street_block']['premise']['#suffix'] = '</div></section>';
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['street_block']['premise']['#attributes']['class'][] = 'form-control';
	
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['locality_block']['locality']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['locality_block']['locality']['#suffix'] = '</div></section>';
	$form['customer_profile_billing']['commerce_customer_address']['und'][0]['locality_block']['locality']['#attributes']['class'][] = 'form-control';
	
	$form['account']['login']['mail']['#prefix'] = '<section class="line pt-2"><div class="col-sm-4">';
	$form['account']['login']['mail']['#suffix'] = '</div></section>';
	$form['account']['login']['mail']['#attributes']['class'][] = 'form-control';
	
	unset($form['customer_profile_billing']['commerce_customer_address']['und'][0]['locality_block']['administrative_area']);
	unset($form['customer_profile_billing']['commerce_customer_address']['und'][0]['locality_block']['postal_code']);
	
	$form['buttons']['#prefix'] = '<section class="line pt-4"><div class="col-sm-6">';
	$form['buttons']['#suffix'] = '</div></section>';
	
	$form['buttons']['continue']['#attributes']['class'][] = 'btn btn-primary';
	$form['buttons']['cancel']['#attributes']['class'][] = 'btn btn-primary btn-default';
}

function md_oldal_form_commerce_checkout_form_review_alter(&$form, &$form_state) {
	$form['#prefix'] = '<section class="line"><div class="container"><div class="row"><div class="col-sm-12">';
	$form['#suffix'] = '</div></div></div></section>';
	
	$form['commerce_payment']['payment_method']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['commerce_payment']['payment_method']['#suffix'] = '</div></section>';
	
	$form['commerce_payment']['payment_details']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['commerce_payment']['payment_details']['#suffix'] = '</div></section>';
	
	$form['commerce_payment']['payment_details']['credit_card']['number']['#attributes']['class'][] = 'form-control';
	
	$form['commerce_payment']['payment_details']['credit_card']['exp_month']['#attributes']['class'][] = 'custom-select';
	$form['commerce_payment']['payment_details']['credit_card']['exp_year']['#attributes']['class'][] = 'custom-select';
	
	$form['buttons']['continue']['#attributes']['class'][] = 'btn btn-primary';
	$form['buttons']['back']['#attributes']['class'][] = 'btn btn-primary btn-default';
}

function md_oldal_form_commerce_checkout_form_complete_alter(&$form, &$form_state) {
	$form['#prefix'] = '<section class="line"><div class="container"><div class="row"><div class="col-sm-12">';
	$form['#suffix'] = '</div></div></div></section>';
}

function md_oldal_form_user_login_alter(&$form, &$form_state) {
	$form['#prefix'] = '<section class="line"><div class="container"><div class="row"><div class="col-sm-12">';
	$form['#suffix'] = '</div></div></div></section>';
	
	$form['name']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['name']['#suffix'] = '</div></section>';
	$form['name']['#attributes']['class'][] = 'form-control';
	
	$form['pass']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['pass']['#suffix'] = '</div></section>';
	$form['pass']['#attributes']['class'][] = 'form-control';
	
	$form['actions']['submit']['#attributes']['class'][] = 'btn btn-default';
	$form['actions']['submit']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['actions']['submit']['#suffix'] = '</div></section>';
}

function md_oldal_form_user_register_form_alter(&$form, &$form_state) {
	$form['#prefix'] = '<section class="line"><div class="container"><div class="row"><div class="col-sm-12">';
	$form['#suffix'] = '</div></div></div></section>';
	
	$form['account']['name']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['account']['name']['#suffix'] = '</div></section>';
	$form['account']['name']['#attributes']['class'][] = 'form-control';
	
	$form['account']['mail']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['account']['mail']['#suffix'] = '</div></section>';
	$form['account']['mail']['#attributes']['class'][] = 'form-control';
	
	$form['actions']['submit']['#attributes']['class'][] = 'btn btn-default';
	$form['actions']['submit']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['actions']['submit']['#suffix'] = '</div></section>';
}

function md_oldal_form_user_pass_alter(&$form, &$form_state) {
	$form['#prefix'] = '<section class="line"><div class="container"><div class="row"><div class="col-sm-12">';
	$form['#suffix'] = '</div></div></div></section>';
	
	$form['name']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['name']['#suffix'] = '</div></section>';
	$form['name']['#attributes']['class'][] = 'form-control';
	
	$form['actions']['submit']['#attributes']['class'][] = 'btn btn-default';
	$form['actions']['submit']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['actions']['submit']['#suffix'] = '</div></section>';

}

function md_oldal_form_comment_form_alter(&$form, &$form_state) {	
	unset($form['subject']);
	unset($form['actions']['preview']);
	
	$form['author']['name']['#attributes'] = array('placeholder' => t('Name'), 'required' => 'required');
	$form['author']['name']['#title_display'] = 'invisible';
	$form['author']['name']['#required'] = TRUE;
	$form['author']['name']['#attributes']['class'][] = 'form-control';
	$form['author']['name']['#prefix'] = '<div class="row"><div class="col-sm-6">';
	$form['author']['name']['#suffix'] = '</div></div>';
	
	$form['field_comment_email'][$form['language']['#value']][0]['email']['#attributes'] = array('placeholder' => t('Email'), 'required' => 'required');
	$form['field_comment_email'][$form['language']['#value']][0]['email']['#title_display'] = 'invisible';
	$form['field_comment_email'][$form['language']['#value']][0]['email']['#required'] = TRUE;
	$form['field_comment_email'][$form['language']['#value']][0]['email']['#attributes']['class'][] = 'form-control';
	$form['field_comment_email'][$form['language']['#value']][0]['email']['#prefix'] = '<div class="row"><div class="col-sm-6">';
	$form['field_comment_email'][$form['language']['#value']][0]['email']['#suffix'] = '</div></div>';
	
	$form['comment_body'][$form['language']['#value']][0]['value']['#attributes'] = array('placeholder' => t('Message ...'), 'required' => 'required');
	$form['comment_body'][$form['language']['#value']][0]['value']['#title_display'] = 'invisible';
	$form['comment_body'][$form['language']['#value']][0]['value']['#required'] = TRUE;
	$form['comment_body'][$form['language']['#value']][0]['value']['#attributes']['class'][] = 'form-control';
	$form['comment_body'][$form['language']['#value']][0]['value']['#prefix'] = '<div class="row"><div class="col-sm-8">';
	$form['comment_body'][$form['language']['#value']][0]['value']['#suffix'] = '</div></div>';
	
	$form['actions']['submit']['#value'] = 'Send message';
	$form['actions']['submit']['#attributes']['class'][] = 'btn btn-primary';
	$form['actions']['submit']['#prefix'] = '<section class="line pt-2">';
	$form['actions']['submit']['#suffix'] = '</section>';
}

function md_oldal_form_search_form_alter(&$form, &$form_state) {	
	$form['#prefix'] = '<section class="line pt-1"><div class="container"><div class="row"><div class="col-sm-12">';
	$form['#suffix'] = '</div></div></div></section>';
	
	$form['basic']['keys']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['basic']['keys']['#suffix'] = '</div></section>';
	$form['basic']['keys']['#attributes']['class'][] = 'form-control';
	
	$form['basic']['submit']['#attributes']['class'][] = 'btn btn-default';
	$form['basic']['submit']['#prefix'] = '<section class="line pt-3"><div class="col-sm-4">';
	$form['basic']['submit']['#suffix'] = '</div></section>';
	
	$form['advanced']['keywords']['or']['#prefix'] = '<section class="line pt-3">';
	$form['advanced']['keywords']['or']['#suffix'] = '</section>';
	$form['advanced']['keywords']['or']['#attributes']['class'][] = 'form-control';
	
	$form['advanced']['keywords']['phrase']['#prefix'] = '<section class="line pt-3">';
	$form['advanced']['keywords']['phrase']['#suffix'] = '</section>';
	$form['advanced']['keywords']['phrase']['#attributes']['class'][] = 'form-control';
	
	$form['advanced']['keywords']['negative']['#prefix'] = '<section class="line pt-3">';
	$form['advanced']['keywords']['negative']['#suffix'] = '</section>';
	$form['advanced']['keywords']['negative']['#attributes']['class'][] = 'form-control';
	
	$form['advanced']['submit']['#attributes']['class'][] = 'btn btn-default';
	$form['advanced']['submit']['#prefix'] = '<section class="line pt-3">';
	$form['advanced']['submit']['#suffix'] = '</section>';
	
	$form['advanced']['#prefix'] = '<section class="line pt-1"><div class="col-sm-4">';
	$form['advanced']['#suffix'] = '</div></section>';
}

function md_oldal_theme() {
    return array(
        'contact_site_form' => array(
        'render element'    => 'form',
        'template'          => 'contact-site-form',
        'path'              => drupal_get_path('theme', 'md_oldal').'/template',
        ),
    );  
}

function md_oldal_preprocess_contact_site_form(&$vars) {
    
    $form["mail"]["#title"] = t("Email Address");
    
    $vars["preamble"] = t("<h1>Contact Us</h1>
            <p>
                Please feel free to contact us if you have any questions or concerns. 
                The majority of our business has been built on word of mouth advertising; customer satisfaction is our prime concern.
            </p>
            <p>
                E-mail is the preferred way of contacting us, but you can also call at <strong>403-934-2755</strong> with any questions or concerns.
            </p>
            <p>Required<sup class=\"red-text\">*</sup></p>");
    
            $vars["form"]['name'] ["#title"]= t("Name");
            $vars["form"]['mail']["#title"] = t("Email Address");
    
    
    $vars['children'] = drupal_render_children($vars['form']);
} 