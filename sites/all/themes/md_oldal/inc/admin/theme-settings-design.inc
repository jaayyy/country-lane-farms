<?php

/**
 * @file
 * Theme settings .
 */

function md_oldal_theme_settings_design(&$form, &$form_state) {
    global $base_url;
	$form['md_oldal_settings']['design'] = array(
		'#type' 					=> 'fieldset',
		'#weight' 				=> -4,
		'#prefix'  				=> '<div id="md-design" class="md-tabcontent clearfix">',
		'#suffix'        => '</div><!-- / #md-design -->',
	);
	
	$form['md_oldal_settings']['design']['design_htmllist'] = array(
		'#markup' 				=> '<div id="md-content-sidebar" class="md-content-sidebar">
                                        <ul class="clearfix">
                                            <li><a href="#ds-sidebar"><i class="fa fa-th-list"></i>Sidebar</a></li>
                                            <li><a href="#ds-social"><i class="fa fa-share"></i>Social Icons</a></li>
                                            <li><a href="#ds-footer"><i class="fa fa-question"></i>Footer</a></li>
                                        </ul>
                                    </div><!-- /.md-content-sidebar -->
			                        <div class="md-content-main">',
		'#weight' 				=> -15,
	);
	$form['md_oldal_settings']['design']['design_htmllistclose'] = array(
		'#markup' 				=> '</div><!-- /.md-listleft -->',
		'#weight' 				=> 15,
	);
    $form['md_oldal_settings']['design']['design_skin'] = array(
        '#type' 					=> 'fieldset',
        '#weight' 				=> 1,
        '#prefix'  				=> '<div id="ds-skin">
                                        <div class="md-tabcontent-row">',
        '#suffix'               => '    </div>
                                    </div><!-- / #ds-general -->',
    );

/* ==================================================================================
================================= Sidebar ===========================================
====================================================================================*/
    $form['md_oldal_settings']['design']['design_sidebar'] = array(
        '#type'             => 'fieldset',
        '#weight' 				=> -5,
        '#prefix'  				=> '<div id="ds-sidebar">',
        '#suffix'        => '</div><!-- / #ds-block -->',
    );
    $form['md_oldal_settings']['design']['design_sidebar']['sidebar_position'] = array(
        '#type'          => 'select',
        '#default_value' => theme_get_setting('sidebar_position','md_oldal') ? theme_get_setting('sidebar_position','md_oldal') : 'right',
        '#options'       => array(
            'no'        => t('No sidebar'),
            'left'      => t('Left'),
            'right' 	=> t('Right'),
        ),
        '#attributes'           => array(
            'class'             => array('select')
        ),
        '#prefix'                     => '<h3 class="md-tabcontent-title">Sidebar position</h3><div class="form-group">',
        '#suffix'                     => '</div>',
        '#field_prefix'               => '<div class="md-selection medium">',
        '#field_suffix'               => '</div>'
    );

   //  ---------------------------------- Social Information --------------------------------------------
    $form['md_oldal_settings']['design']['design_social'] = array(
        '#type'                     => 'fieldset',
        '#weight'                   => -8,
        '#prefix'                   => '<div id="ds-social">',
        '#suffix'                   => '</div><!-- / #ds-block -->',
    );
    $form['md_oldal_settings']['design']['design_social']['footer_social'] = array(
        '#type'                     => 'fieldset',
        '#prefix'                   => '<div id="footer-social"><h3 class="md-tabcontent-title">Custom Social Account</h3>',
        '#suffix'                   => '</div>',
    );
    $form['md_oldal_settings']['design']['design_social']['footer_social']['social_acc_wrapper'] = array(
        '#type'                     => 'fieldset',
        '#prefix'                   => '<div id="ft-social-acc-wrapper">',
        '#suffix'                   => '</div>',
        '#attributes'               => array(
            'class'                 => array('ft-social-acc-wrapper'),
        )
    );
    $max_num = theme_get_setting('ft_social_max_num','md_oldal') ? theme_get_setting('ft_social_max_num','md_oldal') : 1;
    $form['md_oldal_settings']['design']['design_social']['footer_social']['social_acc_wrapper']['ft_popup_add_wrapper'] = array(
        '#markup'                   => '<a class="add-more md-button" data-preview="ft_social_preview" data-max-num="'.$max_num.'" href="#ft-social-sortable-no'.$max_num.'">Add More</a>',
    );
    $form['md_oldal_settings']['design']['design_social']['footer_social']['social_acc_wrapper']['ft_popup_close_wrapper'] = array(
        '#markup'                   => '</div>'
    );
    $form['md_oldal_settings']['design']['design_social']['footer_social']['social_acc_wrapper']['ft_social_preview'] = array(
        '#type' => 'fieldset',
        '#prefix'   => '<div id="ft_social_preview" class="data-container"><div class="popup-wrapper"></div>',
        '#sufix'    => '</div>'
    );
    $form['md_oldal_settings']['design']['design_social']['footer_social']['social_acc_wrapper']['ft_social_preview']['ft_social_order'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
            'class'    => 'hidden-order'
        ),
    );
    $form['md_oldal_settings']['design']['design_social']['footer_social']['social_acc_wrapper']['ft_social_preview']['ft_social_max_num'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
            'class'    => 'hidden-num'
        ),
        '#default_value' => theme_get_setting('ft_social_max_num','md_oldal') ? theme_get_setting('ft_social_max_num','md_oldal') : 1,
    );
    $form['md_oldal_settings']['design']['design_social']['footer_social']['social_acc_wrapper']['ft_social_preview']["ft_social_preview_openhtml"] = array(
        '#markup'   => '<ul id="ft_social_sortable" class="sortable">'
    );
    if(theme_get_setting('ft_social','md_oldal') == null) {
        $ft_social = array(
            'ft_social_sortable_no1' => array(
                'icon' => array(
                    'icon' => '',
                    'bundle' => ''
                ),
                'link'  => ''
            )
        );
    } else {
        $ft_social = theme_get_setting('ft_social','md_oldal');
    }

    foreach($ft_social as $key => $value){
        $explode = explode("_",$key);
        end($explode);
        $num = current($explode);
        $social_icon = $ft_social[$key]['icon'];
        $form['md_oldal_settings']['design']['design_social']['footer_social']['social_acc_wrapper']['ft_social_preview'][$key.'_openhtml'] = array(
            '#markup' => '<li id="ft-social-sortable-'.$num.'" class="draggable-item sortable-item toggle-item" data-num="'.substr($num,2).'" data-id="ft-social-sortable-no'.substr($num,2).'"><a href="#" class="md-remove">X</a>'
        );
        $form['md_oldal_settings']['design']['design_social']['footer_social']['social_acc_wrapper']['ft_social_preview'][$key]['ft_social_icon_'.$num] = array(
            '#type' => 'icon_selector',
            '#title'    => 'Social Icon',
            '#default_bundle' => isset($social_icon['bundle']) ? $social_icon['bundle'] : '',
            '#default_icon' => isset($social_icon['icon']) ? $social_icon['icon'] : '',
            '#prefix'               => '<div class="form-group icon-picker">',
            '#suffix'               => '</div>',
        );

        $form['md_oldal_settings']['design']['design_social']['footer_social']['social_acc_wrapper']['ft_social_preview'][$key]['ft_social_link_'.$num] = array(
            '#type' => 'textfield',
            '#title'    => 'Social Link',
            '#attributes' => array(
                'class' => array('input-border'),
            ),
            '#default_value' => $ft_social[$key]['link'],
            '#maxlength' => 1000,
            '#prefix'               => '<div class="form-group">',
            '#suffix'               => '</div>'
        );
        $form['md_oldal_settings']['design']['design_social']['footer_social']['social_acc_wrapper']['ft_social_preview'][$key.'_closehtml'] = array(
            '#markup' => '</li>'
        );
    }
    $form['md_oldal_settings']['design']['design_social']['footer_social']['social_acc_wrapper']['ft_social_preview']["ft_social_preview_closehtml"] = array(
        '#markup'   => '</ul>'
    );


   //  ---------------------------------- Footer --------------------------------------------
    $form['md_oldal_settings']['design']['design_footer'] = array(
        '#type'                     => 'fieldset',
        '#weight' 				    => -8,
        '#prefix'  				    => '<div id="ds-footer">',
        '#suffix'                   => '</div><!-- / #ds-block -->',
    );
    
    $form['md_oldal_settings']['design']['design_footer']['footer_style'] = array(
        '#type' => 'fieldset',
        '#prefix'   => '<div id="ft-style-wrapper">',
        '#suffix'   => '</div>'
    );

    //  ---------------------------------- Contact Information --------------------------------------------


    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info'] = array(
        '#type' => 'fieldset',
        '#prefix'   => '<div id="contact-info"><h3 class="md-tabcontent-title">Contact Infomation for Style 1</h3>',
        '#suffix'   => '</div>',
    );
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info']['_wrapper'] = array(
        '#type' => 'fieldset',
        '#prefix'   => '<div id="contact-info-wrapper">',
        '#suffix'   => '</div>',
        '#attributes'   => array(
            'class' => array('contact-info-wrapper'),
        )
    );
    $max_num = theme_get_setting('contact_info_max_num','md_oldal') ? theme_get_setting('contact_info_max_num','md_oldal') : 1;
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info']['contact_info_wrapper']['contact_popup_add_wrapper'] = array(
        '#markup'   => '<a class="add-more md-button" data-preview="contact_info_preview" data-max-num="'.$max_num.'" href="#contact-info-sortable-no'.$max_num.'">Add More</a>',
    );
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info']['contact_info_wrapper']['contact_popup_close_wrapper'] = array(
        '#markup' => '</div>'
    );
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info']['contact_info_wrapper']['contact_info_preview'] = array(
        '#type' => 'fieldset',
        '#prefix'   => '<div id="contact_info_preview" class="data-container"><div class="popup-wrapper"></div>',
        '#sufix'    => '</div>'
    );
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info']['contact_info_wrapper']['contact_info_preview']['contact_info_order'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
            'class'    => 'hidden-order'
        ),
    );
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info']['contact_info_wrapper']['contact_info_preview']['contact_info_max_num'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
            'class'    => 'hidden-num'
        ),
        '#default_value' => theme_get_setting('contact_info_max_num','md_oldal') ? theme_get_setting('contact_info_max_num','md_oldal') : 1,
    );
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info']['contact_info_wrapper']['contact_info_preview']["contact_info_preview_openhtml"] = array(
        '#markup'   => '<ul id="contact_info_sortable" class="sortable">'
    );
    if(theme_get_setting('contact_info','md_oldal') == null) {
        $contact_info = array(
            'contact_info_sortable_no1' => array(
                'icon' => array(
                    'icon' => '',
                    'bundle' => ''
                ),
                'detail'  => ''
            )
        );
    } else {
        $contact_info = theme_get_setting('contact_info','md_oldal');
    }

    foreach($contact_info as $key => $value){
        $explode = explode("_",$key);
        end($explode);
        $num = current($explode);
        $info_icon = $contact_info[$key]['icon'];
        $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info']['contact_info_wrapper']['contact_info_preview'][$key.'_openhtml'] = array(
            '#markup' => '<li id="contact-info-sortable-'.$num.'" class="draggable-item sortable-item toggle-item" data-num="'.substr($num,2).'" data-id="contact-info-sortable-no'.substr($num,2).'"><a href="#" class="md-remove">X</a>'
        );
        $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info']['contact_info_wrapper']['contact_info_preview'][$key]['contact_info_icon_'.$num] = array(
            '#type' => 'icon_selector',
            '#title'    => 'Contact Icon',
            '#default_bundle' => isset($info_icon['bundle']) ? $info_icon['bundle'] : '',
            '#default_icon' => isset($info_icon['icon']) ? $info_icon['icon'] : '',
            '#prefix'               => '<div class="form-group icon-picker">',
            '#suffix'               => '</div>',
        );

        $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info']['contact_info_wrapper']['contact_info_preview'][$key]['contact_info_detail_'.$num] = array(
            '#type' => 'textfield',
            '#title'    => 'Detail',
            '#attributes' => array(
                'class' => array('input-border'),
            ),
            '#default_value' => $contact_info[$key]['detail'],
            '#maxlength' => 1000,
            '#prefix'               => '<div class="form-group">',
            '#suffix'               => '</div>'
        );
        $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info']['contact_info_wrapper']['contact_info_preview'][$key.'_closehtml'] = array(
            '#markup' => '</li>'
        );
    }
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info']['contact_info_wrapper']['contact_info_preview']["contact_info_preview_closehtml"] = array(
        '#markup'   => '</ul>'
    );



    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info_style_2'] = array(
        '#type' => 'fieldset',
        '#prefix'   => '<div id="contact-info-2"><h3 class="md-tabcontent-title">Contact Infomation for Style 2</h3>',
        '#suffix'   => '</div>',
    );
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info_style_2']['_wrapper'] = array(
        '#type' => 'fieldset',
        '#prefix'   => '<div id="contact-info-wrapper-2">',
        '#suffix'   => '</div>',
        '#attributes'   => array(
            'class' => array('contact-info-wrapper'),
        )
    );
    $max_num_style_2 = theme_get_setting('contact_info_style_2_max_num','md_oldal') ? theme_get_setting('contact_info_style_2_max_num','md_oldal') : 1;
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info_style_2']['contact_info_style_2_wrapper']['contact_popup_add_style_2_wrapper'] = array(
        '#markup'   => '<a class="add-more md-button" data-preview="contact_info_style_2_preview" data-max-num="'.$max_num_style_2.'" href="#contact-info-style-2-sortable-no'.$max_num_style_2.'">Add More</a>',
    );
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info_style_2']['contact_info_style_2_wrapper']['contact_popup_close_style_2_wrapper'] = array(
        '#markup' => '</div>'
    );
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info_style_2']['contact_info_style_2_wrapper']['contact_info_style_2_preview'] = array(
        '#type' => 'fieldset',
        '#prefix'   => '<div id="contact_info_style_2_preview" class="data-container"><div class="popup-wrapper"></div>',
        '#sufix'    => '</div>'
    );
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info_style_2']['contact_info_style_2_wrapper']['contact_info_style_2_preview']['contact_info_style_2_order'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
            'class'    => 'hidden-order'
        ),
    );
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info_style_2']['contact_info_style_2_wrapper']['contact_info_style_2_preview']['contact_info_style_2_max_num'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
            'class'    => 'hidden-num'
        ),
        '#default_value' => theme_get_setting('contact_info_style_2_max_num','md_oldal') ? theme_get_setting('contact_info_style_2_max_num','md_oldal') : 1,
    );
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info_style_2']['contact_info_style_2_wrapper']['contact_info_style_2_preview']["contact_info_style_2_preview_openhtml"] = array(
        '#markup'   => '<ul id="contact_info_style_2_sortable" class="sortable">'
    );
    if(theme_get_setting('contact_info_style_2','md_oldal') == null) {
        $contact_info = array(
            'contact_info_style_2_sortable_no1' => array(
                'icon' => array(
                    'icon' => '',
                    'bundle' => ''
                ),
                'detail'  => ''
            )
        );
    } else {
        $contact_info = theme_get_setting('contact_info_style_2','md_oldal');
    }

    foreach($contact_info as $key => $value){
        $explode = explode("_",$key);
        end($explode);
        $num = current($explode);
        $info_icon = $contact_info[$key]['icon'];
        $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info_style_2']['contact_info_style_2_wrapper']['contact_info_style_2_preview'][$key.'_openhtml'] = array(
            '#markup' => '<li id="contact-info-style-2-sortable-'.$num.'" class="draggable-item sortable-item toggle-item" data-num="'.substr($num,2).'" data-id="contact-info-style-2-sortable-no'.substr($num,2).'"><a href="#" class="md-remove">X</a>'
        );
        $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info_style_2']['contact_info_style_2_wrapper']['contact_info_style_2_preview'][$key]['contact_info_style_2_icon_'.$num] = array(
            '#type' => 'icon_selector',
            '#title'    => 'Contact Icon',
            '#default_bundle' => isset($info_icon['bundle']) ? $info_icon['bundle'] : '',
            '#default_icon' => isset($info_icon['icon']) ? $info_icon['icon'] : '',
            '#prefix'               => '<div class="form-group icon-picker">',
            '#suffix'               => '</div>',
        );

        $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info_style_2']['contact_info_style_2_wrapper']['contact_info_style_2_preview'][$key]['contact_info_style_2_detail_'.$num] = array(
            '#type' => 'textfield',
            '#title'    => 'Detail',
            '#attributes' => array(
                'class' => array('input-border'),
            ),
            '#default_value' => $contact_info[$key]['detail'],
            '#maxlength' => 1000,
            '#prefix'               => '<div class="form-group">',
            '#suffix'               => '</div>'
        );
        $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info_style_2']['contact_info_style_2_wrapper']['contact_info_style_2_preview'][$key.'_closehtml'] = array(
            '#markup' => '</li>'
        );
    }
    $form['md_oldal_settings']['design']['design_footer']['display_contact']['contact_info_style_2']['contact_info_style_2_wrapper']['contact_info_style_2_preview']["contact_info_style_2_preview_closehtml"] = array(
        '#markup'   => '</ul>'
    );



    $form['md_oldal_settings']['design']['design_footer']['footer_style']['footer_options'] = array(
        '#type'                 => 'select',
        '#options'              => array(
            'style1'            => 'Footer style 1',
            'style2'            => 'Footer style 2',
        ),
        '#default_value'        => theme_get_setting('footer_options','md_oldal'),
        '#attributes'           => array(
            'class'             => array('select')
        ),
        '#prefix'               => '<h3 class="md-tabcontent-title">Footer style</h3>
                                            <div class="form-group">',
        '#suffix'               => '</div>',
        '#field_prefix'         => '<div class="md-selection medium">',
        '#field_suffix'         => '</div>'
    );


    $form['md_oldal_settings']['design']['design_footer']['footer_content_wrapper'] = array(
        '#type' => 'fieldset',
        '#prefix'   => '<div id="ft-content-wrapper">',
        '#suffix'   => '</div>'
    );
    $form['md_oldal_settings']['design']['design_footer']['footer_content_wrapper']['footer_text'] = array(
        '#type' => 'textarea',
        '#title'    => '<h3 class="md-tabcontent-title">'.t('Footer Text Display for Style 1').'</h3>',
        '#resizable'    => false,
        '#default_value'    => theme_get_setting('footer_text','md_oldal') ? theme_get_setting('footer_text','md_oldal') : 'Oldal - Megadrupal Theme - ©2014 - All rights reserved.',
        '#attributes'       => array(
            'class'         => array('textarea-border big')
        )
    );
	$form['md_oldal_settings']['design']['design_footer']['footer_style']['footer_overlay'] = array(
        '#type' => 'checkbox',
        '#default_value' => theme_get_setting('footer_overlay' , 'md_oldal'),
        '#field_suffix' => '<label class="label-checkbox" for="edit-footer-overlay">Enable Overlaid Footer</label>',
        '#attributes'   => array(
            'class' => array('input-checkbox')
        ),
        '#prefix'   =>  '<div class="form-elements">',
        '#suffix'   => '</div>',
    );

    $form['md_oldal_settings']['design']['design_footer']['footer_content_style_2_wrapper'] = array(
        '#type' => 'fieldset',
        '#prefix'   => '<div id="ft-content-wrapper">',
        '#suffix'   => '</div>'
    );
    $form['md_oldal_settings']['design']['design_footer']['footer_content_style_2_wrapper']['footer_style_2_text'] = array(
        '#type' => 'textarea',
        '#title'    => '<h3 class="md-tabcontent-title">'.t('Footer Text Display for Style 2').'</h3>',
        '#resizable'    => false,
        '#default_value'    => theme_get_setting('footer_style_2_text','md_oldal') ? theme_get_setting('footer_style_2_text','md_oldal') : 'Oldal - Megadrupal Theme - ©2014 - All rights reserved.',
        '#attributes'       => array(
            'class'         => array('textarea-border big')
        )
    );
    
}