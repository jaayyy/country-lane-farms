<?php

/**
 * @file
 * Theme settings .
 */

function md_oldal_theme_settings_display(&$form, &$form_state) {
	$form['md_oldal_settings']['display'] = array(
		'#type' 			    => 'fieldset',
		'#weight' 				=> -1,
		'#prefix'  				=> '<div id="md-display" class="md-tabcontent clearfix">',
		'#suffix'               => '</div><!-- / #md-display -->',
	);

	$form['md_oldal_settings']['display']['display_htmllist'] = array(
		'#markup' 				=> '<div id="md-content-sidebar" class="md-content-sidebar">
                                        <ul class="clearfix">
                                            <li><a href="#s-map"><i class="fa fa-map-marker"></i>Map</a></li>
                                            <li><a href="#s-instagram"><i class="fa fa-instagram"></i>Instagram</a></li>
                                            <li><a href="#s-flickr"><i class="fa fa-flickr"></i>Flickr</a></li>
                                        </ul>
                                    </div><!-- /.md-content-sidebar -->
			                        <div class="md-content-main">',
		'#weight' 				=> -15,
	);

	$form['md_oldal_settings']['display']['display_htmllistclose'] = array(
		'#markup' 				=> '</div><!-- /.md-listleft -->',
		'#weight' 				=> 15,
	);
////////////////////////////////////////////////////// Google map //////////////////////////////////////////////////////
    $form['md_oldal_settings']['display']['map'] = array(
        '#type'                 => 'fieldset',
        '#weight' 				=> 1,
        '#prefix'  				=> '<div id="s-map">',
        '#suffix'               => '</div><!-- / #footer -->',
    );
    $form['md_oldal_settings']['display']['map']['map_address'] = array(
        '#type' 				=> 'textfield',
        '#default_value' 	    =>  theme_get_setting('map_address','md_oldal') ? theme_get_setting('map_address','md_oldal') : t('482 Broome St New York, NY 10013'),
        '#prefix'               => '<div class="md-tabcontent-header">
                                            <h3 class="md-tabcontent-title">Address</h3>
                                        </div><!-- /.md-row-description -->
                                        <div class="form-group">',
        '#suffix'               => '</div>',
        '#attributes'           => array(
            'class'             => array('input-border big')
        )
    );
    $form['md_oldal_settings']['display']['map']['map_types']   = array(
        '#type'                 => 'select',
        '#options'              => array(
            'ROADMAP'           => t('Road Map'),
            'SATELINE'          => t('Sate Line'),
            'HYBRID'            => t('Hybrid'),
            'TERRAIN'           => t('Terrain'),
        ),
        '#default_value'        => theme_get_setting('map_types','md_oldal') ? theme_get_setting('map_types','md_oldal') : t('ROADMAP'),
        '#prefix'               => '<div class="md-tabcontent-header">
                                            <h3 class="md-tabcontent-title">Map types</h3>
                                        </div><!-- /.md-row-description -->
                                        <div class="form-group">',
        '#suffix'               => '</div>',
        '#attributes'           => array(
            'class'             => array('select')
        ),
        '#field_prefix'         => '<div class="md-selection medium">',
        '#field_suffix'         => '</div>'
    );
    $form['md_oldal_settings']['display']['map']['map_zoom_level'] = array(
        '#type'                 => 'select',
        '#options'              => array(
            '14'                => '14',
            '15'                => '15',
            '16'                => '16',
            '17'                => '17',
            '18'                => '18'
        ),
        '#default_value'        => theme_get_setting('map_zoom_level','md_oldal') ? theme_get_setting('map_zoom_level','md_oldal') : '17',
        '#prefix'               => '<div class="md-tabcontent-header">
                                            <h3 class="md-tabcontent-title">Map Zoom Level</h3>
                                        </div><!-- /.md-row-description -->
                                        <div class="form-group">',
        '#suffix'               => '</div>',
        '#attributes'           => array(
            'class'             => array('select')
        ),
        '#field_prefix'         => '<div class="md-selection small">',
        '#field_suffix'         => '</div>',
    );


//////////////////////////////////////////// Instagram ///////////////////////////////////////////////////////
    $form['md_oldal_settings']['display']['instagram'] = array(
        '#type'                 => 'fieldset',
        '#weight'               => 1,
        '#prefix'               => '<div id="s-instagram"><h3 class="md-tabcontent-title">Instagram</h3>',
        '#suffix'               => '</div><!-- / #footer -->',
    );
    $form['md_oldal_settings']['display']['instagram']['instagram_enable'] = array(
        '#type' => 'checkbox',
        '#default_value' => theme_get_setting('instagram_enable' , 'md_oldal'),
        '#field_suffix' => '<label class="label-checkbox" for="edit-instagram-enable">Enable Instagram</label>',
        '#attributes'   => array(
            'class' => array('input-checkbox')
        ),
        '#prefix'   =>  '<div class="form-elements">',
        '#suffix'   => '</div>',
    );
    $form['md_oldal_settings']['display']['instagram']['instagram_token'] = array(
        '#type' => 'textfield',
        '#title'    => 'Access Token',
        '#attributes' => array(
            'class' => array('input-border'),
        ),
        '#default_value' => theme_get_setting('instagram_token' , 'md_oldal'),
        '#prefix'               => '<div class="form-group">',
        '#suffix'               => '</div>',
        '#description'          =>  t('This a mandatory setting that allows you to specify a user token'),
    );
    $form['md_oldal_settings']['display']['instagram']['instagram_uid'] = array(
        '#type' => 'textfield',
        '#title'    => 'User ID',
        '#attributes' => array(
            'class' => array('input-border'),
        ),
        '#default_value' => theme_get_setting('instagram_uid' , 'md_oldal'),
        '#prefix'               => '<div class="form-group">',
        '#suffix'               => '</div>',
        '#description'          =>  t('Enter your user ID on Instagram'),
    );
    $form['md_oldal_settings']['display']['instagram']['instagram_images'] = array(
        '#type' => 'textfield',
        '#title'    => 'Number of images',
        '#attributes' => array(
            'class' => array('input-border'),
        ),
        '#default_value' => theme_get_setting('instagram_images' , 'md_oldal'),
        '#prefix'               => '<div class="form-group">',
        '#suffix'               => '</div>',
        '#description'          =>  t('Enter number of images to display'),
    );

//////////////////////////////////////////// Flickr ///////////////////////////////////////////////////////
    $form['md_oldal_settings']['display']['flickr'] = array(
        '#type'                 => 'fieldset',
        '#weight'               => 1,
        '#prefix'               => '<div id="s-flickr"><h3 class="md-tabcontent-title">Flickr</h3>',
        '#suffix'               => '</div><!-- / #footer -->',
    );
    $form['md_oldal_settings']['display']['flickr']['flickr_enable'] = array(
        '#type' => 'checkbox',
        '#default_value' => theme_get_setting('flickr_enable' , 'md_oldal'),
        '#field_suffix' => '<label class="label-checkbox" for="edit-flickr-enable">Enable Flickr</label>',
        '#attributes'   => array(
            'class' => array('input-checkbox')
        ),
        '#prefix'   =>  '<div class="form-elements">',
        '#suffix'   => '</div>',
    );
    $form['md_oldal_settings']['display']['flickr']['flickr_uid'] = array(
        '#type' => 'textfield',
        '#title'    => 'User ID',
        '#attributes' => array(
            'class' => array('input-border'),
        ),
        '#default_value' => theme_get_setting('flickr_uid' , 'md_oldal'),
        '#prefix'               => '<div class="form-group">',
        '#suffix'               => '</div>',
        '#description'          =>  t('Enter your user ID on Flickr'),
    );
    $form['md_oldal_settings']['display']['flickr']['flickr_images'] = array(
        '#type' => 'textfield',
        '#title'    => 'Number of images',
        '#attributes' => array(
            'class' => array('input-border'),
        ),
        '#default_value' => theme_get_setting('flickr_images' , 'md_oldal'),
        '#prefix'               => '<div class="form-group">',
        '#suffix'               => '</div>',
        '#description'          =>  t('Enter number of images to display'),
    );
}