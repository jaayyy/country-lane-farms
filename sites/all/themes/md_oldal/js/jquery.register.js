(function ($) {
    $(document).ready(function() {
        var hear = $("#hear-about").val();
        if (hear == "Friend" || hear == "Other") {
            $("#hear-option").show();
        }
        
        if (hear == "Friend") {
            $("#hear-option").attr("placeholder", "Tell us your friend's name");
        }
        
        if (hear == "Other") {
            $("#hear-option").attr("placeholder", "Let us know how you heard");
        }
        
        $("#register-form").on("submit", function(event) {
           var valid = true;

            if ($("#first-name").val().length == 0) {
                valid = false;
                $("#first-name").popover({
                    content	:	"Please enter your first name."
                }).popover('show');                
            }
            var radios = $("input[type='radio'][name='f[locationid]']:checked").val();
            
            if (radios == 0) {
               valid = false;
                $("#preferred-location").popover({
                    content	:	"Please select your preferred pickup location."
                }).popover('show');           
            }
            
            if ($("#last-name").val().length == 0) {
                valid = false;
                $("#last-name").popover({
                    content	:	"Please enter your last name."
                }).popover('show');                
            }
            
            if (!isValidEmailAddress($("#email").val())) {
                valid = false;
                $("#email").popover({
                    content	:	"Please enter a valid email address."
                }).popover('show');                
            }  else {
                if ($("#email").val()!= $("#conf-email").val()) {
                    valid = false;
                    $("#conf-email").popover({
                        content	:	"Please confirm your email address."
                    }).popover('show');                
                } else {
                    if ($("#email").attr("ok") == "false") {
                        valid = false;
                        $("#email").popover({
                            content	:	"This is email is already assigned to an account, please use a different one."
                        }).popover('show');                
                    }
                }
            }
            
            if ($("#password").val().length <= 7) {
                valid = false;
                $("#password").popover({
                    content	:	"Please choose a password at least 8 chars in length."
                }).popover('show');                
            }  else {
                if ($("#password").val()!= $("#conf-password").val()) {
                    valid = false;
                    $("#conf-password").popover({
                        content	:	"Please confirm your password."
                    }).popover('show');                
                } 
            }
            
            if ($("#phone").val().length == 0) {
                valid = false;
                $("#phone").popover({
                    content	:	"Please enter your phone number."
                }).popover('show');                
            }
 
            if (!$("#privacy").prop("checked")) {
                valid = false;
                $("#privacy").popover({
                    content	:	"Please agree to the Privacy Policy."
                }).popover('show');                
            }
            
            if (!$("#terms").prop("checked")){
                valid = false;
                $("#terms").popover({
                    content	:	"Please agree to our Terms."
                }).popover('show');                
            }            
            
            if (!valid) {
                event.preventDefault();
            }
            
        });
        
        $("#email").on("change", function() {
            var email = $(this).val();
            if (email.length > 0) {
                
             var url = "/username-check";
             var parameters = {
                             e   :   email
             };
             $.get(url, parameters, function(data) {
                data = data.trim();
                 var ok = false;
                 if (data == "false") {
                     ok = true;
                 }
                 $("#email").attr("ok", ok);
             });
            }
        });
        $("#hear-about").on("change", function() {
            var value = $(this).val();
            switch(value) {
                case "Friend":
                     $("#hear-option").attr("placeholder", "Tell us your friend's name");
                    break;
                case "Other":
                    $("#hear-option").attr("placeholder", "Let us know how you heard");
                    break;
            }
            
            switch(value) {
            case "Friend":
            case "Other":
                $("#hear-option").fadeIn("slow").val("");
                $("#hear-value").val(value + ": No Selection");
                break;
            case "":
                $("#hear-option").fadeOut("slow").val("");
                $("#hear-value").val(""); 
                break;
            case "Internet":
                $("#hear-option").fadeOut("slow").val("");
                 $("#hear-value").val(value);
                
             break;
            }
            
            console.log( $("#hear-value").val());
            
        });
        $("#hear-option").on("change", function() {
            var option = $("#hear-about").val();
            var value = $(this).val();
            
            if (value.length > 0) {
            $("#hear-value").val(option + ": " + value);
   
            } else {
                $("#hear-value").val(option + ": No Selection");
            }
            console.log($("#hear-value").val());
        });
    });

})(jQuery);