var map;
var beddingtoncoords;
var canmorecoords;
var chestermerecoords;
var clarioncoords;
var crowfootcoords;
var farmcoords;
var midnaporecoords;
var reddeercoords;
var varsitycoords;
var westbrookcoords;

function initialize() {
    var startcoords = new google.maps.LatLng(51.813208, -114.073258);
    var mapoptions = {
      zoom: 7,
      center: startcoords
    }
    
    map = new google.maps.Map(document.getElementById('map-container'), mapoptions);
  
    beddingtoncoords = new google.maps.LatLng(51.1252, -114.0666);
    var beddington = new google.maps.Marker({
        position: beddingtoncoords,
        title: 'Beddington'
    });
    
    beddington.setMap(map);
    
    canmorecoords = new google.maps.LatLng(51.51447, -115.204695);
    var canmore = new google.maps.Marker({
        position: canmorecoords,
        title: 'Canmore / Coast Canmore Hotel'
    });
    
    canmore.setMap(map);
    
    chestermerecoords = new google.maps.LatLng(51.0481425, -113.8235759);
    var chestermere = new google.maps.Marker({
        position: chestermerecoords,
        title: 'Chestermere'
    });
    
    chestermere.setMap(map);
    
    clarioncoords = new google.maps.LatLng(51.4624, -114.03757);
    var clarion = new google.maps.Marker({
        position: clarioncoords,
        title: 'Clarion Hotel'
    });
    
    clarion.setMap(map);
    
    crowfootcoords = new google.maps.LatLng(51.1289, -114.2004);
    var crowfoot = new google.maps.Marker({
        position: crowfootcoords,
        title: 'Crowfoot'
    });
    
    crowfoot.setMap(map);
    
    farmcoords = new google.maps.LatLng(51.23786, -114.81569);
    var farm = new google.maps.Marker({
        position: farmcoords,
        title: 'Farm Pickup'
    });
    
    farm.setMap(map);
    
    midnaporecoords = new google.maps.LatLng(50.91043, -114.062);
    var midnapore = new google.maps.Marker({
        position: midnaporecoords,
        title: 'Midnapore'
    });
    
    midnapore.setMap(map);
    
    reddeercoords = new google.maps.LatLng(52.242, -113.8378);
    var reddeer = new google.maps.Marker({
        position: reddeercoords,
        title: 'Red Deer / Crossroads Church'
    });
    
    reddeer.setMap(map);
    
    southcentrecoords = new google.maps.LatLng(50.9505, -114.0665);
    var southcentre = new google.maps.Marker({
        position: southcentrecoords,
        title: 'Southcentre Mall'
    });
    
    southcentre.setMap(map);
    
    varsitycoords = new google.maps.LatLng(51.1025, -114.1619);
    var varsity = new google.maps.Marker({
        position: varsitycoords,
        title: 'Varsity'
    });
    
    varsity.setMap(map);
    
    westbrookcoords = new google.maps.LatLng(51.22503, -114.81569);
    var westbrook = new google.maps.Marker({
        position: westbrookcoords,
        title: 'Westbrook Mall'
    });
    
    westbrook.setMap(map);
        
}

google.maps.event.addDomListener(window, 'load', initialize);
function PickupSelected(id) {

    
    var coords = null;
    switch (id) {
        case 1:
            coords = beddingtoncoords;
            break;
        case 3:
            coords = chestermerecoords;
            break;
        case 4:
            coords = clarioncoords;
            break;
        case 5:
            coords = crowfootcoords;
            break;
        case 6:
            coords = farmcoords;
            break;
        case 7:
            coords = midnaporecoords;
            break;
        case 8:
            coords = reddeercoords;
            break;
        case 9:
            coords = southcentrecoords;
            break;
        case 11:
            coords = varsitycoords;
            break;
        case 12:
            coords = westbrookcoords;
            break;
        case 13:
            coords = canmorecoords;
            break;
    }
        map.setZoom(10);

        setTimeout(function() {map.panTo(coords); }, 1200);
        setTimeout(function() {smoothZoom(map, 15, map.getZoom()); }, 1200);
        
        
    
}
var timeout;
function smoothZoom (map, max, cnt) {
    if (cnt >= max) {
            return;
        }
    else {
        z = google.maps.event.addListener(map, 'zoom_changed', function(event){
            google.maps.event.removeListener(z);
            smoothZoom(map, max, cnt + 1);
        });
        setTimeout(function(){map.setZoom(cnt)}, 100); // 80ms is what I found to work well on my system -- it might not work well on all systems
    }
} 