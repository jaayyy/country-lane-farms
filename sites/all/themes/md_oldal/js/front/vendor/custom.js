// JavaScript Document

var $ = jQuery.noConflict();

$(document).ready(function($) {
	$('.navbar .col-sm-11 ul.menu').removeClass('menu').addClass('nav navbar-nav main-nav');
	$('.inverse-color .searh-holder .form-text').addClass('form-control');
	
	$(".full-width").fitVids();
	
	if($('body').hasClass("colored")) {
		$('body').find('.line').each(function() {
		  var html = $(this).find('.col-sm-12').html();
		  html = '<div class="inner-container">' + html + '</div>';
		  $(this).find('.col-sm-12').html(html);
		});
	}
	
	$('.testimonials .col-sm-6:odd').find('.testimonial').addClass('highlighted');
	
	if($('body').hasClass('page-blog-index-2') || 
	   $('body').hasClass('page-blog-index-3') ||
	   $('body').hasClass('page-blog-index')   ||
	   $('body').hasClass('page-taxonomy-term'))
	{
		$('ul.pager .pager-previous').find('a').addClass('btn btn-primary');
		$('ul.pager .pager-next').find('a').addClass('btn btn-primary');
		var pager = $('div.item-list').find('ul.pager');	
		if(pager.length > 0) {
		   var contentHTML = pager.html();
		   pager.wrap( "<div class='container'></div>" );
		}
	}
	
	var replyContent = $('body.page-comment-reply').find('#block-system-main').html();
	replyContent = '<div class="blog-form"><div class="container">' + replyContent + '</div></div>';
	$('body.page-comment-reply').find('#block-system-main').html(replyContent);
	
	$('.view-list-price').find('.col-sm-4:nth-child(2)').children('.pricing-box').addClass('suggested');
	
	var isotopeContainer = $('.isotopeContainer');
	isotopeContainer.isotope({
		itemSelector: '.isotopeSelector',
		layoutMode: 'fitRows'
	});
	$('.isotopeFilters').on( 'click', 'button', function() {
		$('.isotopeFilters').find('.active').removeClass('active');
		$(this).addClass('active');
		var filterValue = $(this).attr('data-filter');
		isotopeContainer.isotope({ filter: filterValue });
	});
});

$(document).ajaxStop(function() {
  	$('.custom-select').selectBoxIt({
	   autoWidth: false
	});
});