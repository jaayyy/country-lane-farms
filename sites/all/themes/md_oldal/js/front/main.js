var $ = jQuery.noConflict();
var Oldal = {
    init : function() {
        //this.initInstagram();
        this.initFlexsliders();
        this.initTooltips();
        this.initPopovers();
        this.resizeContent();
        this.calculateMainMargin();

        $('.search-trigger').click(this.toggleSearch);

        $('.custom-select').selectBoxIt({
           autoWidth: false
        });
        $('.main-nav >li').each(this.addMobileButton);
        $(window).scroll(this.moveHeader);
        this.initFancybox(); 
    },
    initFancybox : function() {
        $('.trigger-fancybox').fancybox({
            padding : 0,
            autoResize: true,
            arrows : false,
            closeBtn : false,
            prevEffect : 'fade',
            nextEffect : 'fade',
            helpers : {
                buttons : {
                    position : 'top'
                }
            },
            beforeShow: function() {
                var closeButton = $('<button class="oldal-close oldal-fancy"><i class="entyp-cancel-1"></i></button>');
                var fullscreenButton = $('<button class="oldal-fullscreen oldal-fancy"><i class="entyp-resize-full-1"></i></button>');
                closeButton.click(function() {
                    $.fancybox.close();
                });
                fullscreenButton.click(function() {
                    $.fancybox.toggle();
                });
                $.fancybox.wrap.append(closeButton);
                $.fancybox.wrap.append(fullscreenButton);
            }
        });
        $('.fancybox-product').fancybox({
            padding : 0,
            autoResize: true,
            arrows : false,
            closeBtn : false,
            prevEffect : 'fade',
            nextEffect : 'fade',
            helpers : {
                buttons : {
                    position : 'top'
                }
            },
            beforeShow: function() {
                var closeButton = $('<button class="oldal-close oldal-fancy"><i class="entyp-cancel-1"></i></button>');
                var fullscreenButton = $('<button class="oldal-fullscreen oldal-fancy"><i class="entyp-resize-full-1"></i></button>');
                closeButton.click(function() {
                    $.fancybox.close();
                });
                fullscreenButton.click(function() {
                    $.fancybox.toggle();
                });
                $.fancybox.wrap.append(closeButton);
                $.fancybox.wrap.append(fullscreenButton);
            }
        });
    },
    calculateMainMargin : function() {
        var mainWrap = $('.main-wrap');
        var height = 0;
        if(mainWrap.length) {
            $('.main-footer').length && (height = $('.main-footer').outerHeight());
            $('.simple-footer').length && (height = $('.simple-footer').outerHeight());
            mainWrap.css({
               "margin-bottom" : height + "px"
            });
        }
    },
    addMobileButton : function() {
        var el = $(this);
        var button = $('<button class="dropdown-toggle"><i class="glyphicon glyphicon-chevron-down"></i></button>');
        if(el.find('>ul').length) {
            el.append(button);
            button.click(Oldal.mobileDropdown);
        }
    },
    mobileDropdown : function() {
        var submenu = $(this).parent().find(">ul");
        if(submenu.is(':visible')) {
            submenu.slideUp(300);
            $(this).html('<i class="glyphicon glyphicon-chevron-down"></i>');
        } else {
            submenu.slideDown(300);
            $(this).html('<i class="glyphicon glyphicon-chevron-up"></i>');
        }
    },
    toggleSearch : function(e) {
        var searchWrap = $('.search-wrap');
        if(searchWrap.is(':visible')) {
            searchWrap.slideUp(300);
        } else {
            searchWrap.slideDown(300);
            searchWrap.find('.form-control').focus();
        }
        e.preventDefault();
    },
    hideBigSearch : function() {
        if($(window).width() <= 768) {
            $('.search-wrap').hide();
        }
    },
    initFlexsliders : function() {
        $('.recent-work').flexslider({
            animation: "slide",
            controlNav: false,
            directionNav : false,
            animationLoop: true,
            slideshow: false
        });
        $('.recent-work .slider-controls .left').click(function() {
            $('.recent-work').flexslider('prev');
        });
        $('.recent-work .slider-controls .right').click(function() {
            $('.recent-work').flexslider('next');
        });
        $('.post-images').flexslider({
            animation: "slide",
            controlNav: false,
            directionNav : false,
            animationLoop: true,
            slideshow: false
        });
        $('.post-images .slider-controls .left').click(function() {
            $('.post-images').flexslider('prev');
        });
        $('.post-images .slider-controls .right').click(function() {
            $('.post-images').flexslider('next');
        });
        $('.recent-posts').flexslider({
            animation: "slide",
            controlNav: false,
            directionNav : false,
            animationLoop: true,
            slideshow: false
        });
        $('.recent-posts .slider-controls .left').click(function() {
            $('.recent-posts').flexslider('prev');
        });
        $('.recent-posts .slider-controls .right').click(function() {
            $('.recent-posts').flexslider('next');
        });
        $('.product-slider').flexslider({
            animation: "slide",
            controlNav: true,
            directionNav : false,
            animationLoop: true,
            slideshow: false
        });
        $('.similar-slider').flexslider({
            animation: "slide",
            controlNav: false,
            directionNav : false,
            animationLoop: true,
            slideshow: false
        });
        $('.similar-slider .slider-controls .left').click(function() {
            $('.similar-slider').flexslider('prev');
        });
        $('.similar-slider .slider-controls .right').click(function() {
            $('.similar-slider').flexslider('next');
        });
    },
    initTooltips : function() {
        $('.oldal-tooltip').tooltip();
    },
    initPopovers : function() {
        $('.oldal-popover').popover();
    },
    resizeContent : function() {
        $('.main-container').css('min-height', ($(window).height() - $('.navbar').outerHeight() - $('.main-footer').outerHeight()) + "px");

        var loginWrap = $('.front .login-order-wrap');
        var loginInner = loginWrap.find('.login-order');

        loginWrap.width('auto');
        loginInner.css('position', 'static');

        //setTimeout(function(){
            //loginWrap.width(loginInner.width());
            //loginInner.css('position', 'fixed');
       // }, 10);
    },
    moveHeader : function() {
        var homeHero = $('.home-hero');
        var firstHeader = $('.navbar .normal-color');
        var secondHeader = $('.navbar .inverse-color');
        var searchWrap = $('.navbar .search-wrap');
        var scrollTop = $(window).scrollTop();
        var secondHeaderH = secondHeader.outerHeight();
        var firstHeaderH = firstHeader.outerHeight();
        var headerSlider = $('.header-slider');
        var topOffset = firstHeaderH;
        if(headerSlider.length) {
            topOffset = headerSlider.outerHeight();
        }
        if(scrollTop >= topOffset) {
            homeHero.addClass('sticky');
            secondHeader.addClass('sticky');
            firstHeader.addClass('opacity');
            searchWrap.addClass('sticky');
            searchWrap.css('top', secondHeaderH + "px");
            $("body").css('padding-top', secondHeaderH+'px');
        } else {
            homeHero.removeClass('sticky');
            firstHeader.removeClass('opacity');
            secondHeader.removeClass('sticky');
            searchWrap.removeClass('sticky');
            searchWrap.css('top', "100%");
            $("body").css('padding-top', 0);
        }
    }
};

$(document).ready(function() {
    Oldal.init();


    var frontLogo = $('.front #fixed-header .logo');
    var frontRightNav = $('.front #fixed-header .login-order');

    if (frontLogo.length) {
        //Make the top stuff sticky
        frontLogo.css({
            top: 10,
            position: 'fixed',
            width: 150,
            height: 150
        })

        scrollHandler();
    };

    function scrollHandler() {
        if (frontLogo.length) {
            if ($(window).width() > 768) {
                var stickyProgress = $(window).scrollTop() / 395;


                if ($(window).scrollTop() > 395){  

                    if (!$('#header').hasClass('sticky')) {
                        frontLogo.css({
                            width: '',
                            height: ''
                        });

                        $('#header').addClass("sticky");
                    };
                    

                    

                    
                }
                else{
                    
                    if ($('#header').hasClass('sticky')) {
                        // frontRightNav.stop(true, false).animate({
                        //     marginLeft: 150,
                        //     left: '50%'
                        // }, 200);
                        frontRightNav.stop(true, false);
                        $('#header').removeClass("sticky");
                    };

                    frontLogo.css({
                        width: 80 + (150-80) * (1 - stickyProgress),
                        height: 80 + (150-80) * (1 - stickyProgress),
                        top: 3 + 8 * (1 - stickyProgress),
                        marginLeft: 35 * (stickyProgress)
                    });

                }

                if ($(window).scrollTop() > 380){
                    $('.home-hero').addClass('sticky');
                } else {
                    $('.home-hero').removeClass('sticky');
                }
            } else {
                frontLogo.css({
                    width: '',
                    height: '',
                    top: 0,
                    position: 'static'
                });
            }

            
        }
    }

    $(window).scroll(function() {
        scrollHandler();

    });

    $('#header .menu a.has-subnav, #header .subnav').mouseover(function() {
        $('.subnav').show();
    }).mouseout(function() {
        $('.subnav').hide();
    });

    var delivery = 1;
    var rowHeight = function(delivery) {
        $('.del-'+delivery).each(function(){
            $(this).height($(this).find('.option-item > span').height()+10);
        });
    }
    rowHeight(delivery);

    if($(window).width() <= 768) {
        $('.option-item').on("click", function() {
            $('.option-row').children('td').removeClass('selected');
            $('.option-row').find('.loc-title').css('color', 'inherit');
            $('.option-row.active').find('.loc-title').css('color', '#fff');
            $(this).parents('td').addClass('selected');
        })
    }

    $('.del-prev').on('click', function(e){
        e.preventDefault();
        $('.del-'+delivery).hide();
        if(delivery == 1) { delivery = 1; }
        else { delivery -= 1; }        
        $('.del-'+delivery).show(); 
        rowHeight(delivery); 
        if($('.del-'+delivery+'.selected').is(':visible')) {
            $('.option-row.active').find('.loc-title').css('color', '#fff');
        } else {
            $('.option-row.active').find('.loc-title').css('color', 'inherit');
        }
    });
    $('.del-next').on('click', function(e){
        e.preventDefault();
        $('.del-'+delivery).hide();
        if(delivery == 4) { delivery = 4; }
        else { delivery += 1; }        
        $('.del-'+delivery).show();
        rowHeight(delivery);
        if($('.del-'+delivery+'.selected').is(':visible')) {
            $('.option-row.active').find('.loc-title').css('color', '#fff');
        } else {
            $('.option-row.active').find('.loc-title').css('color', 'inherit');
        }
    });

    $('.flexslider .views-field-field-image2').each(function(){
        var image = $(this).find('img');
        $(this).css({backgroundImage: 'url('+image.attr('src')+')'});
        image.detach();
    });

});

$(window).bind("load", function() {
    Oldal.resizeContent();
});

$(window).resize(function() {
    Oldal.hideBigSearch();
    Oldal.resizeContent();
});




$(".post").fitVids();
