(function ($) {
    $(document).ready(function() {
        $(".option-item").on("click",function() {
            
            if (!$(this).hasClass("selected")) { 
                var itemid = $(this).attr("detail-id");

                $(".option-item").removeClass("selected");
                $(this).addClass("selected");
                /*
                request = {
                        lat    :   $(this).attr("lat"),
                        lon   :  $(this).attr("long")                   
                    }
                $(".location-map-container").fadeOut("slow");   
                $.get(AJAX_URL + '/order-day.map.php', request, function(data) {
                    $(".location-map-container").html(data);
                    $(".location-map-container").fadeIn("slow");
                });*/
                marker.setMap(null);
                var markerposition = new google.maps.LatLng( $(this).attr("lat"), $(this).attr("long"));
                marker = new google.maps.Marker({
                    position: markerposition,
                    map: map
                });
                map.panTo(markerposition);
                var details = {
                    "dt"  :   $(this).attr("dt"),
                    "t"      :   $(this).attr("when"),
                    "ty"      :   $(this).attr("delivery-type"),
                    "n"      :   $(this).attr("loc"),
                    "a"      :   $(this).attr("adr")
                };
                $(".pickup-details").fadeOut("slow");
                $.get(AJAX_URL + '/order-day.details.php', details, function(data) {
                    $(".pickup-details").html(data);
                    $(".pickup-details").fadeIn("slow");
                });
                
                $(".delivery-item-id").val(itemid);

                $('.option-row.active').removeClass('active');
                $(this).parents('.option-row').addClass('active');
                
            }
        });
        $("#confirm-order-button").on("click", function() {
            $(this).attr("disabled", "disabled");
        });
    });
})(jQuery);