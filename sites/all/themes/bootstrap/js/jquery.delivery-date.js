(function ($) {
    $(document).ready(function() {
        
        $("#delivery-date").datepicker();
        
        $(".location-check").on("change", function() {
            if ($(this).prop("checked")) {
                $(this).parent().parent().addClass("om-item-selected").find("td").addClass("om-item-selected");
            } else {
                $(this).parent().parent().removeClass("om-item-selected").find("td").removeClass("om-item-selected");
            }
        });
        
        $("#delivery-date-form").on("submit", function(event) {
            var submit = true;
            
            $(".location-check:checked").each(function() {
                var id = $(this).attr("location-id");
                if ($("#start-" + id).val().length == 0) {
                   $("#start-" + id).addClass("input-error");
                } else {
                    $("#start-" + id).removeClass("input-error");
                }
                
                if ($("#end-" + id).val().length == 0) {
                   $("#end-" + id).addClass("input-error");
                }  else {
                    $("#end-" + id).removeClass("input-error");
                }              
            });
            if($(this).find(".input-error").length) {
                submit = false;
            }
            if (!submit) {
                event.preventDefault();
            }
        });
    });
})(jQuery);