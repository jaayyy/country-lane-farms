(function ($) {
    $(document).ready(function() {
        $(".add-to-queue").on("click", function() {
            var orderid = $(this).attr("orderid");
            var deliveryid = $(this).attr("delivery-id");
            var request = {
                            a   :   "enqueue",
                            o   :   orderid,
                            d   :   deliveryid
                    }
            $.get(AJAX_URL + "/orders-queue.php", request, function(data) {

                location.reload();
            });
        });
        /*
        $(".close-delivery").on("click", function() {
            var deliveryid = $(this).attr("delivery-id");
            var request = {
                    a   :   "close-delivery",
                    d   :   deliveryid 
            }
            
            $.get(AJAX_URL + "/orders-queue.php", request, function(data) {
                location.reload();
            });            
        });
        */
        $(".delete-order").on("click", function() {
                var orderID = $(this).attr("order-id");
                var name = $(this).attr("customer");
                var amount = parseFloat($(this).attr("amount"));
                
                $("#confirm-delete-button").attr("order-id", orderID);
                $("#delete-name").html(name);
                $("#delete-amount").html(amount.toFixed(2));
                $("#delete-order-modal").modal("show");
        });
        
        $("#confirm-delete-button").on("click", function() {
            var orderID = $(this).attr("order-id");

            var url = "/admin/order_management/omprocessing/";
            var parameters = {
                a   :   "delete-order",
                o   :   orderID
            }
            
            $.get(url, parameters, function(data) {
                
                $("#row-" + data).fadeOut("slow");
                $("#delete-order-modal").modal("hide");
            });
        });
        
        $(".order-queue-button").on("click", function() {
            var orderID = $(this).attr("order-id");
            var current = $(this).attr("current-position");
            var direction = $(this).attr("direction");
            var orderqueue = JSON.stringify(queue);
            var url = "/admin/order_management/omprocessing/";
            var parameters = {
                a   :   "reorder-queue",
                o   :   orderID,
                c   :   current,
                d   :   direction,
                oq   :   encodeURIComponent(orderqueue)
            }
            
            $.get(url, parameters, function(data) {
                location.reload();
            });
        });
    });
    

})(jQuery);