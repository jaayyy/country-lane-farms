(function ($) {
    function CalculateTotal() {
        $("#order-total-bucket").val("0.00");
        
        $(".item-total-hidden").each(function() {
            
            var current = parseFloat($("#order-total-bucket").val());

            var addValue = parseFloat($(this).val());
            var newTotal = current + addValue;
            if (!isNaN(newTotal)) {
                $("#order-total").html(newTotal.toFixed(2));
                $("#order-total-bucket").val(newTotal);
            }
            
        });
    }
    $(document).ready(function() {
        
        $(".quantity-input").on("change", function() {
            var id = $(this).attr("pid");
            var bulkmin = $("#info-" + id).attr("bulk-min");
            var quantity = $(this).val();
            var pricemodel = "";
            var itemid = $("#itemid-" + id).val();
            if (bulkmin > 1 && quantity >= bulkmin) {
                pricemodel = "bulk";
            } else {
                pricemodel = "regular";
            }
            $("#pricemodel-" + id).val(pricemodel);
            
            if (quantity  > 0 && itemid == 0) {
                 $("#itemid-" + id).val("NEW");
            } else if (quantity  == 0 && itemid == "NEW") {
                $("#itemid-" + id).val(0);
            }
            
            var pricelb = 0;
            if (pricemodel == "regular") {
                pricelb = $("#info-" + id).attr("price-lb");
                $("#cost-" + id).val($("#info-" + id).attr("unit-cost"));
                if (pricelb == 0) {
                    var total = $("#info-" + id).attr("unit-cost") * quantity;
                }                
            } else {
                pricelb = $("#info-" + id).attr("price-lb-bulk");
                $("#cost-" + id).val($("#info-" + id).attr("unit-cost-bulk"));
                if (pricelb == 0) {
                    var total = $("#info-" + id).attr("unit-cost-bulk") * quantity;
                }
            }
            
            if (pricelb == 0) {
                $("#total-" + id).html('$' + total.toFixed(2)).removeClass("red-text");
                $("#total-holder-" + id).val(total.toFixed(2));
                CalculateTotal();
            }
            
            $("#lb-" + id).val(pricelb);

        });
        
        $(".actual-weight").on("change", function() {
            var id = $(this).attr("pid");
            var pricelb = parseFloat($("#lb-" + id).val());
            var weight = parseFloat($(this).val());
            var total = pricelb * weight;

            $("#total-" + id).html('$' + total.toFixed(2)).removeClass("red-text");
            $("#total-holder-" + id).val(total.toFixed(2));
            
            CalculateTotal();
        });
    });
})(jQuery);