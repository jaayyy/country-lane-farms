(function ($) {
    $(document).ready(function() {
        $(".delete-delivery-date").on("click", function() {
            var deliveryid = $(this).attr("delivery-id");
            var numberorders = $(this).attr("num-orders");
            var type = $(this).attr("delivery-type");
            var deliverydate = $(this).attr("delivery-date");
            
            $("#delivery-type").html(type);
            $("#delivery-date").html(deliverydate);
            $("#confirm-delete-button").attr("delivery-id", deliveryid);
            
            if (numberorders > 0) {
                $("#orders-warning").show();
                $("#open-orders").html(numberorders);
            } else {
                $("#orders-warning").hide();
            }
            
            $("#delete-delivery-modal").modal("show");
        });
        
        
        $("#confirm-delete-button").on("click", function() {
            var container = $("#delivery-date-row-" + $("#confirm-delete-button").attr("delivery-id"));
            var url = "/admin/order_management/omprocessing/";
            var parameters = {
                a   :   "delete-delivery",
                d   :   $("#confirm-delete-button").attr("delivery-id")
            }
            
            $.get(url, parameters, function(data) {
                $("#delivery-date-row-" + $("#confirm-delete-button").attr("delivery-id")).fadeOut("slow");
                $("#delete-delivery-modal").modal("hide");
            });            
        });
    });
})(jQuery);